!function(modules) {
    var installedModules = {};
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) return installedModules[moduleId].exports;
        var module = installedModules[moduleId] = {
            i: moduleId,
            l: !1,
            exports: {}
        };
        return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), 
        module.l = !0, module.exports;
    }
    __webpack_require__.m = modules, __webpack_require__.c = installedModules, __webpack_require__.d = function(exports, name, getter) {
        __webpack_require__.o(exports, name) || Object.defineProperty(exports, name, {
            enumerable: !0,
            get: getter
        });
    }, __webpack_require__.r = function(exports) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(exports, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(exports, "__esModule", {
            value: !0
        });
    }, __webpack_require__.t = function(value, mode) {
        if (1 & mode && (value = __webpack_require__(value)), 8 & mode) return value;
        if (4 & mode && "object" == typeof value && value && value.__esModule) return value;
        var ns = Object.create(null);
        if (__webpack_require__.r(ns), Object.defineProperty(ns, "default", {
            enumerable: !0,
            value
        }), 2 & mode && "string" != typeof value) for (var key in value) __webpack_require__.d(ns, key, function(key) {
            return value[key];
        }.bind(null, key));
        return ns;
    }, __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function getDefault() {
            return module.default;
        } : function getModuleExports() {
            return module;
        };
        return __webpack_require__.d(getter, "a", getter), getter;
    }, __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    }, __webpack_require__.p = "", __webpack_require__(__webpack_require__.s = 100);
}([ function(module, exports) {
    module.exports = require("tslib");
}, function(module, exports) {
    module.exports = require("typescript-ioc");
}, function(module, exports) {
    module.exports = require("typeorm");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "g", (function() {
        return REAL_HOSTNAME;
    })), __webpack_require__.d(__webpack_exports__, "d", (function() {
        return HOSTNAME;
    })), __webpack_require__.d(__webpack_exports__, "e", (function() {
        return PEM;
    })), __webpack_require__.d(__webpack_exports__, "f", (function() {
        return PORT;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return ADMIN;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return API;
    })), __webpack_require__.d(__webpack_exports__, "c", (function() {
        return HEALTH;
    })), __webpack_require__.d(__webpack_exports__, "h", (function() {
        return STATIC;
    }));
    var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(11), path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(12), net__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(24);
    const regulus = require(__dirname + "/../package.json").regulus || {}, REAL_HOSTNAME = regulus.hostname || "localhost", HOSTNAME = process.argv.includes("--local") && !Object(net__WEBPACK_IMPORTED_MODULE_2__.isIP)(REAL_HOSTNAME) && "localhost" != REAL_HOSTNAME ? "dev." + REAL_HOSTNAME : REAL_HOSTNAME, PEM = REAL_HOSTNAME !== HOSTNAME && Object(fs__WEBPACK_IMPORTED_MODULE_0__.existsSync)(__dirname + "/../REGULUS.pem") && Object(path__WEBPACK_IMPORTED_MODULE_1__.resolve)(__dirname + "/../REGULUS.pem") || "", PORT = REAL_HOSTNAME !== HOSTNAME ? regulus.devport || 7813 : regulus.port || 7812, routes = Object.assign({
        admin: "/adm",
        api: "/api",
        health: "/health"
    }, regulus.routes), ADMIN = routes.admin, API = routes.api, HEALTH = routes.health, STATIC = "/static";
}, function(module, exports) {
    module.exports = require("@taraflex/string-tools");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    function has(item, state) {
        return (item.state & state) === state;
    }
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return has;
    }));
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "User", (function() {
        return User;
    })), __webpack_require__.d(__webpack_exports__, "UserRepository", (function() {
        return UserRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7);
    let User = class User {};
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], User.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: /\S+@\S+/
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        unique: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], User.prototype, "email", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 5,
        max: 70
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        length: 60
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], User.prototype, "password", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 2147483647
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], User.prototype, "role", void 0), 
    User = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)(), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Entity)() ], User);
    class UserRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return Access;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return crud_v;
    }));
    __webpack_require__(83);
    var external_typeorm_ = __webpack_require__(2), types = __webpack_require__(5), string_tools_ = __webpack_require__(4), external_fastest_validator_ = __webpack_require__(52), external_fastest_validator_default = __webpack_require__.n(external_fastest_validator_), array_ = __webpack_require__(53), array_default = __webpack_require__.n(array_), string_ = __webpack_require__(29), string_default = __webpack_require__.n(string_);
    const validator_helpers_v = new external_fastest_validator_default.a({
        messages: {
            validRegex: "The '{field}' {actual}"
        }
    });
    validator_helpers_v.add("regex", (function({messages}) {
        return {
            source: `\n            try {\n                new RegExp(value);\n            } catch(err) {\n                ${this.makeError({
                type: "validRegex",
                actual: "err.message",
                messages
            })}\n            }\n            return value;\n        `
        };
    })), validator_helpers_v.add("javascript", (function({messages}) {
        return {
            source: `\n            try {\n                new Function(value);\n            } catch(err) {\n                ${this.makeError({
                type: "validRegex",
                actual: "err.message",
                messages
            })}\n            }\n            return value;\n        `
        };
    })), validator_helpers_v.add("wysiwyg", string_default.a), validator_helpers_v.add("link", string_default.a), 
    validator_helpers_v.add("md-tgclient", string_default.a), validator_helpers_v.add("md-tgbotlegacy", string_default.a), 
    validator_helpers_v.add("files", array_default.a);
    const compile = validator_helpers_v.compile.bind(validator_helpers_v);
    function getFieldSchema(target, options, propertyName) {
        const {nullable, unsigned, array, zerofill, primary} = options;
        let type;
        try {
            type = options.type.name.toLowerCase();
        } catch {
            type = options.type.toString();
        }
        let vo = target.__validatorInfo && target.__validatorInfo[propertyName];
        const _enum = options.enum || vo && vo.enum;
        let enumPairs = void 0, enumValues = void 0;
        const generated = Object(external_typeorm_.getMetadataArgsStorage)().generations.some(v => v.target === target && v.propertyName === propertyName);
        if (Array.isArray(_enum)) enumValues = _enum, enumPairs = enumValues.map(v => [ v, v ]); else if (_enum) {
            const keys = new Set(Object.keys(_enum));
            for (;keys.size > 0; ) {
                const {value} = keys.values().next();
                let k = value, v = _enum[k];
                !Object(string_tools_.isValidVar)(k) || _enum[v] === k && Object(string_tools_.isValidVar)(v) && v.toUpperCase() == v && k.toUpperCase() !== k ? keys.delete(k) : (Array.isArray(enumPairs) || (enumPairs = [], 
                enumValues = []), keys.delete(v.toString()), keys.delete(k), enumPairs.push([ k, v ]), 
                enumValues.push(v));
            }
        }
        let integer = void 0;
        if (type.includes("int")) type = "number", integer = !0; else if (type.includes("date") || type.includes("time")) type = "date"; else if (type.includes("string") || type.includes("char") || type.includes("clob") || type.includes("text")) type = "string"; else if (type.includes("bool")) type = "boolean"; else {
            if (type.includes("blob") || type.includes("binary")) throw "Validator not implemented";
            "simple-array" === type || "simple-json" === type || "array" === type || array ? type = "array" : "enum" === type || (type = "number");
        }
        (!Array.isArray(enumValues) || enumValues.length < 1) && (enumValues = enumPairs = void 0);
        let state = vo ? 0 | vo.state : 0;
        ("array" === type || "enum" === type || enumValues) && (8 == (8 & state) || !enumValues || vo && vo.min > 1 ? (enumValues = null, 
        type = "array") : type = "enum", vo && (vo = {
            ...vo
        }, delete vo.enum));
        const isNumber = "number" === type, o = {
            type,
            enumPairs,
            enum: enumValues,
            values: enumValues,
            integer,
            min: isNumber && (unsigned || zerofill || vo && vo.positive) ? 0 : null,
            max: isNumber && vo && vo.negative ? 0 : null,
            optional: !generated && (nullable || null != options.default),
            default: zerofill && isNumber && null == options.default ? 0 : options.default,
            state
        }, r = function nullPrune(o) {
            for (let k in o) null == o[k] && delete o[k];
            return o;
        }(vo ? Object.assign(o, vo) : o);
        return "link" === r.type && (r.state = 34 | r.state), primary && (r.state = 18 | r.state | (generated ? 98 : 0)), 
        r;
    }
    function validate(o) {
        const r = this(o);
        if (!0 !== r) throw r;
    }
    function Access(allowedFor, displayInfo, hooks) {
        return entity => {
            const vInsert = Object.create(null), vUpdate = Object.create(null);
            for (let {propertyName, options} of function* getColumns(entity) {
                if (entity && entity !== Function.prototype) {
                    const selfColumns = Object(external_typeorm_.getMetadataArgsStorage)().filterColumns(entity);
                    yield* getColumns(Object.getPrototypeOf(entity));
                    for (let column of selfColumns) yield column;
                }
            }(entity)) {
                const schema = getFieldSchema(entity, options, propertyName);
                Object(types.a)(schema, 34) || (vInsert[propertyName] = schema), vUpdate[propertyName] = schema;
            }
            allowedFor = allowedFor || Object.create(null), allowedFor._ !== +allowedFor._ && (allowedFor._ = -1), 
            Object.freeze(allowedFor), Object.defineProperty(entity, "checkAccess", {
                value(action, user) {
                    if (!user) throw 401;
                    let t = allowedFor[action];
                    if (null == t && (t = allowedFor._), (t & user.role) !== t) throw 403;
                    return !0;
                }
            });
            for (const _ in vInsert) {
                Object.defineProperty(entity, "insertValidate", {
                    value: validate.bind(compile(vInsert))
                });
                break;
            }
            for (const _ in vUpdate) {
                Object.defineProperty(entity, "updateValidate", {
                    value: validate.bind(compile(vUpdate))
                });
                break;
            }
            return displayInfo || (displayInfo = Object.create(null)), displayInfo.display || (displayInfo.display = entity.asyncProvider ? "single" : "table"), 
            Object.defineProperty(entity, "rtti", {
                value: Object.freeze({
                    props: Object.freeze(vUpdate),
                    displayInfo: Object.freeze(displayInfo)
                })
            }), Object.defineProperty(entity, "hooks", {
                value: hooks || Object.create(null)
            }), delete entity.__validatorInfo, entity;
        };
    }
    function crud_v(validatorProps) {
        return ({constructor}, prop) => {
            (constructor.__validatorInfo || (constructor.__validatorInfo = Object.create(null)))[prop] = Object.freeze(validatorProps);
        };
    }
}, function(module, exports, __webpack_require__) {
    "use strict";
    var pug_has_own_property = Object.prototype.hasOwnProperty;
    function pug_classes(val, escaping) {
        return Array.isArray(val) ? function pug_classes_array(val, escaping) {
            for (var className, classString = "", padding = "", escapeEnabled = Array.isArray(escaping), i = 0; i < val.length; i++) className = pug_classes(val[i]), 
            className && (escapeEnabled && escaping[i] && (className = pug_escape(className)), 
            classString = classString + padding + className, padding = " ");
            return classString;
        }(val, escaping) : val && "object" == typeof val ? function pug_classes_object(val) {
            var classString = "", padding = "";
            for (var key in val) key && val[key] && pug_has_own_property.call(val, key) && (classString = classString + padding + key, 
            padding = " ");
            return classString;
        }(val) : val || "";
    }
    function pug_style(val) {
        if (!val) return "";
        if ("object" == typeof val) {
            var out = "";
            for (var style in val) pug_has_own_property.call(val, style) && (out = out + style + ":" + val[style] + ";");
            return out;
        }
        return val + "";
    }
    function pug_attr(key, val, escaped, terse) {
        if (!1 === val || null == val || !val && ("class" === key || "style" === key)) return "";
        if (!0 === val) return " " + (terse ? key : key + '="' + key + '"');
        var type = typeof val;
        return "object" !== type && "function" !== type || "function" != typeof val.toJSON || (val = val.toJSON()), 
        "string" == typeof val || (val = JSON.stringify(val), escaped || -1 === val.indexOf('"')) ? (escaped && (val = pug_escape(val)), 
        " " + key + '="' + val + '"') : " " + key + "='" + val.replace(/'/g, "&#39;") + "'";
    }
    exports.merge = function pug_merge(a, b) {
        if (1 === arguments.length) {
            for (var attrs = a[0], i = 1; i < a.length; i++) attrs = pug_merge(attrs, a[i]);
            return attrs;
        }
        for (var key in b) if ("class" === key) {
            var valA = a[key] || [];
            a[key] = (Array.isArray(valA) ? valA : [ valA ]).concat(b[key] || []);
        } else if ("style" === key) {
            valA = pug_style(a[key]);
            valA = valA && ";" !== valA[valA.length - 1] ? valA + ";" : valA;
            var valB = pug_style(b[key]);
            valB = valB && ";" !== valB[valB.length - 1] ? valB + ";" : valB, a[key] = valA + valB;
        } else a[key] = b[key];
        return a;
    }, exports.classes = pug_classes, exports.style = pug_style, exports.attr = pug_attr, 
    exports.attrs = function pug_attrs(obj, terse) {
        var attrs = "";
        for (var key in obj) if (pug_has_own_property.call(obj, key)) {
            var val = obj[key];
            if ("class" === key) {
                val = pug_classes(val), attrs = pug_attr(key, val, !1, terse) + attrs;
                continue;
            }
            "style" === key && (val = pug_style(val)), attrs += pug_attr(key, val, !1, terse);
        }
        return attrs;
    };
    var pug_match_html = /["&<>]/;
    function pug_escape(_html) {
        var html = "" + _html, regexResult = pug_match_html.exec(html);
        if (!regexResult) return _html;
        var i, lastIndex, escape, result = "";
        for (i = regexResult.index, lastIndex = 0; i < html.length; i++) {
            switch (html.charCodeAt(i)) {
              case 34:
                escape = "&quot;";
                break;

              case 38:
                escape = "&amp;";
                break;

              case 60:
                escape = "&lt;";
                break;

              case 62:
                escape = "&gt;";
                break;

              default:
                continue;
            }
            lastIndex !== i && (result += html.substring(lastIndex, i)), lastIndex = i + 1, 
            result += escape;
        }
        return lastIndex !== i ? result + html.substring(lastIndex, i) : result;
    }
    exports.escape = pug_escape, exports.rethrow = function pug_rethrow(err, filename, lineno, str) {
        if (!(err instanceof Error)) throw err;
        if (!("undefined" == typeof window && filename || str)) throw err.message += " on line " + lineno, 
        err;
        try {
            str = str || __webpack_require__(11).readFileSync(filename, "utf8");
        } catch (ex) {
            pug_rethrow(err, null, lineno);
        }
        var context = 3, lines = str.split("\n"), start = Math.max(lineno - context, 0), end = Math.min(lines.length, lineno + context);
        context = lines.slice(start, end).map((function(line, i) {
            var curr = i + start + 1;
            return (curr == lineno ? "  > " : "    ") + curr + "| " + line;
        })).join("\n");
        throw err.path = filename, err.message = (filename || "Pug") + ":" + lineno + "\n" + context + "\n\n" + err.message, 
        err;
    };
}, function(module, exports) {
    module.exports = require("koa-body");
}, , function(module, exports) {
    module.exports = require("fs");
}, function(module, exports) {
    module.exports = require("path");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "ClientSettings", (function() {
        return ClientSettings;
    })), __webpack_require__.d(__webpack_exports__, "ClientSettingsRepository", (function() {
        return ClientSettingsRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _crud__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7), _ioc__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(14), _utils_tg_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(21);
    let ClientSettings = class ClientSettings {};
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], ClientSettings.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        notEqual: 0,
        description: "Telegram Api id взять [тут](https://my.telegram.org/apps)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 0,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], ClientSettings.prototype, "apiId", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: _utils_tg_utils__WEBPACK_IMPORTED_MODULE_4__.a,
        description: "Telegram Api hash взять [там же](https://my.telegram.org/apps)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: "",
        length: 32
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], ClientSettings.prototype, "apiHash", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        pattern: _utils_tg_utils__WEBPACK_IMPORTED_MODULE_4__.b,
        description: "Токен телеграм бота, получить у [@BotFather](tg://resolve?domain=BotFather)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], ClientSettings.prototype, "token", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        notEqual: 0,
        description: "Id канала (без -100 в начале)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        unsigned: !0,
        default: 0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], ClientSettings.prototype, "channel", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Минимальная пауза между оповещениями в секундах (изменения применяются только на следующей итерации)"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 360,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], ClientSettings.prototype, "alertInterval", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Минимальная пауза перед первой проверкой пользователя после старта диалога с ботом"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        default: 120,
        unsigned: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], ClientSettings.prototype, "firstPause", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Ответ на /start",
        type: "wysiwyg"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "text",
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], ClientSettings.prototype, "helloMessage", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        min: 1,
        description: "Сообщение о необходимости подписки",
        type: "wysiwyg"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "text",
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], ClientSettings.prototype, "badMessage", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.b)({
        description: "Сообщение после подписки (пустое - не высылать)",
        type: "wysiwyg"
    }), Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "text",
        default: ""
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], ClientSettings.prototype, "goodMessage", void 0), 
    ClientSettings = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_crud__WEBPACK_IMPORTED_MODULE_2__.a)({
        GET: 0,
        PATCH: 0
    }, {
        display: "single",
        icon: "cog",
        order: 4
    }, {
        PATCH: "tgclient_save"
    }), Object(_ioc__WEBPACK_IMPORTED_MODULE_3__.a)() ], ClientSettings);
    class ClientSettingsRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return SingletonEntity;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return SingletonRepository;
    }));
    var typeorm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2), typescript_ioc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1), _ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(28);
    function SingletonEntity(options, disable) {
        if (disable) return function(target) {
            return target;
        };
        {
            const ewrap = Object(typeorm__WEBPACK_IMPORTED_MODULE_0__.Entity)(options);
            return function(target) {
                if (ewrap(target), !target.asyncProvider) {
                    const provider = Object(_ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__.b)(async connection => {
                        const repository = connection.getRepository(target);
                        return await repository.findOne() || await repository.createQueryBuilder().insert().values([ {} ]).execute() && await repository.findOneOrFail();
                    });
                    target.asyncProvider = provider, Object(typescript_ioc__WEBPACK_IMPORTED_MODULE_1__.Provided)(provider)(target);
                }
                return target;
            };
        }
    }
    function SingletonRepository(e) {
        return function(target) {
            return target.provider || Object(typescript_ioc__WEBPACK_IMPORTED_MODULE_1__.Provided)(target.provider = Object(_ioc_singleton_providers_factory__WEBPACK_IMPORTED_MODULE_2__.a)(connection => connection.getRepository(e)))(target), 
            target;
        };
    }
}, function(module, exports) {
    module.exports = require("koa-router");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "Settings", (function() {
        return Settings;
    })), __webpack_require__.d(__webpack_exports__, "SettingsRepository", (function() {
        return SettingsRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2), _ioc__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(14), _utils_rnd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(26);
    let Settings = class Settings {
        async updateLinkSecret() {
            this.linkSecret = await Object(_utils_rnd__WEBPACK_IMPORTED_MODULE_3__.a)();
        }
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], Settings.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: Object(_utils_rnd__WEBPACK_IMPORTED_MODULE_3__.b)()
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], Settings.prototype, "secret", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: Object(_utils_rnd__WEBPACK_IMPORTED_MODULE_3__.b)()
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", String) ], Settings.prototype, "linkSecret", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        default: !1
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Boolean) ], Settings.prototype, "installed", void 0), 
    Settings = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(_ioc__WEBPACK_IMPORTED_MODULE_2__.a)(null, !1) ], Settings);
    class SettingsRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, exports) {
    module.exports = require("bcryptjs");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    var got__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(51), got__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(got__WEBPACK_IMPORTED_MODULE_0__);
    __webpack_require__(42);
    __webpack_exports__.a = got__WEBPACK_IMPORTED_MODULE_0___default.a.extend({
        timeout: 6e4,
        ignoreInvalidCookies: !0,
        headers: {
            Accept: "*/*",
            "Cache-Control": "no-cache",
            Connection: "keep-alive",
            Pragma: "no-cache",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
        },
        retry: 0,
        https: {
            rejectUnauthorized: !1
        }
    });
}, function(module, exports) {
    module.exports = require("koa-passport");
}, , function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "c", (function() {
        return getBotInfo;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return BOT_TOKEN_RE;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return API_HASH_RE;
    }));
    var _utils_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(18);
    __webpack_require__(33);
    (function getCommandParser(cmd) {
        const re = new RegExp("^\\/" + cmd + "(\\s+|$)", "i");
        return text => re.test(text) ? text.substr(cmd.length + 2).trim() : null;
    })("start");
    async function getBotInfo(token) {
        const bot = (await Object(_utils_http__WEBPACK_IMPORTED_MODULE_0__.a)(`https://api.telegram.org/bot${token}/getMe`).json()).result;
        if (!bot.is_bot) throw "Invalid token: " + token;
        return bot;
    }
    const BOT_TOKEN_RE = /^\d{9,}:[\w-]{35}$/, API_HASH_RE = /^[0-9abcdef]{32}$/;
}, function(module, exports) {
    module.exports = require("child_process");
}, function(module, exports) {
    module.exports = require("param-case");
}, function(module, exports) {
    module.exports = require("net");
}, function(module, exports) {
    module.exports = require("ws");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "a", (function() {
        return rndString;
    })), __webpack_require__.d(__webpack_exports__, "b", (function() {
        return seedRndString;
    }));
    var base32_encode__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(40), base32_encode__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(base32_encode__WEBPACK_IMPORTED_MODULE_0__), crypto__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(56), node_machine_id__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(57), random_bytes_seed__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(58), random_bytes_seed__WEBPACK_IMPORTED_MODULE_3___default = __webpack_require__.n(random_bytes_seed__WEBPACK_IMPORTED_MODULE_3__), random_seed__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(59), util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(32), _config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3);
    const rndBytes = Object(util__WEBPACK_IMPORTED_MODULE_5__.promisify)(crypto__WEBPACK_IMPORTED_MODULE_1__.randomBytes), STRING_SEED = "subscribe-alert-bot" + _config__WEBPACK_IMPORTED_MODULE_6__.f + Object(node_machine_id__WEBPACK_IMPORTED_MODULE_2__.machineIdSync)(!0);
    async function rndString() {
        return base32_encode__WEBPACK_IMPORTED_MODULE_0___default()(await rndBytes(23 + ~~(7 * Math.random())), "Crockford").toLowerCase();
    }
    function seedRndString(seed = STRING_SEED) {
        const b = random_bytes_seed__WEBPACK_IMPORTED_MODULE_3___default()(seed)(Object(random_seed__WEBPACK_IMPORTED_MODULE_4__.create)(seed).intBetween(23, 30));
        return base32_encode__WEBPACK_IMPORTED_MODULE_0___default()(b, "Crockford").toLowerCase();
    }
}, , function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    function createAsync(initCb) {
        let instance = null;
        return {
            init: async (...args) => instance || (instance = await initCb(...args)),
            get: () => instance
        };
    }
    function create(initCb) {
        let instance;
        return {
            init: (...args) => instance || (instance = initCb(...args)),
            get: () => instance
        };
    }
    __webpack_require__.d(__webpack_exports__, "b", (function() {
        return createAsync;
    })), __webpack_require__.d(__webpack_exports__, "a", (function() {
        return create;
    }));
}, function(module, exports) {
    module.exports = require("fastest-validator/lib/rules/string");
}, function(module, exports) {
    module.exports = require("builtin-status-codes");
}, function(module, exports) {
    module.exports = require("timers");
}, function(module, exports) {
    module.exports = require("util");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_exports__.a = () => Math.floor(9007199254740991 * Math.random());
}, function(module, exports) {
    module.exports = require("etag");
}, function(module, exports) {
    module.exports = require("pluralize");
}, function(module, exports) {
    module.exports = require("msgpack-lite");
}, function(module, exports) {
    module.exports = require("sentence-case");
}, function(module, exports) {
    module.exports = require("marked");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__), __webpack_require__.d(__webpack_exports__, "BUser", (function() {
        return BUser;
    })), __webpack_require__.d(__webpack_exports__, "BUserRepository", (function() {
        return BUserRepository;
    }));
    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0), typeorm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
    let BUser = class BUser {};
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.PrimaryGeneratedColumn)(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], BUser.prototype, "id", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        unsigned: !0,
        default: 0,
        unique: !0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], BUser.prototype, "tId", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        unsigned: !0,
        default: 0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], BUser.prototype, "subscribed", void 0), 
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Column)({
        type: "int",
        unsigned: !0,
        default: 0
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__metadata)("design:type", Number) ], BUser.prototype, "lastAlert", void 0), 
    BUser = Object(tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([ Object(typeorm__WEBPACK_IMPORTED_MODULE_1__.Entity)() ], BUser);
    class BUserRepository extends typeorm__WEBPACK_IMPORTED_MODULE_1__.Repository {}
}, function(module, exports) {
    module.exports = require("base32-encode");
}, function(module, exports) {
    module.exports = require("set-utils");
}, function(module, exports) {
    module.exports = require("tough-cookie");
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, SAVE, can, ctx, isEditableField, isExtendableArrayField, isVisibleField, props) {
            const {sentenceCase} = __webpack_require__(37);
            let marked = __webpack_require__(38);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>";
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && `call_${v.remote.replace(".", "_")}`, !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && `call_${v.remote.replace(".", "_")}`, !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.component = pug_interp = function(v, i, row) {
                this && this.block, this && this.attributes;
                switch (row = row || "item", v.type) {
                  case "string":
                    pug_mixins.input(v, row, i);
                    break;

                  case "array":
                    pug_mixins.array(v, row, i);
                    break;

                  case "files":
                    pug_mixins.files(v, row, i);
                    break;

                  case "date":
                    pug_mixins.date(v, row, i);
                    break;

                  case "number":
                    pug_mixins.number(v, row, i);
                    break;

                  case "enum":
                    pug_mixins.enum(v, row, i);
                    break;

                  case "boolean":
                    pug_mixins.boolean(v, row, i);
                    break;

                  case "wysiwyg":
                    pug_mixins.wysiwyg(v, row, i);
                    break;

                  case "md-tgclient":
                    pug_mixins.code(v, row, i, "md-tgclient");
                    break;

                  case "md-tgbotlegacy":
                    pug_mixins.code(v, row, i, "md-tgbotlegacy");
                    break;

                  case "regex":
                    pug_mixins.code(v, row, i, "regex");
                    break;

                  case "javascript":
                    pug_mixins.mn(v, row, i, "javascript");
                    break;

                  case "link":
                    pug_mixins.link(v, row, i);
                    break;

                  default:
                    pug_html += "<span>Unsupported type</span>";
                }
            }, pug_html += '<div class="rg-data_single" v-if="!!values[0]">';
            let i = 0;
            (function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isVisibleField(v) && (pug_html = pug_html + "<div> <p>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</p>", 
                    pug_mixins.component(v, i, "values[0]"), pug_html += "</div>"), ++i;
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isVisibleField(v) && (pug_html = pug_html + "<div> <p>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</p>", 
                        pug_mixins.component(v, i, "values[0]"), pug_html += "</div>"), ++i;
                    }
                }
            }).call(this), can.PATCH && (pug_html = pug_html + '<p> <el-button @click="save(values[0])" type="primary"' + pug.attr("round", !0, !0, !0) + ' icon="el-icon-check"' + pug.attr(":loading", "values[0].state==" + SAVE, !0, !0) + ">Save</el-button></p>"), 
            pug_html += "</div>";
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "SAVE" in locals_for_with ? locals_for_with.SAVE : "undefined" != typeof SAVE ? SAVE : void 0, "can" in locals_for_with ? locals_for_with.can : "undefined" != typeof can ? can : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0, "isVisibleField" in locals_for_with ? locals_for_with.isVisibleField : "undefined" != typeof isVisibleField ? isVisibleField : void 0, "props" in locals_for_with ? locals_for_with.props : "undefined" != typeof props ? props : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, NONE, Object, REMOVE, SAVE, can, ctx, findPrimaryIndex, isEditableField, isExtendableArrayField, isFullwidthField, isNormalField, props) {
            const {sentenceCase} = __webpack_require__(37);
            let marked = __webpack_require__(38);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, $s = state => `(item.state==${state})`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            let display = null;
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                "table" === display ? (pug_html = pug_html + "<td" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></td>") : (pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>");
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && `call_${v.remote.replace(".", "_")}`, !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && `call_${v.remote.replace(".", "_")}`, !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.component = pug_interp = function(v, i, row) {
                this && this.block, this && this.attributes;
                switch (row = row || "item", v.type) {
                  case "string":
                    pug_mixins.input(v, row, i);
                    break;

                  case "array":
                    pug_mixins.array(v, row, i);
                    break;

                  case "files":
                    pug_mixins.files(v, row, i);
                    break;

                  case "date":
                    pug_mixins.date(v, row, i);
                    break;

                  case "number":
                    pug_mixins.number(v, row, i);
                    break;

                  case "enum":
                    pug_mixins.enum(v, row, i);
                    break;

                  case "boolean":
                    pug_mixins.boolean(v, row, i);
                    break;

                  case "wysiwyg":
                    pug_mixins.wysiwyg(v, row, i);
                    break;

                  case "md-tgclient":
                    pug_mixins.code(v, row, i, "md-tgclient");
                    break;

                  case "md-tgbotlegacy":
                    pug_mixins.code(v, row, i, "md-tgbotlegacy");
                    break;

                  case "regex":
                    pug_mixins.code(v, row, i, "regex");
                    break;

                  case "javascript":
                    pug_mixins.mn(v, row, i, "javascript");
                    break;

                  case "link":
                    pug_mixins.link(v, row, i);
                    break;

                  default:
                    pug_html += "<span>Unsupported type</span>";
                }
            }, pug_mixins.controlls = pug_interp = function() {
                var state;
                this && this.block, this && this.attributes;
                (can.CHANGE || can.DELETE) && (pug_html += '<td class="rg-data__small-cell"><el-button-group>', 
                can.CHANGE && (pug_html += "<keep-alive>", can.PATCH && (pug_html = pug_html + "<el-button" + pug.attr("v-if", $s(NONE), !0, !0) + ' @click="edit(item)" type="primary" icon="el-icon-edit"' + pug.attr(":disabled", $s(REMOVE), !0, !0) + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + ">                           </el-button>"), 
                pug_html = pug_html + "<el-button" + pug.attr("v-if", (state = NONE, `(item.state!=${state})`), !0, !0) + ' @click="save(item)" type="primary" icon="el-icon-check"' + pug.attr(":loading", $s(SAVE), !0, !0) + pug.attr(":disabled", $s(REMOVE), !0, !0) + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + "></el-button></keep-alive>"), 
                can.DELETE && (pug_html = pug_html + '<el-button @click="remove(item, index)" type="danger" icon="el-icon-delete"' + pug.attr(":disabled", $s(SAVE), !0, !0) + pug.attr(":loading", $s(REMOVE), !0, !0) + pug.attr("circle", can.CHANGE, !0, !0) + pug.attr("round", !can.CHANGE, !0, !0) + "></el-button>"), 
                pug_html += "</el-button-group></td>");
            }, pug_html += '<div class="rg-data" v-loading="!inited" ref="table"><table>';
            const hasNormalFields = Object.values(props).some(isNormalField), hasFullWidth = Object.values(props).some(isFullwidthField), hasNormalRow = can.CHANGE || can.DELETE || hasNormalFields;
            let i = 0;
            hasNormalRow && !hasFullWidth ? (pug_html = pug_html + '<tbody><tr class="rg-draggable" v-for="(item, index) in values"' + pug.attr(":key", "item[" + findPrimaryIndex(props) + "]", !0, !0) + ">       ", 
            display = "table", function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var v = $$obj[pug_index2];
                    isNormalField(v) && pug_mixins.component(v, i), ++i;
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        v = $$obj[pug_index2];
                        isNormalField(v) && pug_mixins.component(v, i), ++i;
                    }
                }
            }.call(this), pug_mixins.controlls(), pug_html += "</tr></tbody>") : (pug_html = pug_html + '<tbody class="rg-draggable" v-for="(item, index) in values"' + pug.attr(":key", "item[" + findPrimaryIndex(props) + "]", !0, !0) + "> ", 
            hasNormalRow && (pug_html += "<tr>       ", display = "table", i = 0, function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var v = $$obj[pug_index3];
                    isNormalField(v) && pug_mixins.component(v, i), ++i;
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        v = $$obj[pug_index3];
                        isNormalField(v) && pug_mixins.component(v, i), ++i;
                    }
                }
            }.call(this), pug_mixins.controlls(), pug_html += "</tr>"), hasFullWidth && (display = "single", 
            i = 0, function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isFullwidthField(v) && (pug_html = pug_html + '<tr><td colspan="999"><div class="rg-subtitle">' + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</div>", 
                    pug_mixins.component(v, i), pug_html += "</td></tr>"), ++i;
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isFullwidthField(v) && (pug_html = pug_html + '<tr><td colspan="999"><div class="rg-subtitle">' + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</div>", 
                        pug_mixins.component(v, i), pug_html += "</td></tr>"), ++i;
                    }
                }
            }.call(this)), pug_html += "</tbody>"), hasNormalFields && (pug_html += '<thead ref="tableHeader"><tr>', 
            function() {
                var $$obj = props;
                if ("number" == typeof $$obj.length) for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
                    var v = $$obj[key];
                    isNormalField(v) && (pug_html = pug_html + "<th>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</th>");
                } else {
                    $$l = 0;
                    for (var key in $$obj) {
                        $$l++;
                        v = $$obj[key];
                        isNormalField(v) && (pug_html = pug_html + "<th>" + (null == (pug_interp = v.description ? marked.inlineLexer(v.description, []) : sentenceCase(key)) ? "" : pug_interp) + "</th>");
                    }
                }
            }.call(this), (can.CHANGE || can.DELETE) && (pug_html += '<td class="rg-data__small-cell"><el-button-group v-if="values.length&gt;1">', 
            can.CHANGE && (pug_html = pug_html + '<el-button :disabled="!hasUnsaved" @click="saveAll" type="primary" icon="el-icon-check"' + pug.attr("circle", can.DELETE, !0, !0) + pug.attr("round", !can.DELETE, !0, !0) + "></el-button>"), 
            can.DELETE && (pug_html = pug_html + '<el-button @click="removeAll" type="danger" icon="el-icon-delete"' + pug.attr("circle", can.CHANGE, !0, !0) + pug.attr("round", !can.CHANGE, !0, !0) + "></el-button>"), 
            pug_html += "</el-button-group></td>"), pug_html += "</tr></thead>"), pug_html += '</table><div class="rg-spacer"></div>', 
            can.PUT && (pug_html += '<div class="rg-controlls-panel"><div class="rg-spacer"></div><el-button @click="add" type="primary" round icon="el-icon-plus">New</el-button></div>'), 
            pug_html += "</div>";
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "NONE" in locals_for_with ? locals_for_with.NONE : "undefined" != typeof NONE ? NONE : void 0, "Object" in locals_for_with ? locals_for_with.Object : "undefined" != typeof Object ? Object : void 0, "REMOVE" in locals_for_with ? locals_for_with.REMOVE : "undefined" != typeof REMOVE ? REMOVE : void 0, "SAVE" in locals_for_with ? locals_for_with.SAVE : "undefined" != typeof SAVE ? SAVE : void 0, "can" in locals_for_with ? locals_for_with.can : "undefined" != typeof can ? can : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "findPrimaryIndex" in locals_for_with ? locals_for_with.findPrimaryIndex : "undefined" != typeof findPrimaryIndex ? findPrimaryIndex : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0, "isFullwidthField" in locals_for_with ? locals_for_with.isFullwidthField : "undefined" != typeof isFullwidthField ? isFullwidthField : void 0, "isNormalField" in locals_for_with ? locals_for_with.isNormalField : "undefined" != typeof isNormalField ? isNormalField : void 0, "props" in locals_for_with ? locals_for_with.props : "undefined" != typeof props ? props : void 0), 
        pug_html;
    };
}, function(module, exports) {
    module.exports = require("global-agent");
}, function(module, exports) {
    module.exports = require("dns");
}, function(module, exports) {
    module.exports = require("http");
}, function(module, exports) {
    module.exports = require("koa-mount");
}, function(module, exports) {
    module.exports = require("koa-send");
}, function(module, exports) {
    module.exports = require("os");
}, function(module, exports) {
    module.exports = require("got");
}, function(module, exports) {
    module.exports = require("fastest-validator");
}, function(module, exports) {
    module.exports = require("fastest-validator/lib/rules/array");
}, function(module, exports) {
    module.exports = require("raw-body");
}, function(module, exports) {
    module.exports = require("url");
}, function(module, exports) {
    module.exports = require("crypto");
}, function(module, exports) {
    module.exports = require("node-machine-id");
}, function(module, exports) {
    module.exports = require("random-bytes-seed");
}, function(module, exports) {
    module.exports = require("random-seed");
}, function(module, exports) {
    module.exports = require("koa-compose");
}, function(module, exports) {
    module.exports = require("koa-session");
}, function(module, exports) {
    module.exports = require("passport-local");
}, function(module, exports) {
    module.exports = require("koa");
}, function(module, exports) {
    module.exports = require("alphanum-sort");
}, function(module, exports) {
    module.exports = require("fast-glob");
}, function(module, exports) {
    module.exports = require("time-stamp");
}, function(module, exports) {
    module.exports = require("appdirectory");
}, function(module, exports) {
    module.exports = require("make-dir");
}, function(module, exports) {
    module.exports = require("mem");
}, function(module, exports) {
    module.exports = require("striptags");
}, function(module, exports, __webpack_require__) {
    "use strict";
    var truncate = __webpack_require__(84), illegalRe = /[\/\?<>\\:\*\|"]/g, controlRe = /[\x00-\x1f\x80-\x9f]/g, reservedRe = /^\.+$/, windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i, windowsTrailingRe = /[\. ]+$/;
    function sanitize(input, replacement) {
        if ("string" != typeof input) throw new Error("Input must be string");
        var sanitized = input.replace(illegalRe, replacement).replace(controlRe, replacement).replace(reservedRe, replacement).replace(windowsReservedRe, replacement).replace(windowsTrailingRe, replacement);
        return truncate(sanitized, 255);
    }
    module.exports = function(input, options) {
        var replacement = options && options.replacement || "", output = sanitize(input, replacement);
        return "" === replacement ? output : sanitize(output, "");
    };
}, function(module, exports) {
    module.exports = require("tdl");
}, function(module, exports) {
    module.exports = require("tdl-tdlib-ffi");
}, function(module, exports) {
    module.exports = require("fs-cp");
}, function(module, exports) {
    module.exports = require("koa-compress");
}, function(module, exports) {
    module.exports = require("path-to-regexp");
}, function(module, exports, __webpack_require__) {
    var H = __webpack_require__(99);
    module.exports = function() {
        var T = new H.Template({
            code: function(c, p, i) {
                var t = this;
                return t.b(i = i || ""), t.b("window.Paths = Object.freeze({"), t.b("\n" + i), t.s(t.f("Paths", c, p, 1), c, p, 0, 45, 78, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                    t.b("    '"), t.b(t.t(t.f("name", c, p, 0))), t.b("':"), t.b(t.t(t.f("url", c, p, 0))), 
                    t.b(","), t.b("\n" + i);
                })), c.pop()), t.b("});"), t.b("\n" + i), t.b("window.UserInfo = Object.freeze({email:'"), 
                t.b(t.t(t.f("email", c, p, 0))), t.b("'});"), t.b("\n" + i), t.b("window.Flash = Object.freeze("), 
                t.b(t.t(t.f("Flash", c, p, 0))), t.b(");"), t.b("\n" + i), t.b("window.EDITOR_WORKER = '"), 
                t.b(t.t(t.f("EDITOR_WORKER", c, p, 0))), t.b("';"), t.b("\n" + i), t.b("window.TYPESCRIPT_WORKER = '"), 
                t.b(t.t(t.f("TYPESCRIPT_WORKER", c, p, 0))), t.b("';"), t.b("\n" + i), t.b("window.HasPty = "), 
                t.b(t.t(t.f("HasPty", c, p, 0))), t.b(";"), t.b("\n" + i), t.b("window.EntitiesFactory = function(mixins, debounce) {"), 
                t.b("\n" + i), t.b("    return ["), t.b("\n" + i), t.s(t.f("components", c, p, 1), c, p, 0, 404, 1317, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                    t.b("Object.assign({"), t.b("\n" + i), t.b("    name:'"), t.b(t.t(t.f("name", c, p, 0))), 
                    t.b("',"), t.b("\n" + i), t.b("    icon:'"), t.b(t.t(t.f("icon", c, p, 0))), t.b("',"), 
                    t.b("\n" + i), t.b("    order:"), t.b(t.t(t.f("order", c, p, 0))), t.b(","), t.b("\n" + i), 
                    t.b("    mixins:mixins,"), t.b("\n" + i), t.b("    data:function(){return{"), t.b("\n" + i), 
                    t.b("        draggableOptions:Object.freeze({disabled:!"), t.b(t.t(t.f("sortable", c, p, 0))), 
                    t.b(",draggable:'.rg-draggable',ghostClass:'rg-drag-ghost',animation:0}),"), t.b("\n" + i), 
                    t.b("        values:[],"), t.b("\n" + i), t.s(t.f("remote", c, p, 1), c, p, 0, 708, 755, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                        t.b("        "), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), t.b(t.t(t.f("method", c, p, 0))), 
                        t.b("_data:[],"), t.b("\n" + i);
                    })), c.pop()), t.b("        inited:false"), t.b("\n" + i), t.b("    }},"), t.b("\n" + i), 
                    t.b("    beforeCreate:function(){this.endPoint='"), t.b(t.t(t.f("endPoint", c, p, 0))), 
                    t.b("';this.hooks=Object.freeze("), t.b(t.t(t.f("hooks", c, p, 0))), t.b(");this.can=Object.freeze("), 
                    t.b(t.t(t.f("can", c, p, 0))), t.b(");this.rtti=Object.freeze("), t.b(t.t(t.f("rtti", c, p, 0))), 
                    t.b(");},"), t.b("\n" + i), t.b("    methods:{"), t.b("\n" + i), t.s(t.f("remote", c, p, 1), c, p, 0, 999, 1281, "{{ }}") && (t.rs(c, p, (function(c, p, t) {
                        t.b("        'call_"), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), t.b(t.t(t.f("method", c, p, 0))), 
                        t.b("':debounce(function(){"), t.b("\n" + i), t.b("            var self = this;"), 
                        t.b("\n" + i), t.b("            this.$rpc.remote('"), t.b(t.t(t.f("c", c, p, 0))), 
                        t.b("')."), t.b(t.t(t.f("method", c, p, 0))), t.b(".apply(this.$rpc,arguments).then(function(data){"), 
                        t.b("\n" + i), t.b("                self."), t.b(t.t(t.f("c", c, p, 0))), t.b("_"), 
                        t.b(t.t(t.f("method", c, p, 0))), t.b("_data=data;"), t.b("\n" + i), t.b("            });"), 
                        t.b("\n" + i), t.b("        }, 500),"), t.b("\n" + i);
                    })), c.pop()), t.b("    }"), t.b("\n" + i), t.b("}, "), t.b(t.t(t.f("render", c, p, 0))), 
                    t.b("),"), t.b("\n" + i);
                })), c.pop()), t.b("    ]"), t.b("\n" + i), t.b("};"), t.fl();
            },
            partials: {},
            subs: {}
        });
        return T.render.apply(T, arguments);
    };
}, function(module, exports) {
    module.exports = require("vue-template-compiler");
}, function(module, exports) {
    module.exports = require("vue-template-es2015-compiler");
}, , function(module, exports) {
    module.exports = require("pretty-error");
}, function(module, exports) {
    module.exports = require("node-pty");
}, function(module, exports) {
    module.exports = require("reflect-metadata");
}, function(module, exports, __webpack_require__) {
    "use strict";
    var truncate = __webpack_require__(85), getLength = Buffer.byteLength.bind(Buffer);
    module.exports = truncate.bind(null, getLength);
}, function(module, exports, __webpack_require__) {
    "use strict";
    function isHighSurrogate(codePoint) {
        return codePoint >= 55296 && codePoint <= 56319;
    }
    function isLowSurrogate(codePoint) {
        return codePoint >= 56320 && codePoint <= 57343;
    }
    module.exports = function truncate(getLength, string, byteLength) {
        if ("string" != typeof string) throw new Error("Input must be string");
        for (var codePoint, segment, charLength = string.length, curByteLength = 0, i = 0; i < charLength; i += 1) {
            if (codePoint = string.charCodeAt(i), segment = string[i], isHighSurrogate(codePoint) && isLowSurrogate(string.charCodeAt(i + 1)) && (i += 1, 
            segment += string[i]), curByteLength += getLength(segment), curByteLength === byteLength) return string.slice(0, i + 1);
            if (curByteLength > byteLength) return string.slice(0, i - segment.length + 1);
        }
        return string;
    };
}, function(module, exports, __webpack_require__) {
    var map = {
        "./base.pug": 87,
        "./baseform.pug": 88,
        "./checkpass.pug": 89,
        "./components/mixins.pug": 90,
        "./components/single.pug": 43,
        "./components/table.pug": 44,
        "./index.pug": 91,
        "./install.pug": 92,
        "./login.pug": 93,
        "./newpassword.pug": 94,
        "./resetemail.pug": 95,
        "./resetpass-sended.pug": 96,
        "./resetpass.pug": 97,
        "./submitform.pug": 98
    };
    function webpackContext(req) {
        var id = webpackContextResolve(req);
        return __webpack_require__(id);
    }
    function webpackContextResolve(req) {
        if (!__webpack_require__.o(map, req)) {
            var e = new Error("Cannot find module '" + req + "'");
            throw e.code = "MODULE_NOT_FOUND", e;
        }
        return map[req];
    }
    webpackContext.keys = function webpackContextKeys() {
        return Object.keys(map);
    }, webpackContext.resolve = webpackContextResolve, module.exports = webpackContext, 
    webpackContext.id = 86;
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(assetsUrl, favicon, scripts, styles, title) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html += "</head><body>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var s = $$obj[pug_index1];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index1];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, title, warning) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            title && (pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>"), 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html += "</form></div></div></div></div>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Repeate password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.passwordField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(EDIT, ctx, isEditableField, isExtendableArrayField) {
            const {sentenceCase} = __webpack_require__(37);
            let marked = __webpack_require__(38);
            marked.setOptions({
                xhtml: !0
            });
            const renderer = new marked.Renderer, linkRenderer = renderer.link;
            renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" '), 
            marked.setOptions({
                renderer
            });
            const $ = (row, i) => `${row}.v[${i}]`, $part = (row, i, p) => `${row}.v[${i}].split('|')[${p}]`, readonly = (v, row) => isEditableField(v) ? `(${row || "item"}.state!==${EDIT})` : "true";
            pug_mixins.baseBlock = pug_interp = function(v, row, fieldIndex) {
                var block = this && this.block;
                this && this.attributes;
                const error = row + ".errors[" + fieldIndex + "]";
                pug_html = pug_html + "<span" + pug.attr(":class", `[{'rg-field_readonly':${readonly(v, row)}},{'rg-field_error':${error}}]`, !0, !0) + ">", 
                block && block(), pug_html = pug_html + '<div class="rg-field__error">{{' + (null == (pug_interp = error) ? "" : pug_interp) + "}}  </div></span>";
            }, pug_mixins.files = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-upload-wrapper" + (pug.attr("action", ctx.resolve(v.uploadUrl), !0, !0) + pug.attr("accept", v.uploadAccept, !0, !0) + pug.attr(":limit", v.max, !0, !0) + pug.attr("title", v.placeholder, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0)) + "></el-upload-wrapper>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.enum = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && `call_${v.remote.replace(".", "_")}`, !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                                var el = $$obj[pug_index0];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index0 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index0];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.array = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-select" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("default-first-option", !0, !0, !0) + pug.attr("multiple", !0, !0, !0) + pug.attr("filterable", !0, !0, !0) + pug.attr("allow-create", isExtendableArrayField(v), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("remote", !!v.remote, !0, !0) + pug.attr(":remote-method", !!v.remote && `call_${v.remote.replace(".", "_")}`, !0, !0)) + ">", 
                        v.enumPairs && function() {
                            var $$obj = v.enumPairs;
                            if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                                var el = $$obj[pug_index1];
                                pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                            } else {
                                $$l = 0;
                                for (var pug_index1 in $$obj) {
                                    $$l++;
                                    el = $$obj[pug_index1];
                                    pug_html = pug_html + "<el-option" + (pug.attr("label", el[0], !0, !0) + pug.attr("value", el[1], !0, !0) + pug.attr("key", el[0], !0, !0)) + "></el-option>";
                                }
                            }
                        }.call(this), v.remote && (pug_html = pug_html + "<el-option" + pug.attr("v-for", `item in ${v.remote.replace(".", "_")}_data`, !0, !0) + ' :label="item&amp;&amp;item.label||item" :value="item&amp;&amp;item.value||item"></el-option>'), 
                        pug_html += "</el-select>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.number = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + '<el-input-number controls-position="right"' + pug.attr(":min", v.min, !0, !0) + pug.attr(":max", v.max, !0, !0) + pug.attr(":precision", !!v.integer && "0", !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":disabled", readonly(v, row), !0, !0) + "></el-input-number>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.input = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-input" + (pug.attr("placeholder", v.placeholder || "", !0, !0) + pug.attr(":clearable", !v.readonly && "!" + readonly(v, row), !0, !0) + pug.attr(":minlength", v.min, !0, !0) + pug.attr(":maxlength", v.max, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr("autosize", !0, !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></el-input>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.wysiwyg = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<wysiwyg" + (pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":class", `{'rg-wysiwyg_readonly':${readonly(v, row)}}`, !0, !0)) + "></wysiwyg>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.code = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<tgmd" + (pug.attr("mode", mode, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></tgmd>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.mn = pug_interp = function(v, row, fieldIndex, mode) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<monaco" + (pug.attr("mode", mode, !0, !0) + pug.attr("extraLib", v.extraLib, !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0)) + "></monaco>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.boolean = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-checkbox" + (pug.attr(":disabled", readonly(v, row), !0, !0) + pug.attr("v-model", $(row, fieldIndex), !0, !0)) + "></el-checkbox>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.date = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = pug_html + "<el-date-picker" + pug.attr("placeholder", v.placeholder || "", !0, !0) + ' type="datetime"' + pug.attr("v-model", $(row, fieldIndex), !0, !0) + pug.attr(":readonly", readonly(v, row), !0, !0) + "></el-date-picker>";
                    }
                }, v, row, fieldIndex);
            }, pug_mixins.link = pug_interp = function(v, row, fieldIndex) {
                this && this.block, this && this.attributes;
                pug_mixins.baseBlock.call({
                    block: function() {
                        pug_html = v.placeholder ? pug_html + "<a" + pug.attr(":href", $(row, fieldIndex), !0, !0) + ' target="_blank">' + pug.escape(null == (pug_interp = v.placeholder) ? "" : pug_interp) + "</a>" : pug_html + "<a" + pug.attr(":href", $part(row, fieldIndex, 0), !0, !0) + ' target="_blank">{{ ' + (null == (pug_interp = $part(row, fieldIndex, 1)) ? "" : pug_interp) + " }}</a>";
                    }
                }, v, row, fieldIndex);
            };
        }.call(this, "EDIT" in locals_for_with ? locals_for_with.EDIT : "undefined" != typeof EDIT ? EDIT : void 0, "ctx" in locals_for_with ? locals_for_with.ctx : "undefined" != typeof ctx ? ctx : void 0, "isEditableField" in locals_for_with ? locals_for_with.isEditableField : "undefined" != typeof isEditableField ? isEditableField : void 0, "isExtendableArrayField" in locals_for_with ? locals_for_with.isExtendableArrayField : "undefined" != typeof isExtendableArrayField ? isExtendableArrayField : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(assetsUrl, favicon, inlineScript, scripts, styles, title) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html += '</head><body><div id="app"></div>', inlineScript && (pug_html = pug_html + "<script>" + (null == (pug_interp = inlineScript) ? "" : pug_interp) + "<\/script>"), 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var s = $$obj[pug_index1];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index1];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "inlineScript" in locals_for_with ? locals_for_with.inlineScript : "undefined" != typeof inlineScript ? inlineScript : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Installing";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField("Your email address (for password recovery)"), 
            pug_mixins.passwordField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = "Login") ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = "Login") ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = "Login".toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField(), pug_mixins.passwordField(), pug_html += '<div class="form-group"><a href="resetpass" style="width:100%;text-align:center;display:inline-block;text-decoration:none;">Forgot password?</a></div><br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.passwordField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="password">Password</label><input class="form-control underlined" type="password" name="password"' + pug.attr("placeholder", placeholder || "Password (5-70 symbols)", !0, !0) + ' pattern="^\\S.{3,65}\\S$"' + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "New password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.passwordField("New password (5-70 symbols)"), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "New email";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField("New email"), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, service, styles, warning) {
            pug_html += "<!DOCTYPE html><html>";
            const title = "Reset password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html = pug_html + '<div class="form-group">Письмо с инструкциями по изменению пароля отправлено на ' + pug.escape(null == (pug_interp = email) ? "" : pug_interp) + "</div>", 
            service && (pug_html = pug_html + '<div class="form-group"><a class="btn btn-block btn-primary btn-lg"' + pug.attr("href", service.url, !0, !0) + ">Открыть " + pug.escape(null == (pug_interp = service.title) ? "" : pug_interp) + "</a></div>"), 
            pug_html += "</form></div></div></div></div>", scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "service" in locals_for_with ? locals_for_with.service : "undefined" != typeof service ? service : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", pug_mixins = {}, locals_for_with = locals || {};
        return function(action, assetsUrl, email, errors, favicon, message, scripts, styles, warning) {
            pug_mixins.emailField = pug_interp = function(placeholder) {
                this && this.block, this && this.attributes;
                pug_html = pug_html + '<div class="form-group"><label for="email">Email</label><input class="form-control underlined" type="email" name="email"' + pug.attr("placeholder", placeholder || "Email address", !0, !0) + pug.attr("required", !0, !0, !0) + pug.attr("autofocus", !0, !0, !0) + pug.attr("value", email || "", !0, !0) + "></div>";
            }, pug_html += "<!DOCTYPE html><html>";
            const title = "Reset password";
            pug_html += '<head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>", 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_mixins.emailField(), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "email" in locals_for_with ? locals_for_with.email : "undefined" != typeof email ? email : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports, __webpack_require__) {
    var pug = __webpack_require__(8);
    module.exports = function template(locals) {
        var pug_interp, pug_html = "", locals_for_with = locals || {};
        return function(action, assetsUrl, errors, favicon, message, scripts, styles, title, warning) {
            pug_html += '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="robots" content="noindex,nofollow">', 
            favicon && (pug_html = pug_html + '<link rel="icon"' + pug.attr("href", assetsUrl + "/" + favicon, !0, !0) + ' type="image/x-icon">'), 
            pug_html = pug_html + "<title>" + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + "</title>", 
            styles && function() {
                var $$obj = styles;
                if ("number" == typeof $$obj.length) for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
                    var s = $$obj[pug_index0];
                    s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                } else {
                    $$l = 0;
                    for (var pug_index0 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index0];
                        s && (pug_html = pug_html + '<link rel="stylesheet"' + pug.attr("href", assetsUrl + "/" + s, !0, !0) + ">");
                    }
                }
            }.call(this), pug_html = pug_html + '</head><body><div class="auth"><div class="auth-container"><div class="card"><header class="auth-header"><h1 class="auth-title"><div class="logo"><span class="l l1"></span><span class="l l2"></span><span class="l l3"></span><span class="l l4"></span><span class="l l5"></span></div>' + pug.escape(null == (pug_interp = title) ? "" : pug_interp) + '</h1></header><div class="auth-content">', 
            title && (pug_html = pug_html + '<p class="text-center">' + pug.escape(null == (pug_interp = title.toUpperCase()) ? "" : pug_interp) + "</p>"), 
            pug_html = pug_html + "<form" + pug.attr("action", action || "?", !0, !0) + ' method="POST">', 
            errors && function() {
                var $$obj = errors;
                if ("number" == typeof $$obj.length) for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
                    var e = $$obj[pug_index1];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index1 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index1];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-danger">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), warning && function() {
                var $$obj = warning;
                if ("number" == typeof $$obj.length) for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
                    var e = $$obj[pug_index2];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index2 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index2];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-warning">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), message && function() {
                var $$obj = message;
                if ("number" == typeof $$obj.length) for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
                    var e = $$obj[pug_index3];
                    pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                } else {
                    $$l = 0;
                    for (var pug_index3 in $$obj) {
                        $$l++;
                        e = $$obj[pug_index3];
                        pug_html = pug_html + '<div class="form-group"><div class="alert alert-info">' + pug.escape(null == (pug_interp = e) ? "" : pug_interp) + "</div></div>";
                    }
                }
            }.call(this), pug_html += '<br><div class="form-group"><button class="btn btn-block btn-primary btn-lg" type="submit">OK</button></div></form></div></div></div></div>', 
            scripts && function() {
                var $$obj = scripts;
                if ("number" == typeof $$obj.length) for (var pug_index4 = 0, $$l = $$obj.length; pug_index4 < $$l; pug_index4++) {
                    var s = $$obj[pug_index4];
                    s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                } else {
                    $$l = 0;
                    for (var pug_index4 in $$obj) {
                        $$l++;
                        s = $$obj[pug_index4];
                        s && (pug_html = pug_html + "<script" + pug.attr("src", assetsUrl + "/" + s, !0, !0) + "><\/script>");
                    }
                }
            }.call(this), pug_html += "</body></html>";
        }.call(this, "action" in locals_for_with ? locals_for_with.action : "undefined" != typeof action ? action : void 0, "assetsUrl" in locals_for_with ? locals_for_with.assetsUrl : "undefined" != typeof assetsUrl ? assetsUrl : void 0, "errors" in locals_for_with ? locals_for_with.errors : "undefined" != typeof errors ? errors : void 0, "favicon" in locals_for_with ? locals_for_with.favicon : "undefined" != typeof favicon ? favicon : void 0, "message" in locals_for_with ? locals_for_with.message : "undefined" != typeof message ? message : void 0, "scripts" in locals_for_with ? locals_for_with.scripts : "undefined" != typeof scripts ? scripts : void 0, "styles" in locals_for_with ? locals_for_with.styles : "undefined" != typeof styles ? styles : void 0, "title" in locals_for_with ? locals_for_with.title : "undefined" != typeof title ? title : void 0, "warning" in locals_for_with ? locals_for_with.warning : "undefined" != typeof warning ? warning : void 0), 
        pug_html;
    };
}, function(module, exports) {
    module.exports = require("hogan.js");
}, function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    var external_tslib_ = __webpack_require__(0), external_child_process_ = __webpack_require__(22), external_global_agent_ = __webpack_require__(45), config = __webpack_require__(3);
    const DEV_NULL = process.platform.startsWith("win") ? "nul" : "/dev/null", cmd = "grep -i '^Port\\|^BasicAuth' /etc/tinyproxy/tinyproxy.conf", {Port, BasicAuth} = JSON.parse("{" + Object(external_child_process_.execSync)(config.e ? `ssh -o UserKnownHostsFile=${DEV_NULL} -o StrictHostKeyChecking=no -o BatchMode=yes -i "${config.e}" root@${config.g} "${cmd}"` : cmd, {
        encoding: "utf-8",
        windowsHide: !0
    }).trim().split("\n").map(v => v.replace(/\s+/, ":").replace(/([^:]+)/g, '"$1"')) + "}"), PROXY_HOST = config.g, PROXY_PORT = Port, PROXY_AUTH = BasicAuth.replace(/\s+/, ":"), PROXY_URL = `http://${PROXY_AUTH}@${PROXY_HOST}:${PROXY_PORT}`;
    if ("object" != typeof v8debug && !/--debug|--inspect/.test(process.execArgv.join(" "))) {
        const PrettyError = __webpack_require__(81), pe = new PrettyError;
        pe.withoutColors(), pe.start();
    }
    config.e ? (global.SSH_TUNNEL = Object(external_child_process_.spawn)("ssh", [ "-o", "UserKnownHostsFile=" + DEV_NULL, "-o", "StrictHostKeyChecking=no", "-o", "BatchMode=yes", "-gnNT", "-R", config.f + ":localhost:" + config.f, "-i", config.e, "root@" + config.g ], {
        windowsHide: !0,
        stdio: "inherit"
    }).once("exit", code => process.exit(code)), Object(external_global_agent_.bootstrap)(), 
    global.GLOBAL_AGENT.NO_PROXY = "localhost,127.0.0.1,ipecho.net,ident.me,icanhazip.com", 
    console.log.bind(console, "(./src/core/preload.ts:24)")("Active proxy url: " + (global.GLOBAL_AGENT.HTTP_PROXY = PROXY_URL))) : console.log.bind(console, "(./src/core/preload.ts:26)")(`Proxy url: ${PROXY_URL}`);
    var external_dns_ = __webpack_require__(46), external_http_ = __webpack_require__(47), external_koa_mount_ = __webpack_require__(48), external_koa_mount_default = __webpack_require__.n(external_koa_mount_), external_koa_send_ = __webpack_require__(49), external_koa_send_default = __webpack_require__.n(external_koa_send_), external_path_ = __webpack_require__(12), external_timers_ = __webpack_require__(31), external_typescript_ioc_ = __webpack_require__(1), external_util_ = __webpack_require__(32), external_ws_ = __webpack_require__(25), external_ws_default = __webpack_require__.n(external_ws_), string_tools_ = __webpack_require__(4), external_os_ = __webpack_require__(50);
    const shell = Object(external_os_.platform)().includes("win") ? "cmd.exe" : function hasTmux() {
        try {
            return Object(external_child_process_.execSync)("command -v tmux", {
                windowsHide: !0
            }), !0;
        } catch {
            return !1;
        }
    }() ? "tmux" : "sh";
    let spawn = null;
    try {
        spawn = __webpack_require__(82).spawn;
    } catch {}
    const AVALIBLE = !!spawn;
    var external_msgpack_lite_ = __webpack_require__(36);
    const opts = {
        codec: Object(external_msgpack_lite_.createCodec)({
            preset: !0,
            safe: !1,
            useraw: !1,
            int64: !1,
            binarraybuffer: !0,
            uint8array: !1,
            usemap: !1
        })
    };
    function decode(data) {
        return Object(external_msgpack_lite_.decode)(data, opts);
    }
    function encode(o) {
        return Object(external_msgpack_lite_.encode)(o, opts);
    }
    var rnd_id = __webpack_require__(33);
    class RPC_AnswerChain extends class Chain {
        write(o) {
            this.readResolve && (this.readResolve(o), this.readResolve = null);
        }
        read() {
            return new Promise(resolve => this.readResolve = resolve);
        }
    } {
        constructor(i, timeout) {
            super(), this.i = i, this.timeout = timeout, this.created = Date.now();
        }
    }
    const CALLBACK_SOURCES = new Map, CALLS = new Map, chainDropInterval = setInterval(() => {
        const now = Date.now();
        CALLS.forEach(c => {
            c.timeout && now - c.created > c.timeout && c.write({
                i: c.i,
                e: "RPC call timeout"
            });
        });
    }, 1e4);
    chainDropInterval.unref();
    class RPC_RemoteHandler {
        constructor(key, rpc, timeout) {
            this.key = key, this.rpc = rpc, this.timeout = timeout;
        }
        get(_, f) {
            return (...a) => {
                let i = 0;
                do {
                    i = Object(rnd_id.a)();
                } while (CALLS.has(i));
                const h = new RPC_AnswerChain(i, 1e3 * this.timeout);
                try {
                    CALLS.set(i, h), this.rpc.send(encode({
                        i,
                        c: this.key,
                        f,
                        a
                    }));
                } catch (e) {
                    throw CALLS.delete(i), e;
                }
                return h.read().then(answer => {
                    if (null != answer.e) throw answer.e;
                    return answer.r;
                }).finally(() => CALLS.delete(i));
            };
        }
    }
    const EMPTY_OBJECT = Object.freeze(Object.create(null));
    const RPCServers = new Map;
    function pingServers(timeout) {
        RPCServers.forEach(m => m.forEach(rpc => rpc.ping(timeout)));
    }
    function fromCtx(ctx) {
        return RPCServers.get(ctx.state.user.id).get(parseInt(ctx.headers["x-tab"]));
    }
    class ServerRPCHandler {
        constructor(server) {
            this.server = server;
        }
        get(target, prop) {
            return "__rpc" == prop ? this.server : target[prop];
        }
    }
    class ServerRPC_ServerRPC extends class RPC_RPC {
        static register(key, o) {
            CALLBACK_SOURCES.set(key, o);
        }
        static unregister(key) {
            CALLBACK_SOURCES.delete(key);
        }
        remote(key, timeout) {
            return new Proxy(EMPTY_OBJECT, new RPC_RemoteHandler(key, this, timeout || 0));
        }
        async process(message) {
            if (message) if (message.constructor === String) this.term && this.term.write(message); else {
                const data = decode(new Uint8Array(message, 0));
                if (data.f) {
                    const {i, f, c, a} = data;
                    try {
                        if ("RPC" === c) {
                            if ("enableTerminal" !== f) throw `Deny method [${f}] called on RPC`;
                            this.send(encode({
                                i,
                                r: this.enableTerminal(a[0])
                            }));
                        } else this.send(encode({
                            i,
                            r: await this.rcall(CALLBACK_SOURCES.get(c), f, a)
                        }));
                    } catch (e) {
                        this.send(encode({
                            i,
                            e
                        }));
                    }
                } else CALLS.get(data.i).write(data), CALLS.delete(data.i);
            }
        }
    } {
        constructor(socket, tab, browserId, user, terminalSize) {
            if (super(), this.socket = socket, this.tab = tab, this.browserId = browserId, this.user = user, 
            this.lastActive = Date.now(), this.onWSMessage = message => {
                this.lastActive = Date.now(), this.process(message).catch(console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:49)"));
            }, this.onWSClose = () => {
                try {
                    this.dispose();
                } catch (err) {
                    console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:56)")(err);
                }
            }, this.onWSPong = () => {
                this.lastActive = Date.now();
            }, tab !== +tab || !tab) throw "Invalid websocket tab: " + Object(string_tools_.stringify)(tab);
            let remoteTabs = RPCServers.get(user.id);
            remoteTabs || RPCServers.set(user.id, remoteTabs = new Map), remoteTabs.set(tab, this), 
            socket.on("close", this.onWSClose), socket.on("pong", this.onWSPong), socket.on("message", this.onWSMessage), 
            terminalSize && this.enableTerminal(terminalSize);
        }
        getActual() {
            var _a;
            const r = null === (_a = RPCServers.get(this.user.id)) || void 0 === _a ? void 0 : _a.get(this.tab);
            return r !== this && this.dispose(), r;
        }
        send(data) {
            var _a;
            null === (_a = this.getActual()) || void 0 === _a || _a.socket.send(data);
        }
        enableTerminal(terminalSize) {
            if (this.term) this.term.resize(terminalSize.cols, terminalSize.rows); else {
                const term = function create(options) {
                    return AVALIBLE ? spawn(shell, "tmux" === shell ? [ "new", "-A", "-s", "subscribe-alert-bot-" + config.f ] : [], {
                        name: "xterm-color",
                        cwd: process.env.HOME,
                        env: process.env,
                        ...options
                    }) : null;
                }(terminalSize);
                term && (this.term = term, this.socket.send(String.fromCharCode(27) + "c"), term.onData(data => {
                    try {
                        this.socket.send(data);
                    } catch (err) {
                        console.error.bind(console, "(./src/core/rpc/ServerRPC.ts:116)")(err);
                    }
                }));
            }
        }
        rcall(o, fname, args) {
            return o[fname].apply(new Proxy(o, new ServerRPCHandler(this)), args);
        }
        dispose() {
            this.term && (this.term.kill(), this.term = void 0);
            const c = RPCServers.get(this.user.id);
            (null == c ? void 0 : c.get(this.tab)) === this && c.delete(this.tab), this.socket.readyState != external_ws_default.a.CLOSING && this.socket.readyState != external_ws_default.a.CLOSED && this.socket.close();
        }
        ping(timeout) {
            Date.now() - this.lastActive >= timeout && this.socket.readyState === external_ws_default.a.OPEN && this.socket.ping();
        }
    }
    var external_net_ = __webpack_require__(24), http = __webpack_require__(18), get_my_ip = async function() {
        let ip = "";
        try {
            if (ip = (await Object(http.a)("http://ipecho.net/plain").text()).trim(), !Object(external_net_.isIP)(ip)) throw null;
        } catch {
            try {
                if (ip = (await Object(http.a)("http://ident.me").text()).trim(), !Object(external_net_.isIP)(ip)) throw null;
            } catch {
                if (ip = (await Object(http.a)("http://icanhazip.com").text()).trim(), !Object(external_net_.isIP)(ip)) throw "Invalid ip: " + ip;
            }
        }
        return ip;
    }, types = __webpack_require__(5), ClientSettings = __webpack_require__(13), external_raw_body_ = __webpack_require__(54), external_raw_body_default = __webpack_require__.n(external_raw_body_);
    var body_parse_msgpack = async (ctx, next) => {
        const {method} = ctx.request;
        if ("PATCH" === method || "PUT" === method || "POST" === method) {
            const body = await external_raw_body_default()(ctx.req, {
                limit: 5242880
            });
            ctx.request.body = Object.assign(body && body.length > 0 ? decode(body) : null, ctx.request.query);
        }
        return next();
    }, is_ajax = function(ctx) {
        return "XMLHttpRequest" === ctx.request.headers["x-requested-with"];
    }, check_entity_access = ({checkAccess}) => function(ctx, next) {
        if (!is_ajax(ctx)) throw 403;
        return checkAccess(ctx.request.method, ctx.state.user), next();
    }, external_url_ = __webpack_require__(55), resolve_url = function(ctx, target, localhost) {
        return Object(external_url_.resolve)(ctx.protocol + "://" + (localhost && !ctx.secure ? "127.0.0.1:" + config.f : ctx.host), target.replace(/[\/\\]{2,}/g, "/"));
    };
    function setNoStoreHeader(ctx) {
        ctx.set("Cache-Control", "no-store, no-cache, max-age=0");
    }
    var no_store = function(ctx, next) {
        return setNoStoreHeader(ctx), next();
    };
    const routsCache = Object.create(null), secureRoutsCache = Object.create(null);
    var smart_redirect = (ctx, next) => {
        const rcache = ctx.secure ? secureRoutsCache : routsCache;
        return ctx.resolve = (routeName, params) => !params && ctx.router.stack.find(r => r.name === routeName).paramNames.length < 1 ? rcache[routeName] || (rcache[routeName] = resolve_url(ctx, ctx.router.url(routeName, null))) : resolve_url(ctx, ctx.router.url(routeName, Object.assign({}, ctx.params, params))), 
        ctx.namedRedirect = (routName, delay, params) => {
            setNoStoreHeader(ctx);
            const target = ctx.resolve(routName || "root", params);
            delay > 0 ? (ctx.type = "html", ctx.body = `<html><head><meta http-equiv="refresh" content="${delay};url=${target}" /></head></html>`) : ctx.redirect(target);
        }, next();
    }, external_etag_ = __webpack_require__(34), external_etag_default = __webpack_require__.n(external_etag_), external_koa_router_ = __webpack_require__(15), external_koa_router_default = __webpack_require__.n(external_koa_router_), external_param_case_ = __webpack_require__(23), external_pluralize_ = __webpack_require__(35), external_pluralize_default = __webpack_require__.n(external_pluralize_), external_typeorm_ = __webpack_require__(2), ioc = __webpack_require__(14);
    const entities = [], repositories = [], storage = Object(external_typeorm_.getMetadataArgsStorage)(), {Settings, SettingsRepository} = __webpack_require__(16);
    Settings && storage.filterTables(Settings).length > 0 && (entities.push(Settings), 
    SettingsRepository && repositories.push(Object(ioc.b)(Settings)(SettingsRepository)));
    const {User, UserRepository} = __webpack_require__(6);
    User && storage.filterTables(User).length > 0 && (entities.push(User), UserRepository && repositories.push(Object(ioc.b)(User)(UserRepository)));
    const {BUser, BUserRepository} = __webpack_require__(39);
    BUser && storage.filterTables(BUser).length > 0 && (entities.push(BUser), BUserRepository && repositories.push(Object(ioc.b)(BUser)(BUserRepository)));
    const {ClientSettings: generated_ClientSettings, ClientSettingsRepository} = __webpack_require__(13);
    generated_ClientSettings && storage.filterTables(generated_ClientSettings).length > 0 && (entities.push(generated_ClientSettings), 
    ClientSettingsRepository && repositories.push(Object(ioc.b)(generated_ClientSettings)(ClientSettingsRepository)));
    const entities_entities = entities, entities_repositories = repositories;
    var external_bcryptjs_ = __webpack_require__(17), external_koa_compose_ = __webpack_require__(60), external_koa_compose_default = __webpack_require__.n(external_koa_compose_), external_koa_passport_ = __webpack_require__(19), external_koa_passport_default = __webpack_require__.n(external_koa_passport_), external_koa_session_ = __webpack_require__(61), external_koa_session_default = __webpack_require__.n(external_koa_session_), external_passport_local_ = __webpack_require__(62), entities_User = __webpack_require__(6);
    const DEFAULT_COOKIE_OPTIONS = Object.freeze({
        maxAge: 18e4,
        secure: !1,
        httpOnly: !0,
        sameSite: "lax",
        signed: !1,
        overwrite: !0
    }), DELETE_COOKIE_OPTIONS = Object.freeze({
        ...DEFAULT_COOKIE_OPTIONS,
        maxAge: void 0,
        expires: new Date(-1),
        signed: !1
    });
    function deleteCookie(name, ctx, signed) {
        ctx.cookies.set(name, "", DELETE_COOKIE_OPTIONS), signed && ctx.cookies.set(name + ".sig", "", DELETE_COOKIE_OPTIONS);
    }
    var external_koa_ = __webpack_require__(63), external_koa_default = __webpack_require__.n(external_koa_), entities_Settings = __webpack_require__(16);
    let Koa_Koa = class Koa extends external_koa_default.a {
        constructor(settings) {
            super(), this.proxy = !0, this.keys = [ settings.secret ];
        }
    };
    Koa_Koa = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ entities_Settings.Settings ]) ], Koa_Koa);
    function sessionSig(ctx) {
        return ctx.cookies.get("_s.sig");
    }
    function destroySession(ctx) {
        const id = ctx.state.user && ctx.state.user.id, bId = sessionSig(ctx);
        id && ctx.res.once("finish", () => function destroyByUserId(id, browserId) {
            const m = RPCServers.get(id);
            m && (browserId ? m.forEach(v => v.browserId == browserId && v.dispose()) : m.forEach(v => v.dispose()));
        }(id, bId)), ctx.session = null;
    }
    let AuthRegister_AuthRegister = class AuthRegister {
        constructor(userRepository, app) {
            external_koa_passport_default.a.serializeUser((user, done) => {
                done(null, user.id);
            }), external_koa_passport_default.a.deserializeUser((id, done) => {
                userRepository.findOneOrFail(id).then(user => done(null, user)).catch(err => done(err, !1));
            }), external_koa_passport_default.a.use(new external_passport_local_.Strategy({
                usernameField: "email",
                passwordField: "password"
            }, async (email, password, done) => {
                try {
                    email = Object(string_tools_.trim)(email), password = Object(string_tools_.trim)(password);
                    const user = await userRepository.findOneOrFail({
                        email
                    });
                    await Object(external_bcryptjs_.compare)(password, user.password) ? done(null, user) : done(null, !1);
                } catch (err) {
                    done(err, !1);
                }
            })), this.session = external_koa_session_default()({
                ...DEFAULT_COOKIE_OPTIONS,
                key: "_s",
                maxAge: 31536e6,
                renew: !0,
                signed: !0
            }, app), this.passport = external_koa_passport_default.a.initialize(), this.passportSession = external_koa_passport_default.a.session(), 
            this.passportComposed = external_koa_compose_default()([ this.session, this.passport, this.passportSession ]);
        }
    };
    AuthRegister_AuthRegister = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(1, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ entities_User.UserRepository, Koa_Koa ]) ], AuthRegister_AuthRegister);
    var external_alphanum_sort_ = __webpack_require__(64), external_alphanum_sort_default = __webpack_require__.n(external_alphanum_sort_), external_fast_glob_ = __webpack_require__(65), external_fast_glob_default = __webpack_require__.n(external_fast_glob_), external_fs_ = __webpack_require__(11), external_time_stamp_ = __webpack_require__(66), external_time_stamp_default = __webpack_require__.n(external_time_stamp_), singleton_providers_factory = __webpack_require__(28);
    let ValidateSubscriber = class ValidateSubscriber {
        async beforeInsert({entity}) {
            const f = entity.constructor.insertValidate;
            f && await f(entity);
        }
        async beforeUpdate({entity}) {
            const f = entity.constructor.updateValidate;
            f && await f(entity);
        }
    };
    ValidateSubscriber = Object(external_tslib_.__decorate)([ Object(external_typeorm_.EventSubscriber)() ], ValidateSubscriber);
    var generated = [ ValidateSubscriber ], subscribers = generated, external_appdirectory_ = __webpack_require__(67), external_appdirectory_default = __webpack_require__.n(external_appdirectory_), external_make_dir_ = __webpack_require__(68);
    let cwd = process.cwd();
    const data_path_path = function createDataDir(path) {
        try {
            return Object(external_make_dir_.sync)(path), path;
        } catch (err) {
            return console.error.bind(console, "(./src/core/utils/data-path.ts:14)")(err), path.startsWith(cwd) ? (path = path.slice(cwd.length), 
            cwd = Object(external_path_.join)(cwd, "./../"), createDataDir(Object(external_path_.join)(cwd, "/", path))) : path;
        }
    }(Object(external_path_.normalize)(new external_appdirectory_default.a({
        appName: "subscribe-alert-bot-" + config.f,
        useRoaming: !0
    }).userData()));
    console.log("App directory: " + data_path_path);
    var data_path = function(...pathParts) {
        return pathParts.length > 0 ? Object(external_path_.join)(data_path_path, "/", ...pathParts) : data_path_path;
    };
    function createDBFilename() {
        return "subscribe-alert-bot." + external_time_stamp_default()("YYYY.MM.DD[HH.mm.ss]") + ".sqlite3";
    }
    async function getConnection(database, migrations) {
        const hasMigrations = Array.isArray(migrations) && migrations.length > 0, connection = await Object(external_typeorm_.createConnection)({
            type: "sqlite",
            database,
            synchronize: !hasMigrations && !Object(external_fs_.existsSync)(database),
            logging: [ "error", "migration" ],
            entities: entities_entities,
            subscribers,
            migrationsRun: hasMigrations,
            migrations
        });
        return Object.defineProperty(connection, "filename", {
            value: database
        }), connection;
    }
    let sqliteFilename = null;
    const provider = Object(singleton_providers_factory.b)(async () => {
        const alldb = external_alphanum_sort_default()(await external_fast_glob_default()("subscribe-alert-bot.*].sqlite3", {
            cwd: data_path(),
            absolute: !0,
            extglob: !1,
            braceExpansion: !1,
            caseSensitiveMatch: !1,
            baseNameMatch: !0,
            globstar: !1
        })).reverse();
        sqliteFilename = alldb.find(f => {
            const stat = Object(external_fs_.statSync)(f);
            return stat.size > 4096;
        }) || data_path(createDBFilename());
        try {
            await Promise.all(alldb.map(f => f !== sqliteFilename && external_fs_.promises.unlink(f)));
        } catch (err) {
            console.error.bind(console, "(./src/core/DBConnection.ts:80)")(err);
        }
        let connection = await getConnection(sqliteFilename);
        const migrations = await async function generateMigrations(connection) {
            const {upQueries, downQueries} = await connection.driver.createSchemaBuilder().log();
            if (upQueries.length > 0) {
                const migration = new Function(`return function _${Date.now()}() { }`)(), s = upQueries.reduce((s, q) => s + +q.query.startsWith("DROP INDEX "), 0);
                if (s !== upQueries.length / 2 || s !== upQueries.reduce((s, q) => s + +q.query.startsWith("CREATE INDEX "), 0)) return migration.prototype.up = async function(queryRunner) {
                    console.log("Start migration:");
                    for (let {query} of upQueries) await queryRunner.query(query);
                }, migration.prototype.down = async function(queryRunner) {
                    console.log("Rollback migration:");
                    for (let {query} of downQueries) await queryRunner.query(query);
                }, [ migration ];
            }
            return [];
        }(connection);
        migrations.length > 0 && (await connection.close(), connection = await getConnection(sqliteFilename, migrations)), 
        entities_repositories.forEach(r => r.provider.init(connection));
        for (let e of entities_entities) e.asyncProvider && await e.asyncProvider.init(connection);
        return connection;
    }), dbInit = provider.init;
    let DBConnection_DBConnection = class DBConnection extends external_typeorm_.Connection {};
    async function data2Model(ctx, model, repository) {
        const {body} = ctx.request, {props} = model.constructor.rtti;
        for (let k in body) {
            const p = props[k];
            p && !Object(types.a)(p, 34) && (model[k] = body[k]);
        }
        model.beforeFilter && await model.beforeFilter();
        const o = await repository.save(model);
        return o.afterFilter && await o.afterFilter(), o;
    }
    function msgpack(ctx, data) {
        ctx.type = "application/x-msgpack";
        const body = Array.isArray(data) ? data.length > 0 ? encode({
            fields: Object.keys(data[0]),
            values: data.map(v => Object.values(v))
        }) : null : null != data ? encode(data) : null;
        if ("GET" === ctx.method) {
            const et = body && external_etag_default()(body);
            if (et) {
                if (ctx.set("Cache-Control", "no-cache, max-age=31536000"), ctx.set("ETag", et), 
                ctx.headers["if-none-match"] && ctx.headers["if-none-match"].endsWith(et)) return void (ctx.status = 304);
            } else setNoStoreHeader(ctx);
        }
        ctx.body = body;
    }
    DBConnection_DBConnection = Object(external_tslib_.__decorate)([ Object(external_typescript_ioc_.Provided)(provider) ], DBConnection_DBConnection);
    let ApiRouter_ApiRouter = class ApiRouter extends external_koa_router_default.a {
        constructor(connection, {session, passport, passportSession}) {
            super(), this.use(session, passport, passportSession);
            for (let e of entities_entities) {
                const repository = connection.getRepository(e), access = check_entity_access(e), name = Object(external_param_case_.paramCase)(external_pluralize_default()(e.name)), singltonInstance = e.asyncProvider ? external_typescript_ioc_.Container.get(e) : null, saveModel = singltonInstance ? async (ctx, model) => {
                    const o = await data2Model(ctx, model, repository), {props} = e.rtti;
                    for (let p in props) singltonInstance[p] = o[p], Object(types.a)(props[p], 2) || delete o[p];
                    ctx.status = 201, msgpack(ctx, o);
                } : async (ctx, model) => {
                    const o = await data2Model(ctx, model, repository), {props} = e.rtti;
                    for (let p in props) Object(types.a)(props[p], 2) || delete o[p];
                    ctx.status = 201, msgpack(ctx, o);
                };
                this.get(name, "/" + name, access, async ctx => {
                    msgpack(ctx, await repository.find());
                }).get(`/${name}/:id`, access, async ctx => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) throw 404;
                    msgpack(ctx, model);
                }).put("/" + name, access, body_parse_msgpack, ctx => saveModel(ctx, new e)).patch(`/${name}/:id`, access, body_parse_msgpack, async ctx => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) throw 404;
                    await saveModel(ctx, model);
                }).delete(`/${name}`, access, async ctx => {
                    await repository.clear(), ctx.body = null;
                }).delete(`/${name}/:id`, access, async ctx => {
                    await repository.delete(ctx.params.id), ctx.body = null;
                });
            }
        }
    };
    ApiRouter_ApiRouter = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(1, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ DBConnection_DBConnection, AuthRegister_AuthRegister ]) ], ApiRouter_ApiRouter);
    var external_mem_ = __webpack_require__(69), external_mem_default = __webpack_require__.n(external_mem_), external_striptags_ = __webpack_require__(70), entities_BUser = __webpack_require__(39), sanitize_filename = __webpack_require__(71), sanitize_filename_default = __webpack_require__.n(sanitize_filename), external_tdl_ = __webpack_require__(72), external_tdl_tdlib_ffi_ = __webpack_require__(73);
    class ExtendableError extends Error {
        constructor(message) {
            super(message), this.name = this.constructor.name, Error.captureStackTrace(this, this.constructor);
        }
    }
    async function sendEmail(email, subject, message) {
        try {
            await Object(http.a)("https://script.google.com/macros/s/AKfycbyau7ecNID-Y9M6s067panGIAdkcUS31BCtoa73K4GURJh_76Q/exec", {
                method: "POST",
                form: {
                    email,
                    body: message,
                    subject
                },
                throwHttpErrors: !1,
                resolveBodyOnly: !0
            });
        } catch (err) {
            throw err && console.error.bind(console, "(./src/core/utils/send-email.ts:21)")(err), 
            "Email sending error. Try again later.";
        }
    }
    var tg_utils = __webpack_require__(21);
    class Deffered_DefferTimeout extends ExtendableError {}
    class DeferredMap {
        constructor() {
            this.map = Object.create(null);
        }
        get empty() {
            for (const _ in this.map) return !1;
            return !0;
        }
        keys() {
            return Object.keys(this.map);
        }
        createdAt(id) {
            return this.map[id].createTime;
        }
        make(id, timeout) {
            var _a, _b;
            return (null !== (_a = (_b = this.map)[id]) && void 0 !== _a ? _a : _b[id] = function makeDeffer(timeout) {
                const o = {
                    createTime: Date.now(),
                    promise: null,
                    fulfill: null,
                    reject: null
                };
                return o.promise = new Promise((fullfil, reject) => {
                    o.fulfill = fullfil, o.reject = reject;
                }), timeout && setTimeout(o.reject, timeout, new Deffered_DefferTimeout).unref(), 
                o;
            }(timeout)).promise;
        }
        has(id) {
            return null != this.map[id];
        }
        fulfill(id, v) {
            var _a;
            null === (_a = this.map[id]) || void 0 === _a || _a.fulfill(v), delete this.map[id];
        }
        reject(id, err) {
            var _a;
            null === (_a = this.map[id]) || void 0 === _a || _a.reject(err), delete this.map[id];
        }
        rejectAll(err) {
            for (let id of this.keys()) this.reject(id, err);
        }
    }
    const tdlibOptions = {
        online: !1,
        prefer_ipv6: !1,
        use_storage_optimizer: !0,
        always_parse_markdown: !1,
        ignore_inline_thumbnails: !0
    }, tdlibPostOptions = {
        disable_top_chats: !0,
        disable_time_adjustment_protection: !0,
        disable_persistent_network_statistics: !0,
        ignore_sensitive_content_restrictions: !0,
        disable_sent_scheduled_message_notifications: !0
    };
    function thumb(o, p1, p2) {
        return o[p1] || o[p2];
    }
    class BaseTelegramClient_BaseTelegramClient {
        constructor() {
            this.messagePendings = new DeferredMap, this._id = 0, this.tdl = new external_tdl_tdlib_ffi_.TDLib("./libtdjson"), 
            this.tdParams = {
                _: "tdlibParameters",
                use_secret_chats: !1,
                device_model: "subscribe-alert-bot " + config.f,
                enable_storage_optimizer: tdlibOptions.use_storage_optimizer,
                use_file_database: !1,
                use_chat_info_database: !0,
                use_message_database: !0,
                ignore_file_names: !0,
                use_test_dc: !1
            };
        }
        get id() {
            return this._id;
        }
        async searchChats(query, limit) {
            let chats_ids = new Set((await Promise.all([ this.client.invoke({
                _: "searchChatsOnServer",
                query,
                limit
            }), this.client.invoke({
                _: "searchChats",
                query,
                limit
            }) ])).flatMap(cs => cs.chat_ids));
            return Promise.all(Array.from(chats_ids, chat_id => this.client.invoke({
                _: "getChat",
                chat_id
            })));
        }
        sendErrorEmail() {
            return async function sendErrorEmail(message) {
                try {
                    const ur = external_typescript_ioc_.Container.get(entities_User.UserRepository);
                    await sendEmail((await ur.findOne()).email, "Ошибка в работе бота http://" + config.d, message);
                } catch {}
            }("Сбой в авторизации телеграм клиента. Пересохраните настройки для получения нового авторизационного кода.");
        }
        async update(authInfo, ctx) {
            const {type, apiHash, apiId} = authInfo;
            if (!apiId) throw "Invalid apiId: " + apiId;
            if ("user" != type && "bot" != type) throw "Invalid tdlib client type: " + type;
            if (!tg_utils.a.test(apiHash)) throw "Invalid apiHash: " + apiHash;
            if ("user" === authInfo.type && !authInfo.phone) throw "Invalid phone: " + authInfo.phone;
            if ("bot" === authInfo.type && !tg_utils.b.test(authInfo.token)) throw "Invalid bot token: " + authInfo.token;
            if (!this.client || authInfo.apiHash != this.apiHash || authInfo.apiId != this.apiId || "user" === authInfo.type && authInfo.phone != this.phoneOrToken || "bot" === authInfo.type && authInfo.token != this.phoneOrToken) {
                const phoneOrToken = "bot" === authInfo.type ? authInfo.token : authInfo.phone, profile = sanitize_filename_default()("bot" === type ? (await Object(tg_utils.c)(phoneOrToken)).id.toString() : phoneOrToken);
                try {
                    this.dispose();
                    const client = this.client = new external_tdl_.Client(this.tdl, {
                        apiId,
                        apiHash,
                        databaseDirectory: data_path(profile + "/database"),
                        filesDirectory: data_path(profile + "/files"),
                        verbosityLevel: 1,
                        skipOldUpdates: !1,
                        useTestDc: !1,
                        useMutableRename: !0,
                        tdlibParameters: this.tdParams
                    });
                    client.on("update", async u => {
                        try {
                            "updateMessageSendSucceeded" === u._ ? this.messagePendings.fulfill(u.message.chat_id + "_" + u.old_message_id, u.message) : "updateMessageSendFailed" === u._ ? this.messagePendings.reject(u.message.chat_id + "_" + u.old_message_id, {
                                code: u.error_code,
                                message: u.error_message
                            }) : await this.onUpdate(u);
                        } catch (err) {
                            console.error.bind(console, "(./src/core/BaseTelegramClient.ts:142)")(err);
                        }
                    }), client.on("error", console.error.bind(console, "(./src/core/BaseTelegramClient.ts:145)")), 
                    await client.connect();
                    for (let name in tdlibOptions) await client.invoke({
                        _: "setOption",
                        name,
                        value: {
                            _: "optionValueBoolean",
                            value: tdlibOptions[name]
                        }
                    });
                    this.apiHash = apiHash, this.apiId = apiId, this.phoneOrToken = phoneOrToken, await client.login(() => "bot" === type ? {
                        type: "bot",
                        getToken: async () => phoneOrToken
                    } : {
                        type: "user",
                        getPassword: async passwordHint => {
                            const code = await fromCtx(ctx).remote("ElMessageBox").prompt("Введите телеграм пароль." + (passwordHint ? " Подсказка: " + passwordHint : ""), {
                                showCancelButton: !1,
                                roundButton: !0
                            });
                            return code.value || "";
                        },
                        getPhoneNumber: async _ => phoneOrToken,
                        getAuthCode: async retry => {
                            if (!ctx) throw this.dispose(), retry || await this.sendErrorEmail(), "Telegram client auth required.";
                            const code = await fromCtx(ctx).remote("ElMessageBox").prompt("Введите код авторизации телеграм клиента (придет в sms или в уже авторизованный клиент).", {
                                showCancelButton: !1,
                                inputType: "number",
                                roundButton: !0
                            });
                            return code.value || "";
                        }
                    });
                    const u = await this.getMe();
                    if (this._id = u.id, "userTypeBot" !== u.type._) for (let name in tdlibPostOptions) await this.client.invoke({
                        _: "setOption",
                        name,
                        value: {
                            _: "optionValueBoolean",
                            value: tdlibPostOptions[name]
                        }
                    });
                    return await this.afterUpdate(authInfo), !0;
                } catch (err) {
                    throw this.dispose(), err;
                }
            }
            return !1;
        }
        getMe() {
            return this.client ? this.client.invoke({
                _: "getMe"
            }) : null;
        }
        dispose() {
            this.client && (this.client.destroy(), this.client = null, this.phoneOrToken = null, 
            this.apiHash = null, this.apiId = null, this._id = 0);
        }
        async logout() {
            this.client && await this.client.invoke({
                _: "logOut"
            }), this.dispose();
        }
        editMessage(chat_id, message_id, content) {
            switch (content._) {
              case "messageText":
                return this.client.invoke({
                    _: "editMessageText",
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageText",
                        text: content.text,
                        disable_web_page_preview: !content.web_page
                    }
                });

              case "messageVideo":
                return this.client.invoke({
                    _: "editMessageMedia",
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageVideo",
                        video: {
                            _: "inputFileRemote",
                            id: content.video.video.remote.id
                        },
                        thumbnail: content.video.thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.video.thumbnail, "file", "photo").remote.id
                            },
                            width: content.video.thumbnail.width,
                            height: content.video.thumbnail.height
                        } : void 0,
                        duration: content.video.duration,
                        supports_streaming: content.video.supports_streaming,
                        width: content.video.width,
                        height: content.video.height
                    }
                });

              case "messageAudio":
                return this.client.invoke({
                    _: "editMessageMedia",
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageAudio",
                        audio: {
                            _: "inputFileRemote",
                            id: content.audio.audio.remote.id
                        },
                        album_cover_thumbnail: content.audio.album_cover_thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.audio.album_cover_thumbnail, "file", "photo").remote.id
                            },
                            width: content.audio.album_cover_thumbnail.width,
                            height: content.audio.album_cover_thumbnail.height
                        } : void 0,
                        duration: content.audio.duration,
                        title: content.audio.title,
                        performer: content.audio.performer
                    }
                });

              case "messageAnimation":
                return this.client.invoke({
                    _: "editMessageMedia",
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageAnimation",
                        animation: {
                            _: "inputFileRemote",
                            id: content.animation.animation.remote.id
                        },
                        thumbnail: content.animation.thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.animation.thumbnail, "file", "photo").remote.id
                            },
                            width: content.animation.thumbnail.width,
                            height: content.animation.thumbnail.height
                        } : void 0,
                        duration: content.animation.duration,
                        width: content.animation.width,
                        height: content.animation.height,
                        caption: content.caption
                    }
                });

              case "messagePhoto":
                return this.client.invoke({
                    _: "editMessageMedia",
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessagePhoto",
                        photo: {
                            _: "inputFileRemote",
                            id: content.photo.sizes[0].photo.remote.id
                        },
                        width: content.photo.sizes[0].width,
                        height: content.photo.sizes[0].height,
                        caption: content.caption
                    }
                });

              case "messageDocument":
                return this.client.invoke({
                    _: "editMessageMedia",
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageDocument",
                        document: {
                            _: "inputFileRemote",
                            id: content.document.document.remote.id
                        },
                        thumbnail: content.document.thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.document.thumbnail, "file", "photo").remote.id
                            },
                            width: content.document.thumbnail.width,
                            height: content.document.thumbnail.height
                        } : void 0,
                        caption: content.caption
                    }
                });

              default:
                if (null != content.caption) return this.client.invoke({
                    _: "editMessageCaption",
                    message_id,
                    chat_id,
                    caption: content.caption
                });
                throw new BaseTelegramClient_UnsupportedMessageType(content._);
            }
        }
        async sendMessage(chat_id, content, reply_to) {
            const message = await this.sendMessageInternal(chat_id, content, reply_to);
            if (message.sending_state && "messageSendingStateFailed" === message.sending_state._) throw {
                code: message.sending_state.error_code,
                message: message.sending_state.error_message
            };
            return this.messagePendings.make(message.chat_id + "_" + message.id);
        }
        sendMessageInternal(chat_id, content, reply_to_message_id) {
            switch (content._) {
              case "messageText":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageText",
                        text: content.text,
                        disable_web_page_preview: !content.web_page
                    }
                });

              case "messageVideoNote":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageVideoNote",
                        video_note: {
                            _: "inputFileRemote",
                            id: content.video_note.video.remote.id
                        },
                        thumbnail: content.video_note.thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.video_note.thumbnail, "file", "photo").remote.id
                            },
                            width: content.video_note.thumbnail.width,
                            height: content.video_note.thumbnail.height
                        } : void 0,
                        duration: content.video_note.duration,
                        length: content.video_note.length
                    }
                });

              case "messageVoiceNote":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageVoiceNote",
                        voice_note: {
                            _: "inputFileRemote",
                            id: content.voice_note.voice.remote.id
                        },
                        duration: content.voice_note.duration,
                        waveform: content.voice_note.waveform,
                        caption: content.caption
                    }
                });

              case "messageVideo":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageVideo",
                        video: {
                            _: "inputFileRemote",
                            id: content.video.video.remote.id
                        },
                        thumbnail: content.video.thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.video.thumbnail, "file", "photo").remote.id
                            },
                            width: content.video.thumbnail.width,
                            height: content.video.thumbnail.height
                        } : void 0,
                        duration: content.video.duration,
                        supports_streaming: content.video.supports_streaming,
                        width: content.video.width,
                        height: content.video.height,
                        caption: content.caption
                    }
                });

              case "messageAudio":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageAudio",
                        audio: {
                            _: "inputFileRemote",
                            id: content.audio.audio.remote.id
                        },
                        album_cover_thumbnail: content.audio.album_cover_thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.audio.album_cover_thumbnail, "file", "photo").remote.id
                            },
                            width: content.audio.album_cover_thumbnail.width,
                            height: content.audio.album_cover_thumbnail.height
                        } : void 0,
                        duration: content.audio.duration,
                        title: content.audio.title,
                        performer: content.audio.performer,
                        caption: content.caption
                    }
                });

              case "messageAnimation":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageAnimation",
                        animation: {
                            _: "inputFileRemote",
                            id: content.animation.animation.remote.id
                        },
                        thumbnail: content.animation.thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.animation.thumbnail, "file", "photo").remote.id
                            },
                            width: content.animation.thumbnail.width,
                            height: content.animation.thumbnail.height
                        } : void 0,
                        duration: content.animation.duration,
                        width: content.animation.width,
                        height: content.animation.height,
                        caption: content.caption
                    }
                });

              case "messageLocation":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageLocation",
                        location: content.location,
                        live_period: content.live_period,
                        heading: content.heading,
                        proximity_alert_radius: content.proximity_alert_radius
                    }
                });

              case "messageVenue":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageVenue",
                        venue: content.venue
                    }
                });

              case "messageContact":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageContact",
                        contact: content.contact
                    }
                });

              case "messagePhoto":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: content.photo.sizes[0].photo.remote ? {
                        _: "inputMessagePhoto",
                        photo: {
                            _: "inputFileRemote",
                            id: content.photo.sizes[0].photo.remote.id
                        },
                        width: content.photo.sizes[0].width,
                        height: content.photo.sizes[0].height,
                        caption: content.caption
                    } : {
                        _: "inputMessagePhoto",
                        photo: {
                            _: "inputFileLocal",
                            path: content.photo.sizes[0].photo.local.path
                        },
                        caption: content.caption
                    }
                });

              case "messageDocument":
                return this.client.invoke({
                    _: "sendMessage",
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageDocument",
                        document: {
                            _: "inputFileRemote",
                            id: content.document.document.remote.id
                        },
                        thumbnail: content.document.thumbnail ? {
                            _: "inputThumbnail",
                            thumbnail: {
                                _: "inputFileRemote",
                                id: thumb(content.document.thumbnail, "file", "photo").remote.id
                            },
                            width: content.document.thumbnail.width,
                            height: content.document.thumbnail.height
                        } : void 0,
                        caption: content.caption
                    }
                });

              default:
                throw new BaseTelegramClient_UnsupportedMessageType(content._);
            }
        }
        static isContentSupported(s) {
            return supportedContents.has(s);
        }
    }
    class BaseTelegramClient_UnsupportedMessageType extends ExtendableError {}
    const supportedContents = new Set(Array.from(BaseTelegramClient_BaseTelegramClient.prototype.sendMessageInternal.toString().matchAll(/case\s+['"](\w+)/g), r => r[1]));
    function now() {
        return Math.ceil(Date.now() / 1e3);
    }
    const NULL_ITER = {
        value: null
    };
    let TgClient_TgClient = class TgClient extends BaseTelegramClient_BaseTelegramClient {
        constructor(settings, repository) {
            super(), this.settings = settings, this.repository = repository, this.objects = [], 
            this.resolvers = new Set, this.members = 1, this.sanitize = external_mem_default()((function(s) {
                return this.client.execute({
                    _: "parseTextEntities",
                    text: Object(external_striptags_.striptags)(s, {
                        tagReplacementText: "\n",
                        allowedTags: new Set([ "strong", "b", "i", "em", "s", "strike", "del", "code", "pre", "u", "a" ])
                    }).replace(/&nbsp;/g, " ").trim().replace(/[\r\n]+/g, "\n"),
                    parse_mode: {
                        _: "textParseModeHTML"
                    }
                });
            })), this.afterUpdate = async authInfo => {
                var _a;
                await this.client.invoke({
                    _: "getChat",
                    chat_id: +("-100" + authInfo.channel)
                }), this.members = await this.repository.count() || 1, null !== (_a = this.t) && void 0 !== _a || (this.t = this.thread().catch(console.error.bind(console, "(./src/TgClient.ts:79)")));
            }, this.onUpdate = async update => {
                if ("updateNewMessage" === update._ && !update.message.is_outgoing && !update.message.is_channel_post && "messageText" === update.message.content._ && /^\/start(\s|$)/.test(update.message.content.text.text)) {
                    const user = {
                        tId: update.message.chat_id
                    };
                    this.send(user, this.settings.helloMessage).catch(console.error.bind(console, "(./src/TgClient.ts:91)")), 
                    this.has(user.tId) || await this.repository.findOne(user) || setTimeout(() => {
                        this.has(user.tId) || this.add(user.tId);
                    }, 1e3 * this.settings.firstPause).unref();
                }
            };
        }
        [Symbol.asyncIterator]() {
            return this;
        }
        get size() {
            return this.objects.length;
        }
        add(value) {
            this.resolvers.size > 0 ? function setShift(s) {
                for (let x of s) return s.delete(x), x;
            }(this.resolvers)({
                value
            }) : this.objects.push(value);
        }
        has(id) {
            return this.objects.some(u => u === id);
        }
        next() {
            return this.objects.length > 0 ? Promise.resolve({
                value: this.objects.shift()
            }) : new Promise(resolve => {
                this.resolvers.add(resolve), setTimeout(() => {
                    this.resolvers.delete(resolve), resolve(NULL_ITER);
                }, Math.max(100, 1e3 * this.settings.alertInterval / this.members | 0)).unref();
            });
        }
        send(to, text) {
            return this.sendMessage(to.tId, {
                _: "messageText",
                web_page: void 0,
                text: this.sanitize(text)
            });
        }
        async thread() {
            let lastUId = -1;
            const db = this.repository.createQueryBuilder().select();
            for await (let tId of this) {
                let user;
                try {
                    if (user = tId ? await this.repository.findOne({
                        tId
                    }) || this.repository.create({
                        tId
                    }) : await db.where("id > :id", {
                        id: lastUId
                    }).andWhere("lastAlert <= :lastAlert", {
                        lastAlert: now() - this.settings.alertInterval
                    }).getOne(), !user) {
                        lastUId = -1, this.add(null);
                        continue;
                    }
                    tId || (lastUId = user.id), await this.client.invoke({
                        _: "createPrivateChat",
                        user_id: user.tId,
                        force: !0
                    });
                    const status = await this.client.invoke({
                        _: "getChatMember",
                        user_id: user.tId,
                        chat_id: +("-100" + this.settings.channel)
                    });
                    "chatMemberStatusLeft" === status.status._ ? (user.subscribed = 0, user.lastAlert = now(), 
                    await this.repository.save(user), await this.send(user, this.settings.badMessage)) : user.subscribed || (user.subscribed = now(), 
                    this.settings.goodMessage && (user.lastAlert = user.subscribed), await this.repository.save(user), 
                    this.settings.goodMessage && await this.send(user, this.settings.goodMessage));
                } catch (err) {
                    !err || "User not found" !== err.message && "Bot was blocked by the user" !== err.message && "Chat not found" !== err.message && "Chat info not found" !== err.message && "Can't access the chat" !== err.message || await this.repository.remove(user).catch(console.error.bind(console, "(./src/TgClient.ts:159)")), 
                    console.error.bind(console, "(./src/TgClient.ts:161)")(user, err);
                } finally {
                    user && await this.repository.count().then(v => this.members = v || 1, console.error.bind(console, "(./src/TgClient.ts:164)"));
                }
            }
        }
    };
    TgClient_TgClient = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(1, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ ClientSettings.ClientSettings, entities_BUser.BUserRepository ]) ], TgClient_TgClient);
    let App_App = class App {
        constructor(tgClient, settings, settingsRepository, apiRouter) {
            this.tgClient = tgClient, this.settings = settings, apiRouter.patch("tgclient_save", "/tgclient-save/:id", check_entity_access(ClientSettings.ClientSettings), body_parse_msgpack, smart_redirect, async ctx => {
                const body = ctx.request.body;
                ClientSettings.ClientSettings.insertValidate(body), await this.tgClient.update({
                    type: "bot",
                    ...body
                }, ctx);
                const {props} = ClientSettings.ClientSettings.rtti;
                for (let k in body) {
                    const p = props[k];
                    p && !Object(types.a)(p, 98) && (settings[k] = body[k]);
                }
                await settingsRepository.save(settings), ctx.status = 201, msgpack(ctx, settings);
            });
        }
        async init() {
            const {settings} = this;
            if (settings.apiHash && settings.apiId && settings.token) try {
                await this.tgClient.update({
                    type: "bot",
                    ...settings
                }), console.log.bind(console, "(./src/App.ts:49)")("Tg connected");
            } catch (err) {
                console.error.bind(console, "(./src/App.ts:51)")(err), this.tgClient.sendErrorEmail();
            }
        }
    };
    App_App = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(1, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(2, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(3, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ TgClient_TgClient, ClientSettings.ClientSettings, ClientSettings.ClientSettingsRepository, ApiRouter_ApiRouter ]) ], App_App);
    var external_fs_cp_ = __webpack_require__(74), external_fs_cp_default = __webpack_require__.n(external_fs_cp_), external_koa_compress_ = __webpack_require__(75), external_koa_compress_default = __webpack_require__.n(external_koa_compress_), external_path_to_regexp_ = __webpack_require__(76), check_auth = function(ctx, next) {
        if (ctx.isAuthenticated()) return next();
        throw is_ajax(ctx) ? 401 : "Login required";
    };
    var flash = async (ctx, next) => {
        const t = function getCookie(name, ctx) {
            try {
                const v = ctx.cookies.get(name);
                return v ? JSON.parse(decodeURIComponent(v)) : null;
            } catch (err) {
                console.error.bind(console, "(./src/core/utils/server-cookie.ts:36)")(name, err), 
                deleteCookie(name, ctx);
            }
            return null;
        }("_f", ctx);
        let a = [ [], [], [] ];
        try {
            t && (ctx.flash = {
                messages: t[2],
                warnings: t[1],
                errors: t[0]
            }), ctx.addFlash = (message, level) => {
                a[0 | level].push(message);
            }, await next();
        } finally {
            a.some(v => v.length > 0) ? function setCookie(name, ctx, data) {
                ctx.cookies.set(name, encodeURIComponent(JSON.stringify(data)), DEFAULT_COOKIE_OPTIONS);
            }("_f", ctx, a) : t && deleteCookie("_f", ctx);
        }
    };
    let root = "", rootSlash = "";
    var normalize_traling_slashes = function(ctx, next) {
        if ("GET" === ctx.method) {
            const {path} = ctx;
            if (root || (root = new URL(ctx.resolve("root")).pathname.replace(/\/$/, ""), rootSlash = root + "/"), 
            "/" !== path) {
                if (root === path) return ctx.status = 301, ctx.redirect(rootSlash + (ctx.querystring ? "?" + ctx.querystring : ""));
                if (path.endsWith("/") && path !== rootSlash) return ctx.status = 301, ctx.redirect(path.slice(0, -1) + (ctx.querystring ? "?" + ctx.querystring : ""));
            }
        }
        return next();
    };
    const assetsUrls = new Array(2);
    var pug = function(ctx, next) {
        return ctx.pug = (file, locals, noCommon) => {
            ctx.type = "html";
            const template = "function" == typeof file ? file : __webpack_require__(86)("./" + file + ".pug"), r = Object.assign({}, ...[ !noCommon && "common" ].concat(null == locals ? void 0 : locals.entries).map(e => e && require(__dirname + "/" + e + "-assets.json"))), styles = Object.values(r).map(r => r.css).filter(Boolean), scripts = Object.values(r).map(r => r.js).filter(Boolean), body = template({
                title: "Subscribe alert bot",
                assetsUrl: assetsUrls[+ctx.secure] || (assetsUrls[+ctx.secure] = resolve_url(ctx, config.h, !ctx.secure && config.d != config.g)),
                favicon: "favicon.ico",
                ...ctx.flash,
                ...locals,
                styles,
                scripts
            });
            ctx.set("Cache-Control", "no-cache, max-age=31536000");
            const et = external_etag_default()(body);
            ctx.set("ETag", et), ctx.headers["if-none-match"] && ctx.headers["if-none-match"].endsWith(et) ? ctx.status = 304 : ctx.body = body;
        }, next();
    }, redirect_after_error = redirectRouteName => async function(ctx, next) {
        if (is_ajax(ctx)) await next(); else try {
            await next();
        } catch (err) {
            err instanceof Error ? (console.error.bind(console, "(./src/core/middlewares/redirect-after-error.ts:16)")(err), 
            ctx.status = 500, destroySession(ctx), ctx.namedRedirect(redirectRouteName, 1)) : (err && ctx.addFlash && ("function" == typeof err.forEach ? err.forEach(e => ctx.addFlash(Object(string_tools_.stringify)(e))) : ctx.addFlash(Object(string_tools_.stringify)(err))), 
            ctx.namedRedirect(redirectRouteName));
        }
    }, index_mustache = __webpack_require__(77), index_mustache_default = __webpack_require__.n(index_mustache);
    function route(method, params, ...middlewares) {
        return (target, prop) => {
            (target.constructor.__routes_info__ || (target.constructor.__routes_info__ = [])).push({
                method,
                name: params && params.name,
                path: params && params.path || "/" + Object(external_param_case_.paramCase)(prop),
                prop,
                middlewares
            });
        };
    }
    function applyRoutes(router, routes) {
        for (let {method, name, path, prop, middlewares} of routes.constructor.__routes_info__) "function" == typeof path && (path = path.call(routes)), 
        name ? router[method](name, path, ...middlewares, routes[prop].bind(routes)) : router[method](path, ...middlewares, routes[prop].bind(routes));
        return router;
    }
    const get = route.bind(null, "get"), post = (route.bind(null, "head"), route.bind(null, "post"));
    route.bind(null, "put"), route.bind(null, "delete"), route.bind(null, "patch"), 
    route.bind(null, "all");
    var external_vue_template_compiler_ = __webpack_require__(78), external_vue_template_es2015_compiler_ = __webpack_require__(79), external_vue_template_es2015_compiler_default = __webpack_require__.n(external_vue_template_es2015_compiler_);
    const templates = {
        single: __webpack_require__(43),
        table: __webpack_require__(44)
    };
    function isExtendableArrayField(v) {
        return Object(types.a)(v, 8);
    }
    function isVisibleField(v) {
        return !Object(types.a)(v, 98);
    }
    function isEditableField(v) {
        return !Object(types.a)(v, 34);
    }
    function isNormalField(v) {
        return !Object(types.a)(v, 98) && !Object(types.a)(v, 4);
    }
    function isFullwidthField(v) {
        return !Object(types.a)(v, 98) && Object(types.a)(v, 4);
    }
    function findPrimaryIndex(props) {
        let i = 0;
        for (let k in props) {
            if (Object(types.a)(props[k], 18)) return i;
            ++i;
        }
        throw new Error("Can't find primary field.");
    }
    function toFunction(code) {
        return `(function(){${code}})`;
    }
    var rtti_to_vue_component = ({props, displayInfo}, can, ctx) => {
        const vueTemplate = templates[displayInfo.display]({
            props,
            EDIT: 1,
            NONE: 0,
            REMOVE: 3,
            SAVE: 2,
            can,
            ctx,
            isExtendableArrayField,
            findPrimaryIndex,
            isVisibleField,
            isEditableField,
            isNormalField,
            isFullwidthField
        }), {render, staticRenderFns} = Object(external_vue_template_compiler_.compile)(vueTemplate, {
            preserveWhitespace: !1
        });
        return external_vue_template_es2015_compiler_default()(`(function(){\nreturn {staticRenderFns:[${staticRenderFns.map(toFunction)}],render:${toFunction(render)}};\n})()`);
    }, external_set_utils_ = __webpack_require__(41), external_set_utils_default = __webpack_require__.n(external_set_utils_), external_builtin_status_codes_ = __webpack_require__(30), external_builtin_status_codes_default = __webpack_require__.n(external_builtin_status_codes_);
    function convertError(err) {
        return Object(string_tools_.isJsonable)(err) ? err : Object(string_tools_.stringify)(err);
    }
    var catch_error = async (ctx, next) => {
        try {
            await next();
        } catch (err) {
            const ajax = is_ajax(ctx);
            if (ctx.type = ajax ? "json" : "text", Array.isArray(err)) ctx.status = 400, ctx.body = ajax ? JSON.stringify({
                errors: err.map(convertError)
            }) : err.map(string_tools_.stringify).join("\n"); else {
                if (external_builtin_status_codes_default.a[err]) ctx.status = +err, err = external_builtin_status_codes_default.a[err]; else {
                    const s = 0 | +err.status || 500;
                    ctx.status = s, s >= 500 ? (console.error.bind(console, "(./src/core/middlewares/catch-error.ts:30)")(err), 
                    err = external_builtin_status_codes_default.a[s] || external_builtin_status_codes_default.a[500]) : delete err.status;
                }
                ctx.body = ajax ? JSON.stringify({
                    errors: [ convertError(err) ]
                }) : Object(string_tools_.stringify)(err);
            }
        }
    };
    let MainRouter_MainRouter = class MainRouter extends external_koa_router_default.a {
        constructor() {
            super(), this.use(catch_error).get("health", config.c, no_store, ctx => {
                ctx.type = "json", ctx.body = '{"success":"ok"}';
            }).get("index", "/", no_store, ctx => {
                ctx.status = 200, ctx.body = "";
            });
        }
    };
    MainRouter_MainRouter = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__metadata)("design:paramtypes", []) ], MainRouter_MainRouter);
    var external_koa_body_ = __webpack_require__(9), external_koa_body_default = __webpack_require__.n(external_koa_body_);
    let checkpass_CheckPassRoutes = class CheckPassRoutes {
        checkpass(ctx) {
            const {action} = ctx.params;
            ctx.pug("checkpass", {
                action: ctx.resolve("checkpass", {
                    action
                })
            });
        }
        async checkpassPost(ctx) {
            ctx.session.confirmed = 0;
            let {password} = ctx.request.body;
            if (!await Object(external_bcryptjs_.compare)(Object(string_tools_.trim)(password), ctx.state.user.password)) throw "Wrong password";
            ctx.session.confirmed = Date.now(), ctx.namedRedirect(ctx.params.action);
        }
    };
    Object(external_tslib_.__decorate)([ get({
        name: "checkpass",
        path: "/checkpass/:action"
    }), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", void 0) ], checkpass_CheckPassRoutes.prototype, "checkpass", null), 
    Object(external_tslib_.__decorate)([ post({
        path: "/checkpass/:action"
    }, redirect_after_error("checkpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", Promise) ], checkpass_CheckPassRoutes.prototype, "checkpassPost", null), 
    checkpass_CheckPassRoutes = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton ], checkpass_CheckPassRoutes);
    let install_InstallRoutes = class InstallRoutes {
        constructor(settings, userRepository, settingsRepository) {
            this.settings = settings, this.userRepository = userRepository, this.settingsRepository = settingsRepository;
        }
        install(ctx) {
            this.settings.installed ? ctx.isAuthenticated() ? ctx.namedRedirect("root") : (ctx.addFlash("Already installed. Login please.", 1), 
            ctx.namedRedirect("login")) : ctx.pug("install");
        }
        async installPost(ctx) {
            if (this.settings.installed) ctx.namedRedirect("install"); else {
                let {email, password} = ctx.request.body;
                email = Object(string_tools_.trim)(email), password = Object(string_tools_.trim)(password), 
                entities_User.User.insertValidate({
                    email,
                    password
                });
                let user = await this.userRepository.findOne({
                    email
                });
                if (user) throw "Пользователь с таким email уже существует.";
                user = new entities_User.User, user.email = email, user.password = await Object(external_bcryptjs_.hash)(password, 10), 
                await this.userRepository.insert(user), this.settings.installed = !0, await this.settingsRepository.save(this.settings), 
                await ctx.login(user), ctx.namedRedirect("root");
            }
        }
    };
    Object(external_tslib_.__decorate)([ get({
        name: "install"
    }), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", void 0) ], install_InstallRoutes.prototype, "install", null), 
    Object(external_tslib_.__decorate)([ post({
        path: "/install"
    }, redirect_after_error("install"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", Promise) ], install_InstallRoutes.prototype, "installPost", null), 
    install_InstallRoutes = Object(external_tslib_.__decorate)([ Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(1, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(2, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ entities_Settings.Settings, entities_User.UserRepository, entities_Settings.SettingsRepository ]) ], install_InstallRoutes);
    class login_LoginRoutes {
        login(ctx) {
            ctx.pug("login");
        }
        loginPost(ctx, next) {
            return external_koa_passport_default.a.authenticate("local", async (_, user, info) => {
                user ? (await ctx.login(user), ctx.namedRedirect("root")) : (ctx.addFlash(info && info.message || "Invalid login data"), 
                ctx.namedRedirect("login"));
            })(ctx, next);
        }
    }
    Object(external_tslib_.__decorate)([ get({
        name: "login"
    }), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", void 0) ], login_LoginRoutes.prototype, "login", null), 
    Object(external_tslib_.__decorate)([ post({
        path: "/login"
    }, external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object, Function ]), Object(external_tslib_.__metadata)("design:returntype", void 0) ], login_LoginRoutes.prototype, "loginPost", null);
    let resetemail_ResetEmailRoutes = class ResetEmailRoutes {
        constructor(userRepository) {
            this.userRepository = userRepository;
        }
        resetemail(ctx) {
            Date.now() - (ctx.session.confirmed || 0) < 12e4 ? ctx.pug("resetemail") : ctx.namedRedirect("checkpass", 0, {
                action: "resetemail"
            });
        }
        async resetemailPost(ctx) {
            let {email} = ctx.request.body;
            email = Object(string_tools_.trim)(email);
            const user = {
                ...ctx.state.user,
                email
            };
            entities_User.User.updateValidate(user), await this.userRepository.save(user), await ctx.login(user), 
            ctx.namedRedirect("root");
        }
    };
    Object(external_tslib_.__decorate)([ get({
        name: "resetemail"
    }), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", void 0) ], resetemail_ResetEmailRoutes.prototype, "resetemail", null), 
    Object(external_tslib_.__decorate)([ post({
        path: "/resetemail"
    }, redirect_after_error("resetemail"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", Promise) ], resetemail_ResetEmailRoutes.prototype, "resetemailPost", null), 
    resetemail_ResetEmailRoutes = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ entities_User.UserRepository ]) ], resetemail_ResetEmailRoutes);
    const services = Object.freeze({
        "mail.ru": Object.freeze({
            title: "почту Mail.Ru",
            url: "https://e.mail.ru/"
        }),
        "bk.ru": Object.freeze({
            title: "почту Mail.Ru (bk.ru)",
            url: "https://e.mail.ru/"
        }),
        "list.ru": Object.freeze({
            title: "почту Mail.Ru (list.ru)",
            url: "https://e.mail.ru/"
        }),
        "inbox.ru": Object.freeze({
            title: "почту Mail.Ru (inbox.ru)",
            url: "https://e.mail.ru/"
        }),
        "yandex.ru": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ru/"
        }),
        "ya.ru": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ru/"
        }),
        "yandex.ua": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.ua/"
        }),
        "yandex.by": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.by/"
        }),
        "yandex.kz": Object.freeze({
            title: "Яндекс.Почта",
            url: "https://mail.yandex.kz/"
        }),
        "yandex.com": Object.freeze({
            title: "Yandex.Mail",
            url: "https://mail.yandex.com/"
        }),
        "gmail.com": Object.freeze({
            title: "Gmail",
            url: "https://mail.google.com/"
        }),
        "googlemail.com": Object.freeze({
            title: "Gmail",
            url: "https://mail.google.com/"
        }),
        "outlook.com": Object.freeze({
            title: "Outlook.com",
            url: "https://mail.live.com/"
        }),
        "hotmail.com": Object.freeze({
            title: "Outlook.com (Hotmail)",
            url: "https://mail.live.com/"
        }),
        "live.ru": Object.freeze({
            title: "Outlook.com (live.ru)",
            url: "https://mail.live.com/"
        }),
        "live.com": Object.freeze({
            title: "Outlook.com (live.com)",
            url: "https://mail.live.com/"
        }),
        "me.com": Object.freeze({
            title: "iCloud Mail",
            url: "https://www.icloud.com/"
        }),
        "icloud.com": Object.freeze({
            title: "iCloud Mail",
            url: "https://www.icloud.com/"
        }),
        "rambler.ru": Object.freeze({
            title: "Рамблер/почта",
            url: "https://mail.rambler.ru/"
        }),
        "yahoo.com": Object.freeze({
            title: "Yahoo! Mail",
            url: "https://mail.yahoo.com/"
        }),
        "ukr.net": Object.freeze({
            title: "почту ukr.net",
            url: "https://mail.ukr.net/"
        }),
        "i.ua": Object.freeze({
            title: "почту I.UA",
            url: "http://mail.i.ua/"
        }),
        "bigmir.net": Object.freeze({
            title: "почту Bigmir.net",
            url: "http://mail.bigmir.net/"
        }),
        "tut.by": Object.freeze({
            title: "почту tut.by",
            url: "https://mail.tut.by/"
        }),
        "inbox.lv": Object.freeze({
            title: "Inbox.lv",
            url: "https://www.inbox.lv/"
        }),
        "mail.kz": Object.freeze({
            title: "почту mail.kz",
            url: "http://mail.kz/"
        })
    });
    var detect_email_service = email => {
        const m = email.split("@"), domain = m && m.length > 1 ? m[m.length - 1] : null;
        return services[domain] || "";
    }, rnd = __webpack_require__(26);
    let resetpass_ResetPassRoutes = class ResetPassRoutes {
        constructor(userRepository) {
            this.userRepository = userRepository, this.resetPasswordTokens = Object.create(null), 
            Object(external_timers_.setInterval)(() => {
                const NOW = Date.now();
                for (let token in this.resetPasswordTokens) NOW - this.resetPasswordTokens[token].time > 12e5 && delete this.resetPasswordTokens[token];
            }, 12e5).unref();
        }
        validateToken(token) {
            let tokenInfo = this.resetPasswordTokens[token];
            if (!tokenInfo) throw "Invalid or expiried recovery url";
            const {time} = tokenInfo;
            if (Date.now() - time > 12e5) throw delete this.resetPasswordTokens[token], "Expiried recovery url";
            return tokenInfo;
        }
        resetpass(ctx) {
            ctx.pug("resetpass", {
                email: ctx.isAuthenticated() ? ctx.state.user.email : ""
            });
        }
        async resetpassPost(ctx) {
            let {email} = ctx.request.body;
            email = Object(string_tools_.trim)(email);
            const user = await this.userRepository.findOne({
                email
            });
            if (!user) throw "User not found";
            const {id} = user;
            for (let t in this.resetPasswordTokens) this.resetPasswordTokens[t].id === id && delete this.resetPasswordTokens[t];
            const token = await Object(rnd.a)();
            this.resetPasswordTokens[token] = {
                id,
                time: Date.now()
            }, await sendEmail(email, "Изменение пароля " + ctx.resolve("root"), "Для продолжения смены пароля перейдите на " + ctx.resolve("newpassword", {
                token
            })), ctx.pug("resetpass-sended", {
                email,
                service: detect_email_service(email)
            });
        }
        newpassword(ctx) {
            const {token} = ctx.params;
            this.validateToken(token), ctx.pug("newpassword", {
                action: ctx.resolve("newpassword", {
                    token
                })
            });
        }
        async newpasswordPost(ctx) {
            const {token} = ctx.params, {id} = this.validateToken(token);
            delete this.resetPasswordTokens[token];
            const user = id && await this.userRepository.findOne(id);
            if (!user) throw "User not found. Попробуйте другой аккаунт.";
            let {password} = ctx.request.body;
            password = Object(string_tools_.trim)(password), entities_User.User.insertValidate({
                email: user.email,
                password
            }), user.password = await Object(external_bcryptjs_.hash)(password, 10), await this.userRepository.save(user), 
            await ctx.login(user), ctx.namedRedirect("root");
        }
    };
    function restart(ctx) {
        ctx.type = "json", ctx.body = '{"success":"ok"}', ctx.res.once("finish", () => process.exit(2));
    }
    Object(external_tslib_.__decorate)([ get({
        name: "resetpass"
    }), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", void 0) ], resetpass_ResetPassRoutes.prototype, "resetpass", null), 
    Object(external_tslib_.__decorate)([ post({
        path: "/resetpass"
    }, redirect_after_error("resetpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", Promise) ], resetpass_ResetPassRoutes.prototype, "resetpassPost", null), 
    Object(external_tslib_.__decorate)([ get({
        name: "newpassword",
        path: "/newpassword/:token"
    }, redirect_after_error("resetpass")), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", void 0) ], resetpass_ResetPassRoutes.prototype, "newpassword", null), 
    Object(external_tslib_.__decorate)([ post({
        path: "/newpassword/:token"
    }, redirect_after_error("resetpass"), external_koa_body_default()({
        text: !1,
        json: !1
    })), Object(external_tslib_.__metadata)("design:type", Function), Object(external_tslib_.__metadata)("design:paramtypes", [ Object ]), Object(external_tslib_.__metadata)("design:returntype", Promise) ], resetpass_ResetPassRoutes.prototype, "newpasswordPost", null), 
    resetpass_ResetPassRoutes = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ entities_User.UserRepository ]) ], resetpass_ResetPassRoutes);
    const EXT_COMPS = new Set([ "javascript" ]), WORKER_INFO = require(__dirname + "/workers-assets.json"), COMP_ENT_MAP = {
        javascript: "monaco"
    };
    let AdminRouter_AdminRouter = class AdminRouter extends external_koa_router_default.a {
        constructor(settings, dbConnection, installRoutes, loginRoutes, resetPassRoutes, checkPassRoutes, resetEmailRoutes, {session, passport, passportSession}) {
            super(), this.use(flash, smart_redirect, normalize_traling_slashes, redirect_after_error("login"), session, passport, passportSession, pug), 
            this.put("logout", "/logout", ctx => {
                ctx.status = 204, destroySession(ctx);
            }), applyRoutes(this, installRoutes), settings.installed || this.use((settings => function(ctx, next) {
                if (settings.installed) return next();
                ctx.namedRedirect("install");
            })(settings)), applyRoutes(this, loginRoutes), applyRoutes(this, resetPassRoutes), 
            this.use(check_auth), applyRoutes(this, checkPassRoutes), applyRoutes(this, resetEmailRoutes);
            const componentsCache = {};
            this.get("root", "/", ctx => {
                const user = ctx.state.user;
                if (!componentsCache[user.role]) {
                    const components = [], validators = new Set;
                    for (let entity of entities_entities) {
                        let {name, rtti, checkAccess, hooks} = entity;
                        name = external_pluralize_default()(name);
                        const can = Object.create(null);
                        for (let action of [ "GET", "PATCH", "PUT", "DELETE" ]) try {
                            can[action] = checkAccess(action, ctx.state.user);
                        } catch {}
                        if (can.GET && rtti.displayInfo.display) {
                            can.CHANGE = can.PUT || can.PATCH;
                            const render = rtti_to_vue_component(rtti, can, ctx), remote = Object.values(rtti.props).filter(v => v.remote).map(v => {
                                const i = v.remote.lastIndexOf("."), c = v.remote.slice(0, i), method = v.remote.slice(i + 1);
                                if (!Object(string_tools_.isValidVar)(c) || !Object(string_tools_.isValidVar)(method) || method == v.remote) throw `Invalid property ${Object(string_tools_.tosource)(v)}`;
                                return {
                                    c,
                                    method
                                };
                            });
                            for (const p in rtti.props) validators.add(rtti.props[p].type);
                            components.push({
                                name,
                                sortable: !(!can.PATCH || !rtti.displayInfo.sortable || "single" === rtti.displayInfo.display),
                                order: 0 | rtti.displayInfo.order,
                                icon: rtti.displayInfo.icon,
                                render,
                                can: Object(string_tools_.tosource)(can, null, ""),
                                rtti: Object(string_tools_.tosource)(rtti, null, ""),
                                hooks: Object(string_tools_.tosource)(hooks, null, ""),
                                endPoint: Object(external_param_case_.paramCase)(name),
                                remote
                            });
                        }
                    }
                    const classes = [], m = external_typescript_ioc_.Scope.Singleton.constructor.instances;
                    m.forEach((v, k) => classes.push({
                        name: k.name,
                        props: Object.keys(v).filter(p => !/^[_#]/.test(p))
                    }));
                    const {stack} = external_typescript_ioc_.Container.get(MainRouter_MainRouter);
                    componentsCache[user.role] = {
                        components,
                        Paths: stack.filter(l => l.name).map(l => {
                            if (l.paramNames.length > 0) {
                                const tokens = Object(external_path_to_regexp_.parse)(l.path);
                                return {
                                    name: l.name,
                                    url: `function (${l.paramNames.map(p => p.name).join(", ")}) { return ${tokens.map(p => p.constructor == String ? JSON.stringify(p) : p.name).join(' + "/" + ')}}`
                                };
                            }
                            return {
                                name: l.name,
                                url: JSON.stringify(l.path)
                            };
                        }),
                        entries: Array.from(external_set_utils_default.a.map(external_set_utils_default.a.intersect(validators, EXT_COMPS), c => COMP_ENT_MAP[c])).concat("index"),
                        HasPty: AVALIBLE,
                        EDITOR_WORKER: WORKER_INFO["editor.worker"].js,
                        TYPESCRIPT_WORKER: WORKER_INFO["typescript.worker"].js
                    };
                }
                ctx.pug("index", {
                    entries: componentsCache[user.role].entries,
                    inlineScript: index_mustache_default()({
                        ...componentsCache[user.role],
                        email: user.email,
                        Flash: JSON.stringify(ctx.flash)
                    })
                }, !0);
            }), this.get("backup", "/backup", no_store, external_koa_compress_default()(), ctx => {
                ctx.attachment(createDBFilename()), ctx.body = Object(external_fs_.createReadStream)(dbConnection.filename);
            }), this.put("restore", "/restore", async ctx => {
                await external_fs_cp_default()(ctx.req, data_path(createDBFilename())), await restart(ctx);
            }), this.put("restart", "/restart", restart);
        }
    };
    AdminRouter_AdminRouter = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(1, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(2, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(3, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(4, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(5, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(6, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(7, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ entities_Settings.Settings, DBConnection_DBConnection, install_InstallRoutes, login_LoginRoutes, resetpass_ResetPassRoutes, checkpass_CheckPassRoutes, resetemail_ResetEmailRoutes, AuthRegister_AuthRegister ]) ], AdminRouter_AdminRouter);
    let core_Main = class Main {
        constructor(baseApp, authRegister, app, router, apiRouter, adminRouter) {
            this.baseApp = baseApp, this.authRegister = authRegister, this.app = app, this.router = router, 
            this.apiRouter = apiRouter, this.adminRouter = adminRouter;
        }
        async main() {
            const {router, apiRouter, adminRouter, app, baseApp} = this;
            await baseApp.init(), router.use(config.b, apiRouter.routes(), apiRouter.allowedMethods()), 
            router.use(config.a, adminRouter.routes(), adminRouter.allowedMethods()), app.use(router.routes()).use(router.allowedMethods()).use(external_koa_mount_default()(config.h, ctx => external_koa_send_default()(ctx, ctx.path, {
                root: Object(external_path_.normalize)(__dirname + config.h),
                maxage: 31536e6,
                gzip: !0,
                brotli: !1,
                setHeaders(res) {
                    res.setHeader("Access-Control-Allow-Origin", "http://" + config.d), res.setHeader("Cache-Control", "no-store, no-cache, max-age=0");
                }
            })));
            const server = Object(external_http_.createServer)(app.callback()), {passportComposed} = this.authRegister, wss = new external_ws_.Server({
                server,
                verifyClient: (info, callback) => {
                    try {
                        const ctx = app.createContext(info.req, null);
                        passportComposed(ctx, () => (callback(!!ctx.state.user), null)).catch(console.error);
                    } catch (err) {
                        console.error.bind(console, "(./src/core/index.ts:79)")(err);
                    }
                }
            });
            wss.on("connection", async (socket, request) => {
                try {
                    const ctx = app.createContext(request, null);
                    await passportComposed(ctx, () => {
                        let terminalSize = ctx.query.cols && ctx.query.rows ? {
                            cols: +ctx.query.cols || 100,
                            rows: +ctx.query.rows || 40
                        } : null;
                        return new ServerRPC_ServerRPC(socket, parseInt(ctx.query.tab), sessionSig(ctx), ctx.state.user, terminalSize), 
                        null;
                    });
                } catch (err) {
                    console.error.bind(console, "(./src/core/index.ts:102)")(err), socket.close();
                }
            }), Object(external_timers_.setInterval)(pingServers, 1e4, 8e3).unref();
            const [currentIp, hostIps] = await Promise.all([ await get_my_ip(), await Object(external_util_.promisify)(external_dns_.resolve4)(config.d).catch(console.error.bind(console, "(./src/core/index.ts:111)")), new Promise(resolve => server.listen(config.f, resolve)) ]);
            console.log(`Current ip: ${currentIp} \nHost: ${config.g} [${hostIps || ""}]\nAdmin url: http://${config.d}` + config.a);
        }
    };
    core_Main = Object(external_tslib_.__decorate)([ external_typescript_ioc_.Singleton, Object(external_tslib_.__param)(0, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(1, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(2, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(3, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(4, external_typescript_ioc_.Inject), Object(external_tslib_.__param)(5, external_typescript_ioc_.Inject), Object(external_tslib_.__metadata)("design:paramtypes", [ App_App, AuthRegister_AuthRegister, Koa_Koa, MainRouter_MainRouter, ApiRouter_ApiRouter, AdminRouter_AdminRouter ]) ], core_Main), 
    dbInit().then(() => external_typescript_ioc_.Container.get(core_Main).main()).catch(err => {
        console.error.bind(console, "(./src/core/index.ts:124)")(err), process.exit(1);
    });
} ]);