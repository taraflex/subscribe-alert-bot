import { Context } from 'koa';
import { Inject, Singleton } from 'typescript-ioc';

import { EntityClass, has, RTTIItemState } from '@crud/types';
import { ClientSettings, ClientSettingsRepository } from '@entities/ClientSettings';
import bodyParseMsgpack from '@middlewares/body-parse-msgpack';
import checkEntityAccess from '@middlewares/check-entity-access';
import smartRedirect from '@middlewares/smart-redirect';

import { ApiRouter, msgpack } from './core/ApiRouter';
import { TgClient } from './TgClient';

@Singleton
export class App {
    constructor(
        @Inject private readonly tgClient: TgClient,
        @Inject private readonly settings: ClientSettings,
        @Inject settingsRepository: ClientSettingsRepository,
        @Inject apiRouter: ApiRouter,
    ) {
        apiRouter.patch('tgclient_save', '/tgclient-save/:id', checkEntityAccess(ClientSettings), bodyParseMsgpack, smartRedirect, async (ctx: Context) => {
            const body: ClientSettings = <any>ctx.request.body;

            (ClientSettings as EntityClass<ClientSettings>).insertValidate(body);

            await this.tgClient.update({ type: 'bot', ...body }, ctx);

            const { props } = (ClientSettings as EntityClass).rtti;
            for (let k in body) {
                const p = props[k];
                if (p && !has(p, RTTIItemState.HIDDEN)) {
                    settings[k] = body[k];
                }
            }

            await settingsRepository.save(settings);

            ctx.status = 201;
            msgpack(ctx, settings);
        });
    }

    async init() {
        const { settings } = this;

        if (settings.apiHash && settings.apiId && settings.token) {
            try {
                await this.tgClient.update({ type: 'bot', ...settings });
                LOG_INFO('Tg connected');
            } catch (err) {
                LOG_ERROR(err);
                this.tgClient.sendErrorEmail();
            }
        }
    }
}