import { Column, Entity, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';

@Access()
@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ pattern: /\S+@\S+/ })
    @Column({ unique: true })
    email: string;

    @v({ min: 5, max: 70 })
    @Column({ length: 60 })
    password: string;

    @Column({ type: 'int', default: 0b1111111111111111111111111111111 })
    role: number;
}

export abstract class UserRepository extends Repository<User>{ }