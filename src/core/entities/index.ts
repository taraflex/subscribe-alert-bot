import { EntityClass, RepositoryClass } from '@crud/types';
//@ts-ignore
import * as generated from '@entities/generated'; //будет сгенерировано через webpack

export const entities: EntityClass[] = generated.entities;
export const repositories: RepositoryClass[] = generated.repositories;