import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { SingletonEntity } from '@ioc';
import { rndString, seedRndString } from '@utils/rnd';

@SingletonEntity(null, PROJECT_SETTINGS_EXIST)
export class Settings {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: seedRndString() })
    secret: string;

    @Column({ default: seedRndString() })
    linkSecret: string;

    @Column({ default: false })
    installed: boolean;

    async updateLinkSecret() {
        this.linkSecret = await rndString();
    }
}

export abstract class SettingsRepository extends Repository<Settings>{ }