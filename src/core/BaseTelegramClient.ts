import { ElMessageBox } from 'element-ui/types/message-box';
import { Context } from 'koa';
import sanitize from 'sanitize-filename';
import { Client } from 'tdl';
import { TDLib } from 'tdl-tdlib-ffi';
import { chat, file, message, MessageContent, TdlibParameters, Update } from 'tdlib-types';
import { IDisposable } from 'xterm';

import { fromCtx } from '@rpc/ServerRPC';
import { PORT } from '@utils/config';
import dataPath from '@utils/data-path';
import { ExtendableError } from '@utils/extendable-error';
import { sendErrorEmail } from '@utils/send-email';
import { API_HASH_RE, BOT_TOKEN_RE, getBotInfo } from '@utils/tg-utils';

import { DeferredMap } from './Deffered';

export interface AuthInfo {
    readonly apiHash: string;
    readonly apiId: number;
}

export interface UserAuthInfo extends AuthInfo {
    readonly type: 'user';
    readonly phone: string;
}

export interface BotAuthInfo extends AuthInfo {
    readonly type: 'bot';
    readonly token: string;
}

const tdlibOptions = {
    online: false,
    prefer_ipv6: false,
    use_storage_optimizer: true,
    always_parse_markdown: false,
    ignore_inline_thumbnails: true,
} as const;

const tdlibPostOptions = {
    disable_top_chats: true,
    disable_time_adjustment_protection: true,
    disable_persistent_network_statistics: true,
    ignore_sensitive_content_restrictions: true,
    disable_sent_scheduled_message_notifications: true,
} as const;

function thumb<T>(o: T, p1: keyof T, p2: string): file {
    return o[p1 as any] || o[p2 as any];
}

export abstract class BaseTelegramClient implements IDisposable {
    protected messagePendings = new DeferredMap<message>();

    protected client: Client;

    protected apiHash: string;
    protected apiId: number;
    protected phoneOrToken: string;
    protected _id: number = 0;

    get id() {
        return this._id;
    }

    private readonly tdl = new TDLib('./libtdjson');

    protected readonly tdParams: Partial<TdlibParameters> = {
        _: 'tdlibParameters',
        use_secret_chats: false,
        device_model: APP_NAME + ' ' + PORT,
        enable_storage_optimizer: tdlibOptions.use_storage_optimizer,
        use_file_database: false,
        use_chat_info_database: true,
        use_message_database: true,
        ignore_file_names: true,
        use_test_dc: false
    };

    async searchChats(query: string, limit: number): Promise<chat[]> {
        let chats_ids = new Set((await Promise.all([
            this.client.invoke({ _: 'searchChatsOnServer', query, limit }),
            this.client.invoke({ _: 'searchChats', query, limit })
        ])).flatMap(cs => cs.chat_ids));
        return Promise.all(Array.from(chats_ids, chat_id => this.client.invoke({ _: 'getChat', chat_id })));
    }

    sendErrorEmail() {
        return sendErrorEmail(`Сбой в авторизации телеграм клиента. Пересохраните настройки для получения нового авторизационного кода.`);
    }

    async update(authInfo: Readonly<UserAuthInfo | BotAuthInfo>, ctx?: Context) {
        const { type, apiHash, apiId } = authInfo;
        if (!apiId) {
            throw 'Invalid apiId: ' + apiId;
        }
        if (type != 'user' && type != 'bot') {
            throw 'Invalid tdlib client type: ' + type;
        }
        if (!API_HASH_RE.test(apiHash)) {
            throw 'Invalid apiHash: ' + apiHash;
        }
        if (authInfo.type === 'user' && !authInfo.phone) {
            throw 'Invalid phone: ' + authInfo.phone;
        }
        if (authInfo.type === 'bot' && !BOT_TOKEN_RE.test(authInfo.token)) {
            throw 'Invalid bot token: ' + authInfo.token;
        }
        if (
            !this.client ||
            authInfo.apiHash != this.apiHash ||
            authInfo.apiId != this.apiId ||
            (authInfo.type === 'user' && authInfo.phone != this.phoneOrToken) ||
            (authInfo.type === 'bot' && authInfo.token != this.phoneOrToken)
        ) {
            const phoneOrToken = authInfo.type === 'bot' ? authInfo.token : authInfo.phone;
            const profile = sanitize(type === 'bot' ? (await getBotInfo(phoneOrToken)).id.toString() : phoneOrToken);
            try {
                this.dispose();
                const client = this.client = new Client(this.tdl, {
                    apiId,
                    apiHash,
                    databaseDirectory: dataPath(profile + '/database'),
                    filesDirectory: dataPath(profile + '/files'),
                    verbosityLevel: 1,
                    skipOldUpdates: false,
                    useTestDc: false,
                    useMutableRename: true,
                    tdlibParameters: this.tdParams
                });
                client.on('update', async (u: Update) => {
                    try {
                        if (u._ === 'updateMessageSendSucceeded') {
                            this.messagePendings.fulfill(u.message.chat_id + '_' + u.old_message_id, u.message);
                        } else if (u._ === 'updateMessageSendFailed') {
                            this.messagePendings.reject(u.message.chat_id + '_' + u.old_message_id, { code: u.error_code, message: u.error_message });
                        } else {
                            await this.onUpdate(u);
                        }
                    } catch (err) {
                        LOG_ERROR(err);
                    }
                });
                client.on('error', LOG_ERROR);
                await client.connect();
                for (let name in tdlibOptions) {
                    await client.invoke({
                        _: 'setOption',
                        name,
                        value: { _: 'optionValueBoolean', value: tdlibOptions[name] }
                    });
                }
                this.apiHash = apiHash;
                this.apiId = apiId;
                this.phoneOrToken = phoneOrToken;
                await client.login(() => type === 'bot' ?
                    {
                        type: 'bot',
                        getToken: async () => phoneOrToken
                    } : {
                        type: 'user',
                        getPassword: async (passwordHint) => {
                            const code = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt('Введите телеграм пароль.' + (passwordHint ? ' Подсказка: ' + passwordHint : ''), {
                                showCancelButton: false,
                                roundButton: true
                            });
                            return code.value || '';
                        },
                        getPhoneNumber: async _ => phoneOrToken,
                        getAuthCode: async (retry?: boolean) => {
                            if (!ctx) {
                                this.dispose();
                                if (!retry) {
                                    await this.sendErrorEmail();
                                }
                                throw 'Telegram client auth required.';
                            }
                            const code = await fromCtx(ctx).remote<ElMessageBox>('ElMessageBox').prompt('Введите код авторизации телеграм клиента (придет в sms или в уже авторизованный клиент).', {
                                showCancelButton: false,
                                inputType: 'number',
                                roundButton: true
                            });
                            return code.value || '';
                        },
                    }
                );
                const u = await this.getMe();
                this._id = u.id;
                if (u.type._ !== "userTypeBot") {//todo check if can change value
                    for (let name in tdlibPostOptions) {
                        await this.client.invoke({
                            _: 'setOption',
                            name,
                            value: { _: 'optionValueBoolean', value: tdlibPostOptions[name] }
                        });
                    }
                }
                await this.afterUpdate(authInfo);
                return true;
            } catch (err) {
                this.dispose();
                throw err;
            }
        }
        return false;
    }

    protected readonly afterUpdate: (authInfo: any) => Promise<any | void>;
    protected readonly onUpdate: (_: Update) => Promise<any | void>;

    getMe() {
        return this.client ? this.client.invoke({ _: 'getMe' }) : null;
    }

    dispose() {
        //this.removeAllListeners();
        if (this.client) {
            this.client.destroy();
            this.client = null;
            this.phoneOrToken = null;
            this.apiHash = null;
            this.apiId = null;
            this._id = 0;
        }
    }

    async logout() {
        if (this.client) {
            await this.client.invoke({ _: 'logOut' });
        }
        this.dispose();
    }

    editMessage(chat_id: number, message_id: number, content: MessageContent) {
        switch (content._) {
            case 'messageText':
                return this.client.invoke({
                    _: 'editMessageText',
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageText",
                        text: content.text,
                        disable_web_page_preview: !content.web_page
                    }
                });
            /*case 'messageLocation':
                return this.client.invoke({
                    _: 'editMessageLiveLocation',
                    message_id,
                    chat_id,
                    location: content.location
                });*/
            case 'messageVideo':
                return this.client.invoke({
                    _: 'editMessageMedia',
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageVideo',
                        video: {
                            _: 'inputFileRemote',
                            id: content.video.video.remote.id
                        },
                        thumbnail: content.video.thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.video.thumbnail, 'file', 'photo').remote.id
                            },
                            width: content.video.thumbnail.width,
                            height: content.video.thumbnail.height,
                        } : undefined,
                        duration: content.video.duration,
                        supports_streaming: content.video.supports_streaming,
                        width: content.video.width,
                        height: content.video.height,
                    }
                });
            case 'messageAudio':
                return this.client.invoke({
                    _: 'editMessageMedia',
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageAudio',
                        audio: {
                            _: 'inputFileRemote',
                            id: content.audio.audio.remote.id
                        },
                        album_cover_thumbnail: content.audio.album_cover_thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.audio.album_cover_thumbnail, 'file', 'photo').remote.id
                            },
                            width: content.audio.album_cover_thumbnail.width,
                            height: content.audio.album_cover_thumbnail.height,
                        } : undefined,
                        duration: content.audio.duration,
                        title: content.audio.title,
                        performer: content.audio.performer
                    }
                });
            case 'messageAnimation':
                return this.client.invoke({
                    _: 'editMessageMedia',
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageAnimation',
                        animation: {
                            _: 'inputFileRemote',
                            id: content.animation.animation.remote.id
                        },
                        thumbnail: content.animation.thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.animation.thumbnail, 'file', 'photo').remote.id
                            },
                            width: content.animation.thumbnail.width,
                            height: content.animation.thumbnail.height,
                        } : undefined,
                        duration: content.animation.duration,
                        width: content.animation.width,
                        height: content.animation.height,
                        caption: content.caption,
                    }
                });
            case 'messagePhoto':
                return this.client.invoke({
                    _: 'editMessageMedia',
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessagePhoto',
                        photo: {
                            _: 'inputFileRemote',
                            id: content.photo.sizes[0].photo.remote.id
                        },
                        width: content.photo.sizes[0].width,
                        height: content.photo.sizes[0].height,
                        caption: content.caption
                    }
                });
            case 'messageDocument':
                return this.client.invoke({
                    _: 'editMessageMedia',
                    message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageDocument',
                        document: {
                            _: 'inputFileRemote',
                            id: content.document.document.remote.id,
                        },
                        thumbnail: content.document.thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.document.thumbnail, 'file', 'photo').remote.id,
                            },
                            width: content.document.thumbnail.width,
                            height: content.document.thumbnail.height,
                        } : undefined,
                        caption: content.caption
                    }
                });
            default:
                if (content['caption'] != null) {
                    return this.client.invoke({
                        _: 'editMessageCaption',
                        message_id,
                        chat_id,
                        caption: content['caption'],
                    });
                } else {
                    throw new UnsupportedMessageType(content._);
                }
        }
    }

    async sendMessage(chat_id: number, content: MessageContent, reply_to?: number) {
        const message = await this.sendMessageInternal(chat_id, content, reply_to);
        if (message.sending_state && message.sending_state._ === 'messageSendingStateFailed') {
            throw {
                code: message.sending_state.error_code,
                message: message.sending_state.error_message
            };
        }
        return this.messagePendings.make(message.chat_id + '_' + message.id);
    }

    sendMessageInternal(chat_id: number, content: MessageContent, reply_to_message_id: number) {
        /*
    | messageExpiredPhoto
    | messageSticker
    | messageExpiredVideo
    | messageGame
    | messagePoll
    | messageInvoice
    | messageBasicGroupChatCreate
    | messageSupergroupChatCreate
    | messageChatChangeTitle
    | messageChatChangePhoto
    | messageChatDeletePhoto
    | messageChatAddMembers
    | messageChatJoinByLink
    | messageChatDeleteMember
    | messageChatUpgradeTo
    | messageChatUpgradeFrom
    | messagePinMessage
    | messageScreenshotTaken
    | messageChatSetTtl
    | messageCustomServiceAction
    | messageGameScore
    | messagePaymentSuccessful
    | messagePaymentSuccessfulBot
    | messageContactRegistered
    | messageWebsiteConnected
    | messagePassportDataSent
    | messagePassportDataReceived
    | messageUnsupported
        */
        switch (content._) {
            case 'messageText':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: "inputMessageText",
                        text: content.text,
                        disable_web_page_preview: !content.web_page
                    },
                });
            case 'messageVideoNote':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageVideoNote',
                        video_note: {
                            _: 'inputFileRemote',
                            id: content.video_note.video.remote.id,
                        },
                        thumbnail: content.video_note.thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.video_note.thumbnail, 'file', 'photo').remote.id
                            },
                            width: content.video_note.thumbnail.width,
                            height: content.video_note.thumbnail.height,
                        } : undefined,
                        duration: content.video_note.duration,
                        length: content.video_note.length
                    }
                });
            case 'messageVoiceNote':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageVoiceNote',
                        voice_note: {
                            _: 'inputFileRemote',
                            id: content.voice_note.voice.remote.id,
                        },
                        duration: content.voice_note.duration,
                        waveform: content.voice_note.waveform,
                        caption: content.caption,
                    }
                });
            case 'messageVideo':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageVideo',
                        video: {
                            _: 'inputFileRemote',
                            id: content.video.video.remote.id
                        },
                        thumbnail: content.video.thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.video.thumbnail, 'file', 'photo').remote.id
                            },
                            width: content.video.thumbnail.width,
                            height: content.video.thumbnail.height,
                        } : undefined,
                        duration: content.video.duration,
                        supports_streaming: content.video.supports_streaming,
                        width: content.video.width,
                        height: content.video.height,
                        caption: content.caption
                    }
                });
            case 'messageAudio':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageAudio',
                        audio: {
                            _: 'inputFileRemote',
                            id: content.audio.audio.remote.id
                        },
                        album_cover_thumbnail: content.audio.album_cover_thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.audio.album_cover_thumbnail, 'file', 'photo').remote.id
                            },
                            width: content.audio.album_cover_thumbnail.width,
                            height: content.audio.album_cover_thumbnail.height,
                        } : undefined,
                        duration: content.audio.duration,
                        title: content.audio.title,
                        performer: content.audio.performer,
                        caption: content.caption
                    }
                });
            case 'messageAnimation':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageAnimation',
                        animation: {
                            _: 'inputFileRemote',
                            id: content.animation.animation.remote.id
                        },
                        thumbnail: content.animation.thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.animation.thumbnail, 'file', 'photo').remote.id
                            },
                            width: content.animation.thumbnail.width,
                            height: content.animation.thumbnail.height,
                        } : undefined,
                        duration: content.animation.duration,
                        width: content.animation.width,
                        height: content.animation.height,
                        caption: content.caption,
                    }
                });
            case 'messageLocation':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageLocation',
                        location: content.location,
                        live_period: content.live_period,
                        heading: content.heading,
                        proximity_alert_radius: content.proximity_alert_radius
                    }
                });
            case 'messageVenue':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageVenue',
                        venue: content.venue,
                    }
                });
            case 'messageContact':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageContact',
                        contact: content.contact,
                    }
                });
            case 'messagePhoto':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: content.photo.sizes[0].photo.remote ? {
                        _: 'inputMessagePhoto',
                        photo: { _: 'inputFileRemote', id: content.photo.sizes[0].photo.remote.id },
                        width: content.photo.sizes[0].width,
                        height: content.photo.sizes[0].height,
                        caption: content.caption
                    } : {
                        _: 'inputMessagePhoto',
                        photo: { _: 'inputFileLocal', path: content.photo.sizes[0].photo.local.path },
                        caption: content.caption
                    }
                });
            case 'messageDocument':
                return this.client.invoke({
                    _: 'sendMessage',
                    reply_to_message_id,
                    chat_id,
                    input_message_content: {
                        _: 'inputMessageDocument',
                        document: {
                            _: 'inputFileRemote',
                            id: content.document.document.remote.id,
                        },
                        thumbnail: content.document.thumbnail ? {
                            _: 'inputThumbnail',
                            thumbnail: {
                                _: 'inputFileRemote',
                                id: thumb(content.document.thumbnail, 'file', 'photo').remote.id,
                            },
                            width: content.document.thumbnail.width,
                            height: content.document.thumbnail.height,
                        } : undefined,
                        caption: content.caption
                    }
                });
            default:
                throw new UnsupportedMessageType(content._);
        }
    }

    static isContentSupported(s: string) {
        return supportedContents.has(s);
    }
}

export class UnsupportedMessageType extends ExtendableError { }

const supportedContents = new Set(Array.from(BaseTelegramClient.prototype.sendMessageInternal.toString().matchAll(/case\s+['"](\w+)/g), r => r[1]));