import { createReadStream } from 'fs';
import cp from 'fs-cp';
import { Context } from 'koa';
import compress from 'koa-compress';
import Router from 'koa-router';
import { paramCase } from 'param-case';
import { parse as pathToRegexp } from 'path-to-regexp';
import pluralize from 'pluralize';
import { Container, Inject, Scope, Singleton } from 'typescript-ioc';

import { Action, ValidatorType } from '@crud/types';
import { entities } from '@entities';
import { Settings } from '@entities/Settings';
import { User } from '@entities/User';
import checkAuth from '@middlewares/check-auth';
import flash from '@middlewares/flash';
import installed from '@middlewares/installed';
import noStore from '@middlewares/no-store';
import normalizeTralingSlashes from '@middlewares/normalize-traling-slashes';
import pug from '@middlewares/pug';
import redirectAfterError from '@middlewares/redirect-after-error';
import smartRedirect from '@middlewares/smart-redirect';
import { isValidVar, tosource } from '@taraflex/string-tools';
import componentsTemplate from '@templates/components/index.mustache';
import dataPath from '@utils/data-path';
import * as PtyFactory from '@utils/pty-factory';
import { applyRoutes } from '@utils/routes-helpers';
import rtti2vue from '@utils/rtti-to-vue-component';
import SUtils from 'set-utils';
import { AuthRegister, destroySession } from './AuthRegister';
import { createDBFilename, DBConnection } from './DBConnection';
import { MainRouter } from './MainRouter';
import { CheckPassRoutes } from './routes/checkpass';
import { InstallRoutes } from './routes/install';
import { LoginRoutes } from './routes/login';
import { ResetEmailRoutes } from './routes/resetemail';
import { ResetPassRoutes } from './routes/resetpass';

function restart(ctx: Context) {
    ctx.type = 'json';
    ctx.body = '{"success":"ok"}';
    ctx.res.once('finish', () => process.exit(2));
}

const EXT_COMPS = new Set<ValidatorType>(['javascript']);
const WORKER_INFO = __non_webpack_require__(__dirname + '/workers-assets.json');
const COMP_ENT_MAP: Record<string, string> = {
    'javascript': 'monaco'
}

@Singleton
export class AdminRouter extends Router {

    constructor(
        @Inject settings: Settings,
        @Inject dbConnection: DBConnection,
        @Inject installRoutes: InstallRoutes,
        @Inject loginRoutes: LoginRoutes,
        @Inject resetPassRoutes: ResetPassRoutes,
        @Inject checkPassRoutes: CheckPassRoutes,
        @Inject resetEmailRoutes: ResetEmailRoutes,
        @Inject { session, passport, passportSession }: AuthRegister
    ) {
        super();

        this.use(
            //helmet({ hsts: false, referrerPolicy: true }),
            flash, //flash messages
            smartRedirect, //redirect to named route 
            normalizeTralingSlashes,
            redirectAfterError('login'),
            session,
            passport,
            passportSession,
            pug //render pug template
        );

        this.put('logout', '/logout', ctx => {
            ctx.status = 204;
            destroySession(ctx);
        });

        applyRoutes(this, installRoutes);
        if (!settings.installed) {
            this.use(installed(settings));
        }
        applyRoutes(this, loginRoutes);
        applyRoutes(this, resetPassRoutes);

        this.use(checkAuth);
        applyRoutes(this, checkPassRoutes);
        applyRoutes(this, resetEmailRoutes);

        const componentsCache = {};

        this.get('root', '/', (ctx: Context) => {
            const user = ctx.state.user as User;
            if (!componentsCache[user.role]) {
                const components = [];
                const validators = new Set<ValidatorType>();
                for (let entity of entities) {
                    let { name, rtti, checkAccess, hooks } = entity;
                    name = pluralize(name);

                    const can = Object.create(null);
                    for (let action of ['GET', 'PATCH', 'PUT', 'DELETE']) {
                        try {
                            can[action] = checkAccess(action as Action, ctx.state.user);
                        } catch { }
                    }
                    if (can.GET && rtti.displayInfo.display) {
                        can.CHANGE = can.PUT || can.PATCH;
                        const render = rtti2vue(rtti, can, ctx);
                        const remote = Object.values(rtti.props)
                            .filter(v => v.remote)
                            .map(v => {
                                const i = v.remote.lastIndexOf('.');
                                const c = v.remote.slice(0, i);
                                const method = v.remote.slice(i + 1);
                                if (!isValidVar(c) || !isValidVar(method) || method == v.remote) {
                                    throw `Invalid property ${tosource(v)}`;
                                }
                                return { c, method };
                            });

                        for (const p in rtti.props) {
                            validators.add(rtti.props[p].type);
                        }

                        components.push({
                            name,
                            sortable: !!(can.PATCH && rtti.displayInfo.sortable && rtti.displayInfo.display !== 'single'),
                            order: rtti.displayInfo.order | 0,
                            icon: rtti.displayInfo.icon,
                            render,
                            can: tosource(can, null, ''),
                            rtti: tosource(rtti, null, ''),
                            hooks: tosource(hooks, null, ''),
                            endPoint: paramCase(name),
                            remote
                        });
                    }
                }

                const classes: { name: string, props: string[] }[] = [];
                const m = (Scope.Singleton.constructor as any).instances as Map<Function, any>;
                m.forEach((v, k) => classes.push({ name: k.name, props: Object.keys(v).filter(p => !/^[_#]/.test(p)) }));

                const { stack } = Container.get(MainRouter) as Router;
                componentsCache[user.role] = {
                    components,
                    Paths: stack
                        .filter(l => l.name)
                        .map(l => {
                            if (l.paramNames.length > 0) {
                                const tokens = pathToRegexp(l.path);
                                return {
                                    name: l.name,
                                    url: `function (${l.paramNames.map(p => p.name).join(', ')}) { return ${tokens.map(p => p.constructor == String ? JSON.stringify(p) : (p as any).name).join(' + "/" + ')}}`
                                }
                            } else {
                                return {
                                    name: l.name,
                                    url: JSON.stringify(l.path)
                                }
                            }
                        }),
                    entries: Array.from(SUtils.map(SUtils.intersect(validators, EXT_COMPS), c => COMP_ENT_MAP[c])).concat('index'),
                    HasPty: PtyFactory.AVALIBLE,
                    EDITOR_WORKER: WORKER_INFO['editor.worker'].js,
                    TYPESCRIPT_WORKER: WORKER_INFO['typescript.worker'].js
                };
            }

            ctx.pug('index', {
                entries: componentsCache[user.role].entries,
                inlineScript: componentsTemplate({
                    ...componentsCache[user.role],
                    email: user.email,
                    Flash: JSON.stringify(ctx.flash)
                })
            }, true);
        });

        this.get('backup', '/backup', noStore, compress(), (ctx: Context) => {
            ctx.attachment(createDBFilename());
            ctx.body = createReadStream(dbConnection.filename);//todo close filestream???
        });

        //put вместо get/post потому что возможен csrf
        this.put('restore', '/restore', async (ctx: Context) => {
            await cp(ctx.req, dataPath(createDBFilename()));
            await restart(ctx);
        });

        this.put('restart', '/restart', restart);
    }
}