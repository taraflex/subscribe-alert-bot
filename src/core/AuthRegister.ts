import { compare } from 'bcryptjs';
import { Context, Middleware } from 'koa';
import compose from 'koa-compose';
import passport from 'koa-passport';
import session from 'koa-session';
import { Strategy } from 'passport-local';
import { Inject, Singleton } from 'typescript-ioc';

import { User, UserRepository } from '@entities/User';
import { destroyByUserId } from '@rpc/ServerRPC';
import { trim } from '@taraflex/string-tools';
import { DEFAULT_COOKIE_OPTIONS } from '@utils/server-cookie';

import { Koa } from './Koa';

const SESSION_COOKIE_LITERAL = '_s';

export function sessionSig(ctx: Context) {
    return ctx.cookies.get(SESSION_COOKIE_LITERAL + '.sig');
}

export function destroySession(ctx: Context) {
    const id = ctx.state.user && ctx.state.user.id;
    const bId = sessionSig(ctx);
    if (id) {
        ctx.res.once('finish', () => destroyByUserId(id, bId));
    }
    ctx.session = null;
    //deleteCookie(SESSION_COOKIE_LITERAL, ctx, true);
}

@Singleton
export class AuthRegister {
    readonly session: Middleware;
    readonly passport: Middleware;
    readonly passportSession: Middleware;
    readonly passportComposed: Middleware;

    constructor(
        @Inject userRepository: UserRepository,
        @Inject app: Koa
    ) {

        passport.serializeUser((user: User, done: Function) => {
            done(null, user.id);
        });

        passport.deserializeUser((id: number, done) => {
            userRepository.findOneOrFail(id)
                .then(user => done(null, user))
                .catch(err => done(err, false));
        });

        passport.use(new Strategy({
            usernameField: 'email',
            passwordField: 'password'
        }, async (email: string, password: string, done: Function) => {
            try {
                email = trim(email);
                password = trim(password);
                const user = await userRepository.findOneOrFail({ email })
                if (await compare(password, user.password)) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            } catch (err) {
                done(err, false);
            }
        }));

        this.session = session({
            ...DEFAULT_COOKIE_OPTIONS,
            key: SESSION_COOKIE_LITERAL,
            maxAge: 86400000 * 365,
            renew: true,
            signed: true
        }, app);
        this.passport = passport.initialize();
        this.passportSession = passport.session();
        this.passportComposed = compose([this.session, this.passport, this.passportSession]);
    }
}