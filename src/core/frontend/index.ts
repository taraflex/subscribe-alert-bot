import 'normalize.css';
import 'element-ui/lib/theme-chalk/index.css';
import './modular-admin-html-gh-pages/css/app-blue.css';
import '@fortawesome/fontawesome-free/css/solid.min.css';
import '@fortawesome/fontawesome-free/css/brands.min.css';
import '@fortawesome/fontawesome-free/css/fontawesome.min.css';
import './index.scss';
import './wysiwyg';

import cloneDeep from 'clone-deep';
import ElementUI, { Message, MessageBox, Notification } from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import deepEqual from 'fast-deep-equal';
import pMap from 'p-map';
import { paramCase } from 'param-case';
import { sentenceCase } from 'sentence-case';
import Vue from 'vue';
import { mixins } from 'vue-class-component';
import VueRouter from 'vue-router';

import { Action, has, RowState, RTTI, RTTIItemState } from '@crud/types';
// @ts-ignore
import App from '@frontend/App.vue';
// @ts-ignore
import ElUploadWrapper from '@frontend/ElUploadWrapper.vue';
import { ErrorHandlerMixin, notify } from '@frontend/error-handler-mixin';
//@ts-ignore
import frontendComponents from '@frontend/generated'; //array generated by webpack
// @ts-ignore
import Terminal from '@frontend/terminal/Terminal.vue';
import { FlashLevel } from '@middlewares/flash';
import { ClientRPC } from '@rpc/ClientRPC';
import { debounce } from '@taraflex/debounce';
import { httpJson, httpMsgpack } from '@utils/axios-smart';
import { decode, encode } from '@utils/msgpack';
import { compile, typeDefault } from '@utils/validator-helpers';

// @ts-ignore
import Monaco from './monaco/Monaco.vue';
// @ts-ignore
import Tgmd from './tgmd/Tgmd.vue';

__webpack_require__.p = window['LAST_SCRIPT_SRC'] || __webpack_require__.p;

Vue.component(Monaco.name, Monaco);
Vue.component(Tgmd.name, Tgmd);

ClientRPC.register('ElMessageBox', MessageBox);
ClientRPC.register('ElNotification', Notification);
ClientRPC.register('ElMessage', Message);
ClientRPC.register('console', {
    log: console.log.bind(console),
    error: console.error.bind(console),
    warn: console.warn.bind(console),
    info: console.info.bind(console),
    debug: console.debug.bind(console),
    clear: console.clear.bind(console),
    table: console.table.bind(console),
});
Vue.prototype.$rpc = new ClientRPC();

Vue.use(VueRouter);
Vue.use(ElementUI, { size: 'medium', locale });

const entitiesComponents = EntitiesFactory([{
    components: {
        ElUploadWrapper
    },
    async created() {
        try {
            const { props } = this.rtti as RTTI;

            this.fields = Object.freeze(Object.keys(props));
            this.DEFS = this.genDefault();

            const p = Object.create(null);
            (this.fields as string[]).forEach(k => {
                if (!has(props[k], RTTIItemState.HIDDEN)) {
                    p[k] = props[k];
                }
            })

            this.validate = compile(p);

            this.visibleFileds = new Set(Object.keys(p));
            this.primaryFieldIndex = this.fields.findIndex(f => has(props[f], RTTIItemState.PRIMARY));

            if (Flash) {
                Flash.messages && Flash.messages.forEach(m => notify(m, FlashLevel.INFO));
                Flash.warnings && Flash.warnings.forEach(m => notify(m, FlashLevel.WARNING));
                Flash.errors && Flash.errors.forEach(m => notify(m, FlashLevel.ERROR));
            }

            await this.loadData();
        } catch (err) {
            this.onError(err);
        } finally {
            this.inited = true;
            this.loading = false;
        }
    },
    async activated() {
        try {
            await this.loadData();
        } catch (err) {
            this.onError(err);
        } finally {
            this.loading = false;
        }
    },
    mounted() {
        const { tableHeader } = this.$refs;
        //todo адаптировать под draggable
        if (tableHeader /*&& !this.draggableOptions.disabled*/) {
            const table = this.$refs.table as HTMLElement;
            this._tableHeaderScrollHandler = () => {
                tableHeader.style.transform = `translate(0,${table.scrollTop}px)`;
            };
            table.addEventListener('scroll', this._tableHeaderScrollHandler);
        }
    },
    beforeDestroy() {
        if (this._tableHeaderScrollHandler) {
            this.$refs.table.removeEventListener('scroll', this._tableHeaderScrollHandler);
        }
    },
    computed: {
        hasUnsaved() {
            return this.values.some(v => v.state === RowState.EDIT);
        }
    },
    methods: {
        getUrl(act: Action, ...args: string[]) {
            let path = Paths[this.hooks[act] || this.endPoint];
            if (path instanceof Function) {
                return path.apply(null, args);
            }
            if (args.length > 0) {
                return path + '/' + args.join('/');
            }
            return path;
        },
        editSingleRow() {
            if ((this.rtti as RTTI).displayInfo.display === 'single' && this.can.PATCH) {
                this.values[0].state = RowState.EDIT;
                return true;
            }
            return false;
        },
        async loadData() {
            if (!this.loading) {
                this.loading = true;

                const { data, status } = await httpMsgpack.get(this.getUrl('GET'));

                if ((status !== 304 || this.values.length < 2) && data && data.byteLength > 0) {
                    let { values } = decode(new Uint8Array(data, 0));
                    if (Array.isArray(values)) {
                        const changed = new Map();
                        const created: { v: any, i: number }[] = [];
                        if (this.can.CHANGE) {
                            const lastIndex = this.values.length - 1;
                            for (let i = 0; i <= lastIndex; ++i) {
                                const row = this.values[i];
                                if (row.state === RowState.EDIT) {
                                    const prime = row.v[this.primaryFieldIndex];
                                    if (prime != null) {
                                        changed.set(prime, row.v);
                                    } else if (i < lastIndex || !deepEqual(row.v, this.genDefault())) {
                                        created.push({ v: row.v, i });
                                    }
                                }
                            }
                        }
                        if (changed.size > 0) {
                            values = values.map(v => {
                                const oldRow = changed.get(v[this.primaryFieldIndex]);
                                if (oldRow) {
                                    const row = { state: RowState.EDIT, errors: [], v: oldRow };
                                    try {
                                        this.row2Obj(row);
                                    } catch (e) {
                                        this.processErrors(e, row);
                                    }
                                    return row;
                                }
                                return { state: RowState.NONE, errors: [], v }
                            });
                            changed.clear();
                        } else {
                            values = values.map(v => ({ state: RowState.NONE, errors: [], v }));
                        }
                        created.forEach(({ v, i }) => {
                            values.splice(Math.min(i, values.length), 0, { state: RowState.EDIT, errors: [], v });
                        })
                        this.values = values;
                    }
                }

                if (this.values.length < 1) {
                    this.add();
                } else {
                    this.editSingleRow();
                }
            }
        },
        edit(row) {
            row.state = this.isNew(row) || this.can.PATCH ? RowState.EDIT : RowState.NONE;
        },
        async save(row) {
            if (this.can.CHANGE) {
                try {
                    row.state = RowState.SAVE;

                    const o = this.row2Obj(row);
                    this.cleanErrors(row);

                    const bo = encode(o);
                    const primary = this.getPrimary(row);
                    const { data } = primary == null ?
                        await httpMsgpack.put(this.getUrl('PUT'), bo) :
                        await httpMsgpack.patch(this.getUrl('PATCH', primary), bo);

                    const ndata = decode(new Uint8Array(data, 0));
                    for (let k in ndata) {
                        row.v[this.fields.indexOf(k)] = ndata[k];
                    }

                    if (!this.editSingleRow()) {
                        row.state = RowState.NONE;
                    }
                } catch (e) {
                    this.onError(e, row);
                    this.edit(row);
                }
            }
        },
        async saveAll() {
            if (this.inited && this.can.CHANGE) {
                try {
                    const rows = this.values.filter(row => row.state === RowState.EDIT);
                    if (rows.length > 0) {
                        this.inited = false;
                        await pMap(rows, row => this.save(row), { concurrency: 6 });
                    }
                } finally {
                    this.inited = true;
                }
            }
        },
        async remove(row, i: number) {
            const primary = this.getPrimary(row);
            if (primary == null) {
                this.removeRow(i);
            } else if (this.can.DELETE) {
                try {
                    row.state = RowState.REMOVE;
                    await httpMsgpack.delete(this.getUrl('DELETE', primary));
                    this.removeRow(i);
                } catch (e) {
                    this.onError(e, row);
                    row.state = RowState.NONE;
                }
            }
        },
        async removeAll() {
            if (this.inited && this.can.DELETE) {
                await this.$confirm('Удалить все записи?', '', {
                    showCancelButton: false,
                    type: 'warning'
                });
                try {
                    this.inited = false;
                    await httpMsgpack.delete(this.getUrl('DELETE'));
                    this.values.splice(0, this.values.length);
                    this.add();
                } catch (e) {
                    this.onError(e);
                } finally {
                    this.inited = true;
                    this._tableHeaderScrollHandler && this._tableHeaderScrollHandler();
                }
            }
        },
        add() {
            if (this.can.PUT) {
                if (this.values.length > 0 && deepEqual(this.values[this.values.length - 1].v, this.DEFS)) {
                    const row = this.values[this.values.length - 1];
                    row.state = RowState.EDIT;
                    this.cleanErrors(row);
                } else {
                    this.values.push({ state: RowState.EDIT, errors: [], v: cloneDeep(this.DEFS) });
                }
            }
        },
        removeRow(i: number) {
            this.values.splice(i, 1);
            if (this.values.length < 1) {
                this.add();
            }
        },
        genDefault() {
            const a = [];
            const { props } = this.rtti as RTTI;
            for (let i = 0; i < this.fields.length; ++i) {
                const f = props[this.fields[i]];
                if (has(f, RTTIItemState.HIDDEN)) {
                    a.push(undefined);
                } else {
                    if (f.type === 'array' || f.type === 'files') {
                        if (f.default) {
                            try {
                                if (f.default.startsWith('[') && f.default.endsWith(']')) {
                                    a.push(JSON.parse(f.default));
                                    continue;
                                }
                            } catch { }
                            a.push(f.default.split(',').filter(s => s));
                        } else {
                            a.push([]);
                        }
                    } else {
                        a.push(f.default === undefined ? typeDefault(f.type) : f.default);
                    }
                }
            }
            return Object.freeze(a.map(Object.freeze));
        },
        isNew(row) {
            return this.getPrimary(row) == null;
        },
        getPrimary(row) {
            return row.v[this.primaryFieldIndex];
        },
        cleanErrors(row) {
            row && row.errors.splice(0, row.errors.length);
        },
        onError(errors, row) {
            this.loading = false;
            errors = errors.errors || errors;
            if (Array.isArray(errors)) {
                this.cleanErrors(row);
                this.processErrors(errors, row);
                if (row && row.errors.length > 0) {
                    this.edit(row);
                }
            } else {
                notify(errors);
            }
        },
        row2Obj(row) {
            const o = Object.create(null);
            for (let i = 0; i < this.fields.length; ++i) {
                const { props } = this.rtti as RTTI;
                const field = this.fields[i];
                if (!has(props[field], RTTIItemState.HIDDEN)) {
                    o[field] = row.v[i];
                }
            }
            const validateResult = this.validate(o);
            if (validateResult !== true) {
                throw validateResult;
            }
            return o;
        },
        processErrors(errors: any[], row) {
            for (let error of errors) {
                try {
                    if (!this.visibleFileds.has(error.field)) {
                        throw 0;
                    }
                    row.errors[this.fields.indexOf(error.field)] = error.message.replace(new RegExp(`\s+'${error.field}'\s+`), ' ');
                } catch {
                    notify(error);
                }
            }
        },
        sort({ newIndex, oldIndex }) {
            for (let i = oldIndex; i <= newIndex; i += Math.sign(newIndex - oldIndex)) {
                console.log(i);
            }
        }
    }
}], debounce);

entitiesComponents.push(...frontendComponents/*, Playground*/);

if (HasPty) {
    entitiesComponents.push(Terminal)
}

const routes = Object.freeze(
    entitiesComponents
        .sort((a, b) => (a.order || Number.POSITIVE_INFINITY) - (b.order || Number.POSITIVE_INFINITY))
        .map(v => {
            const { name } = v;
            return Object.freeze({
                path: '/' + paramCase(name),
                icon: v.icon || '',
                name: sentenceCase(name),
                component: v,
            })
        })
);

new Vue({
    ...App,
    el: '#app',
    mixins: [mixins(ErrorHandlerMixin)],
    router: new VueRouter({
        routes: [
            ...routes,
            (routes.length > 0 ? { path: '/', redirect: routes[0].path } : { path: '/' })
        ]
    }),
    data: {
        routes,
        locked: false,
        UserInfo,
        Paths,
    },
    methods: {
        async restore({ target }) {
            try {
                const file: File = target.files[0];
                if (!file) {
                    return;
                }
                if (!file.name.toLowerCase().endsWith('.sqlite3')) {
                    throw 'Invalid database file extension.';
                }
                if (file.size <= 4096) {
                    throw 'Database file size less than possible.';
                }
                this.loading = true;
                const data: ArrayBuffer = await new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.onloadend = () => {
                        if (reader.error) {
                            reject(reader.error);
                        } else {
                            resolve(reader.result as ArrayBuffer)
                        }
                    }
                    reader.readAsArrayBuffer(file);
                });
                this.$rpc.dispose();
                await httpJson.put(Paths.restore, data);
                await this.waitServerRestart();
            } catch (err) {
                this.onError(err);
                this.$rpc.restartClosed();
            } finally {
                target.value = '';
                this.loading = false;
            }
        }
    }
});