import ResizeObserver from 'resize-observer-polyfill';

import baseTextSettings from '@frontend/base-text-settings';
import { notify } from '@frontend/error-handler-mixin';
import rndId from '@utils/rnd-id';

type IStandaloneCodeEditor = import('monaco-editor').editor.IStandaloneCodeEditor;

function createWorker(url: string) {
    let blob: Blob;
    const content = `importScripts('${url}')`;
    try {
        blob = new Blob([content], { type: 'application/javascript' });
    } catch (e) {
        //@ts-ignore
        const builder = new (window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder)();
        builder.append(content);
        blob = builder.getBlob();
    }
    return new Worker((window.URL || (window as any).webkitURL).createObjectURL(blob));
}

(self as any).MonacoEnvironment = {
    getWorker(_: any, label: string) {
        const path = window['LAST_SCRIPT_SRC'] || __webpack_require__.p;
        if (label === 'typescript' || label === 'javascript') {
            return createWorker(path + TYPESCRIPT_WORKER);
        }
        return createWorker(path + EDITOR_WORKER);
    }
}

export default {
    name: 'monaco',
    props: {
        value: String,
        readonly: Boolean,
        extraLib: String,
        mode: String
    },
    data() {
        return {
            linesCount: 1,
            loading: true
        };
    },
    watch: {
        readonly(v: boolean) {
            this.editor && (this.editor as IStandaloneCodeEditor).updateOptions({ readOnly: v });
        },
        value(v: string) {
            v = v || '';
            if (this.editor && v != this._lastv) {
                const e = this.editor as IStandaloneCodeEditor;
                this._listener.dispose();
                const model = e.getModel();
                model.setValue(v);
                this.linesCount = model.getLineCount();
                this._listener = model.onDidChangeContent(_ => this.onChange());
            }
        }
    },
    methods: {
        onChange() {
            const model = (this.editor as IStandaloneCodeEditor).getModel();
            this.linesCount = model.getLineCount();
            this.$emit('input', this._lastv = model.getValue());
        }
    },
    async  mounted() {
        try {
            const { editor, languages, KeyCode, KeyMod } = require('monaco-editor') as typeof import('monaco-editor');
            if (this.extraLib) {
                this._lang = languages.typescript.javascriptDefaults.addExtraLib(this.extraLib, rndId() + 'backend.d.ts');
            }

            languages.typescript.javascriptDefaults.setDiagnosticsOptions({
                noSemanticValidation: false,
                noSyntaxValidation: false
            });

            languages.typescript.javascriptDefaults.setCompilerOptions({
                target: languages.typescript.ScriptTarget.Latest,
                allowNonTsExtensions: true,
                module: languages.typescript.ModuleKind.CommonJS,
                moduleResolution: languages.typescript.ModuleResolutionKind.NodeJs,
                strictBindCallApply: true,
                resolveJsonModule: true,
                allowSyntheticDefaultImports: true
            });

            const e = editor.create(this.$refs.playground, {
                ...baseTextSettings,
                value: this._lastv = this.value || '',
                lineHeight: 16,
                language: this.mode || 'javascript',
                accessibilitySupport: 'off',
                autoClosingBrackets: 'never',
                minimap: { enabled: false },
                quickSuggestionsDelay: 170,
                renderIndentGuides: false,
                renderLineHighlight: 'gutter',
                renderWhitespace: 'none',
                roundedSelection: false,
                wordSeparators: "`~!@#%^&*()=-+[{]}\\|;:'\",.<>/?",
                wordWrap: 'on',
                lightbulb: { enabled: false },
                readOnly: this.readonly,
                scrollBeyondLastLine: false
            });
            e.addCommand(KeyMod.CtrlCmd | KeyMod.Shift | KeyCode.KEY_P, () => e.getAction('editor.action.quickCommand').run());
            e.addCommand(KeyMod.CtrlCmd | KeyCode.KEY_D, () => e.getAction('editor.action.formatDocument').run());
            this.linesCount = e.getModel().getLineCount();
            this._listener = e.getModel().onDidChangeContent(_ => this.onChange());
            /* e.addAction({
                 id: 'run-code',
                 label: 'Run code',
                 keybindings: [KeyCode.F5],
                 run: async (e) => {
                     if (!e.getConfiguration().readOnly) {
                         try {
                             e.updateOptions({ readOnly: true });
                             let script = e.getValue();
                             storage.write('js_playground', script);
 
                             await (this.$rpc as ClientRPC).remote<ICore>('ICore').eval(script);
                         } catch (err) {
                             notify(err);
                         } finally {
                             e.updateOptions({ readOnly: false });
                             //todo if displayed
                             //e.focus();
                         }
                     }
                 }
             });*/

            this.editor = e;

            this.obs = this.obs || new ResizeObserver(() => {
                const dim = (this.$refs.playground as HTMLParagraphElement).getBoundingClientRect();
                e.layout({ width: dim.width - 2, height: Math.max(16, dim.height - 20) });
            });

            this.obs.observe(this.$refs.playground);

            await languages.typescript.getJavaScriptWorker();

            this.loading = false;
        } catch (err) {
            if (this.obs) {
                this.obs.disconnect();
            }
            if (this._listener) {
                this._listener.dispose();
                this._listener = null;
            }
            if (this._lang) {
                this._lang.dispose();
                this._lang = null;
            }
            if (this.editor) {
                this.editor.dispose();
                this.editor = null;
            }
            notify(err);
            this.loading = false;
        }
    },
    beforeDestroy() {
        if (this.obs) {
            this.obs.disconnect();
        }
        if (this.editor) {
            this.editor.dispose();
            this.editor = null;
        }
    }
}