import { Notification } from 'element-ui';
import Vue from 'vue';
import Component from 'vue-class-component';

import { FlashLevel } from '@middlewares/flash';
import { stringify } from '@taraflex/string-tools';
import { http, httpJson } from '@utils/axios-smart';
import delay from '@utils/delay';

export function notify(err, level?: FlashLevel) {
    const params: any = {
        message: stringify(err),
        position: 'top-right',
        offset: -8
    }
    switch (level || FlashLevel.ERROR) {
        case FlashLevel.ERROR:
            LOG_ERROR(err);
            params.duration = 0;
            console.log(Notification.error(params));
            break;
        case FlashLevel.WARNING:
            LOG_WARN(err);
            params.duration = 0;
            Notification.warning(params);
            break;
        default:
            LOG_INFO(err);
            Notification.info(params);
            break;
    }
}

@Component
export class ErrorHandlerMixin extends Vue {
    loading = false;
    onError(errors) {
        this.loading = false;
        errors = errors && errors.errors || errors;
        if (errors && typeof (errors.forEach) === 'function') {
            errors.forEach(e => notify(e));
        } else {
            notify(errors);
        }
    }
    run(fn, ...args) {
        fn && this[fn](...args);
    }
    async logout() {
        try {
            this.loading = true;
            this.$rpc.dispose();
            await http.http.put(Paths.logout);
            window.location.href = Paths.login;
        } catch (err) {
            this.onError(err);
            this.$rpc.restartIfClosed();
        } finally {
            this.loading = false;
        }
    }
    changePassword() {
        window.location.href = Paths.resetpass;
    }
    changeEmail() {
        window.location.href = Paths.resetemail;
    }
    async waitServerRestart(): Promise<never> {
        for (; ;) {
            try {
                await delay(7000);
                if ((await httpJson.get(Paths.health)).data.success === 'ok') {
                    window.location.reload(true);
                }
            } catch { }
        }
    }
    async restartServer() {
        try {
            this.loading = true;
            this.$rpc.dispose();
            await httpJson.put(Paths.restart).catch(e => e);
            await this.waitServerRestart();
        } catch (err) {
            this.onError(err);
            this.$rpc.restartIfClosed();
        }
    }
}