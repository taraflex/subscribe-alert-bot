import { Context } from 'koa';
import { IPty } from 'node-pty';
import ServerWebSoket from 'ws';

import { User } from '@entities/User';
import { stringify } from '@taraflex/string-tools';
import { create as ptyCreate } from '@utils/pty-factory';

import { RPC } from './RPC';

//todo clear chain when remove user
const RPCServers = new Map<number, Map<number, ServerRPC>>();

export function pingServers(timeout: number) {
    RPCServers.forEach(m => m.forEach(rpc => rpc.ping(timeout)));
}

export function fromCtx(ctx?: Context): ServerRPC {
    return RPCServers.get(ctx.state.user.id).get(parseInt(ctx.headers['x-tab']));
}

export function fromThis(self?: any): ServerRPC {
    return (self || this).__rpc;
}

export function destroyByUserId(id: number, browserId?: string) {
    const m = RPCServers.get(id)
    if (m) {
        if (browserId) {
            m.forEach(v => v.browserId == browserId && v.dispose());
        } else {
            m.forEach(v => v.dispose());
        }
    }
}

class ServerRPCHandler {
    constructor(private readonly server: ServerRPC) { }
    get(target: any, prop: string) {
        return prop == '__rpc' ? this.server : target[prop];
    }
}

export class ServerRPC extends RPC {
    protected lastActive: number = Date.now();

    private readonly onWSMessage = (message: string | ArrayBuffer) => {
        this.lastActive = Date.now();
        this.process(message).catch(LOG_ERROR);
    }

    private readonly onWSClose = () => {
        try {
            this.dispose();
        } catch (err) {
            LOG_ERROR(err);
        }
    }

    private readonly onWSPong = () => {
        this.lastActive = Date.now();
    }

    constructor(
        protected readonly socket: ServerWebSoket,
        protected readonly tab: number,
        readonly browserId: string,
        protected readonly user: User,
        terminalSize?: { cols: number, rows: number }
    ) {
        super();

        if (tab !== +tab || !tab) {
            throw 'Invalid websocket tab: ' + stringify(tab);
        }

        let remoteTabs = RPCServers.get(user.id);
        if (!remoteTabs) {
            RPCServers.set(user.id, remoteTabs = new Map());
        }
        remoteTabs.set(tab, this);

        socket.on('close', this.onWSClose);
        socket.on('pong', this.onWSPong);
        socket.on('message', this.onWSMessage);

        if (terminalSize) {
            this.enableTerminal(terminalSize);
        }
    }

    getActual() {
        const r = RPCServers.get(this.user.id)?.get(this.tab);
        if (r !== this) {
            this.dispose();
        }
        return r;
    }

    protected send(data: Uint8Array) {
        this.getActual()?.socket.send(data);
    }

    enableTerminal(terminalSize: { cols: number, rows: number }) {
        if (this.term) {
            (this.term as IPty).resize(terminalSize.cols, terminalSize.rows);
        } else {
            const term = ptyCreate(terminalSize);
            if (term) {
                this.term = term;
                this.socket.send(String.fromCharCode(27) + 'c');
                term.onData(data => {
                    try {
                        this.socket.send(data);
                    } catch (err) {
                        LOG_ERROR(err);
                    }
                });
                //todo auto restart
                //term.on('exit', () => {});
            }
        }
    }

    protected rcall(o: any, fname: string, args: any[]) {
        return (o[fname] as Function).apply(new Proxy(o, new ServerRPCHandler(this)), args);
    }

    dispose() {
        if (this.term) {
            (this.term as IPty).kill();
            this.term = undefined;
        }
        const c = RPCServers.get(this.user.id);
        if (c?.get(this.tab) === this) {
            c.delete(this.tab);
        }
        if (
            this.socket.readyState != ServerWebSoket.CLOSING &&
            this.socket.readyState != ServerWebSoket.CLOSED
        ) {
            this.socket.close();
        }
    }

    ping(timeout: number) {
        if (Date.now() - this.lastActive >= timeout && this.socket.readyState === ServerWebSoket.OPEN) {
            this.socket.ping();
        }
    }
}