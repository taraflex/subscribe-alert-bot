import encode32 from 'base32-encode';
import { createHash } from 'crypto';

export default function (s: string | Buffer | NodeJS.TypedArray | DataView, base32?: boolean): string {
    const h = createHash('md5').update(s);
    return base32 ? (encode32(h.digest().buffer, 'Crockford') as string).toLowerCase() : h.digest('hex');
}