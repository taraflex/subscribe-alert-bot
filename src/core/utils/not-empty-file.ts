import { statSync } from 'fs';

export default function (filename: string) {
    try {
        return statSync(filename).size > 0;
    } catch { }
    return false;
}