import { Container } from 'typescript-ioc';

import { UserRepository } from '@entities/User';
import http from '@utils/http';

import { HOSTNAME } from './config';

export default async function sendEmail(email: string, subject: string, message: string) {
    try {
        await http('https://script.google.com/macros/s/AKfycbyau7ecNID-Y9M6s067panGIAdkcUS31BCtoa73K4GURJh_76Q/exec', {
            method: 'POST',
            form: {
                email,
                body: message,
                subject
            },
            throwHttpErrors: false,
            resolveBodyOnly: true
        });
    } catch (err) {
        err && LOG_ERROR(err);
        throw 'Email sending error. Try again later.';
    }
}

export async function sendErrorEmail(message: string) {
    if (!DEBUG) {
        try {
            const ur = Container.get(UserRepository) as UserRepository;
            await sendEmail((await ur.findOne()).email, 'Ошибка в работе бота http://' + HOSTNAME, message);
        } catch { }
    }
}