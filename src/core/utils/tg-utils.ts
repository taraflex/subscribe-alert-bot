import http from '@utils/http';
import rndId from '@utils/rnd-id';

interface TelegramUser {
    id: number;
    is_bot: boolean;
    first_name: string;
    last_name?: string;
    username?: string;
    language_code?: string;
}

export function getCommandParser(cmd: string) {
    const re = new RegExp('^\\/' + cmd + '(\\s+|$)', 'i');
    return (text: string): string => re.test(text) ? text.substr(cmd.length + 2).trim() : null;
}

export const startCmdPayload = getCommandParser('start');

export async function getBotInfo(token: string): Promise<TelegramUser> {
    const bot: TelegramUser = (await http(`https://api.telegram.org/bot${token}/getMe`).json())['result'];
    if (!bot.is_bot) {
        throw 'Invalid token: ' + token;
    }
    return bot;
}

const DEFAULT_URL = { toString: () => rndId().toFixed() } as string;
export function invisibleLink(url: string = DEFAULT_URL) {
    return `[\u00A0](${url})⁠`;
}

const escMdMapClient = {
    '~~': '\u200D~\u200D~\u200D',
    '**': '\u200D*\u200D*\u200D',
    '](': ']\u200D(',
    '__': '\u200D_\u200D_\u200D',
    '@': '@\u200D',
    '#': '#\u200D',
    '/': '/\u200D',
    '`': '’',
    '\\': '﹨',
};

export function escMdClient(s: string) {
    return s ? s.replace(/~~|\*\*|\]\(|__|#|@|\/|`|\\/g, m => escMdMapClient[m]) : '';
}

const escMdMapBotLegacy = {
    '*': '\\*',
    '_': '\\_',
    '[': '\\[',
    '@': '@\u200D',
    '#': '#\u200D',
    '/': '/\u200D',
    '`': '\\`',
    '\\': '﹨',
};

export function escMdBotLegacy(s: string) {
    return s ? s.replace(/\*|\[|_|#|@|\/|`|\\/g, m => escMdMapBotLegacy[m]) : '';
}

const escMdMapForce = {
    '*': '✶',
    '#': '♯',
    '[': '［',
    ']': '］',
    '_': '＿',
    '\\': '﹨',
    '`': '’',
    '~': '～',
    '@': '@\u200D',
    '/': '/\u200D',
};

export function escMdForce(s: string) {
    return s ? s.replace(/[~@\/\*\[\]\\_`#]/g, m => escMdMapForce[m]) : '';
}

export const BOT_TOKEN_RE = /^\d{9,}:[\w-]{35}$/;
export const API_HASH_RE = /^[0-9abcdef]{32}$/;