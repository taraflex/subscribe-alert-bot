import FV from 'fastest-validator';
import checkArray from 'fastest-validator/lib/rules/array';
import checkString from 'fastest-validator/lib/rules/string';

import { ValidatorType } from '@crud/types';

export function typeDefault(type: ValidatorType) {
    switch (type) {
        case 'enum':
        case 'number':
            return 0;
        case 'javascript':
        case 'regex':
        case 'string':
        case 'wysiwyg':
        case 'md-tgbotlegacy':
        case 'md-tgclient':
        case 'link':
            return '';
        case 'boolean':
            return false;
        case 'files':
        case 'array':
            return [];
        case 'date':
            return new Date();
        case 'any':
        case 'object':
            return Object.create(null);
    }
    return null;
}

const v = new FV({
    messages: {
        validRegex: "The '{field}' {actual}"
    }
});
v.add('regex', function ({ messages }) {
    return {
        source: `
            try {
                new RegExp(value);
            } catch(err) {
                ${this.makeError({ type: "validRegex", actual: "err.message", messages })}
            }
            return value;
        `
    };
});
v.add('javascript', function ({ messages }) {
    return {
        source: `
            try {
                new Function(value);
            } catch(err) {
                ${this.makeError({ type: "validRegex", actual: "err.message", messages })}
            }
            return value;
        `
    };
});
v.add('wysiwyg', checkString);
v.add('link', checkString);
v.add('md-tgclient', checkString);
v.add('md-tgbotlegacy', checkString);
v.add('files', checkArray);

export const compile = v.compile.bind(v);