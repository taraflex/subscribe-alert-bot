export class PromisesIterator<T = any> implements AsyncIterator<T, void> {
    [Symbol.asyncIterator]() {
        return this;
    }
    protected objects: T[] = [];
    protected lastError: any;

    protected readonly resolvers: ((o: { value: T }) => void)[] = [];
    protected readonly rejecters: ((o: any) => void)[] = [];

    protected finished: boolean;

    constructor(promises: readonly PromiseLike<T>[]) {
        let s = promises.length;
        this.finished = s < 1;
        promises.forEach(p => p.then(value => {
            this.objects.push(value);
            this.tick(--s < 1);
        }, err => {
            this.lastError = err ?? this.lastError;
            this.tick(--s < 1);
        }));
    }

    protected tick(finished: boolean) {
        this.finished = finished;
        if (this.resolvers.length > 0) {

            if (this.objects.length > 0) {
                this.resolvers.shift()({ value: this.objects.shift() });
                this.rejecters.shift();
            }

            if (finished && this.rejecters.length > 0) {
                this.rejecters.forEach(c => c(this.lastError));
                this.resolvers.length = 0;
                this.rejecters.length = 0;
            }
        }
    }

    next(): Promise<{ value: T } | { done: true, value: undefined }> {
        return new Promise((resolve, reject) => {
            if (this.objects.length > 0) {
                resolve({ value: this.objects.shift() });
            } else if (this.finished) {
                if (this.lastError !== undefined) {
                    reject(this.lastError);
                } else {
                    resolve({ done: true, value: undefined })
                }
            } else {
                this.resolvers.push(resolve);
                this.rejecters.push(reject);
            }
        });
    }
}