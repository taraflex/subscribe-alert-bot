import { execSync } from 'child_process';

import { PEM, REAL_HOSTNAME } from './config';

export const DEV_NULL = process.platform.startsWith('win') ? 'nul' : '/dev/null';

const cmd = "grep -i '^Port\\|^BasicAuth' /etc/tinyproxy/tinyproxy.conf";
const { Port, BasicAuth } = JSON.parse('{' + execSync(PEM ? `ssh -o UserKnownHostsFile=${DEV_NULL} -o StrictHostKeyChecking=no -o BatchMode=yes -i "${PEM}" root@${REAL_HOSTNAME} "${cmd}"` : cmd, { encoding: 'utf-8', windowsHide: true }).trim().split('\n').map(v => v.replace(/\s+/, ':').replace(/([^:]+)/g, '"$1"')) + '}');

export const PROXY_HOST = REAL_HOSTNAME;
export const PROXY_PORT = Port;
export const PROXY_AUTH = BasicAuth.replace(/\s+/, ':');
export const PROXY_URL = `http://${PROXY_AUTH}@${PROXY_HOST}:${PROXY_PORT}`;