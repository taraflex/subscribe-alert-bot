import { compare } from 'bcryptjs';
import { Context } from 'koa';
import bodyParser from 'koa-body';
import { Singleton } from 'typescript-ioc';

import redirectAfterError from '@middlewares/redirect-after-error';
import { trim } from '@taraflex/string-tools';
import { get, post } from '@utils/routes-helpers';

@Singleton
export class CheckPassRoutes {

    @get({ name: 'checkpass', path: '/checkpass/:action' })
    checkpass(ctx: Context) {
        const { action } = ctx.params;
        ctx.pug('checkpass', { action: ctx.resolve('checkpass', { action }) });
    }

    @post({ path: '/checkpass/:action' }, redirectAfterError('checkpass'), bodyParser({
        text: false,
        json: false
    }))
    async checkpassPost(ctx: Context) {
        ctx.session.confirmed = 0;
        let { password } = ctx.request.body;

        if (!await compare(trim(password), ctx.state.user.password)) {
            throw 'Wrong password';
        }
        ctx.session.confirmed = Date.now();
        ctx.namedRedirect(ctx.params.action);
    }

}