import { Context } from 'koa';

import { Action, EntityClass } from '@crud/types';
import isAjax from '@utils/is-ajax';

export default ({ checkAccess }: EntityClass) => function (ctx: Context, next: () => Promise<any>) {
    if (!isAjax(ctx)) {
        throw 403;
    }
    checkAccess(ctx.request.method as Action, ctx.state.user);
    return next();
}