import etag from 'etag';
import { Context } from 'koa';
import { compileTemplate, LocalsObject } from 'pug';

import { STATIC, HOSTNAME, REAL_HOSTNAME } from '@utils/config';
import resolveUrl from '@utils/resolve-url';

declare module 'koa' {
    interface Context {
        pug: (file?: string | compileTemplate, locals?: LocalsObject, noCommon?: boolean) => void;
    }
}

const assetsUrls: string[] = new Array(2);

export default function (ctx: Context, next: () => Promise<any>) {
    ctx.pug = (file?: string | compileTemplate, locals?: LocalsObject, noCommon?: boolean) => {

        ctx.type = 'html';
        const template: compileTemplate = (typeof file === 'function') ? file : require('../templates/' + file + '.pug');

        const r: Record<string, { css?: string, js?: string }> = Object.assign({}, ...[!noCommon && 'common'].concat(locals?.entries).map(e => e && __non_webpack_require__(__dirname + '/' + e + '-assets.json')));

        const styles = Object.values(r).map(r => r.css).filter(Boolean)
        const scripts = Object.values(r).map(r => r.js).filter(Boolean)

        const body = template({
            title: APP_TITLE,
            assetsUrl: assetsUrls[+ctx.secure] || (assetsUrls[+ctx.secure] = resolveUrl(ctx, STATIC, !ctx.secure && HOSTNAME != REAL_HOSTNAME)),
            favicon: 'favicon.ico',
            ...ctx.flash,
            ...locals,
            styles,
            scripts
        });

        ctx.set('Cache-Control', 'no-cache, max-age=31536000');
        const et = etag(body);
        ctx.set('ETag', et);
        if (ctx.headers['if-none-match'] && ctx.headers['if-none-match'].endsWith(et)) {
            ctx.status = 304;
        } else {
            ctx.body = body;
        }
    };
    return next();
}