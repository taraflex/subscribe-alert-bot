import { Connection, ObjectLiteral, Repository } from 'typeorm';
import { Provider } from 'typescript-ioc';

export interface ParametricProvider<T, Args extends any[]> extends Provider {
    init(...args: Args): T;
}

export interface AsyncProvider<T, Args extends any[]> extends Provider {
    init(...args: Args): Promise<T>;
}

export type ValidatorType = 'files' | 'link' | 'number' | 'string' | 'boolean' | 'array' | 'object' | 'wysiwyg' | 'md-tgbotlegacy' | 'md-tgclient' | 'regex' | 'javascript' | 'date' | 'enum' | 'any';

export type ArrayValidator = {
    type?: ValidatorType,
    /**
     *If true, the validator accepts empty array[].
     */
    empty?: boolean,
    /**
     *Minimum count of elements.
     */
    min?: number,
    /**
     *Maximum count of elements.
     */
    max?: number,
    /**
     *Fix count of elements.
     */
    length?: number,
    /**
     *The array must contains this element too.
     */
    contains?: any,
    /**
     *Every element must be an element of the enum array.
     */
    enum?: any[],
    items?: Validator | Validator[]
}

export type AnyValidator = {
    type?: ValidatorType
}

export type BooleanValidator = {
    type?: ValidatorType,
    /**
     *if true and the type is not Boolean, try to convert. 1, "true", "1", "on" will be true. 0, "false", "0", "off" will be false.
     */
    convert?: boolean
}

export type DateValidator = {
    type?: ValidatorType,
    /**
     *if true and the type is not Date, try to convert with new Date().
     */
    convert?: boolean
}

export type NumberValidator = {
    type?: ValidatorType,
    /**
     *Minimum value.
     */
    min?: number,
    /**
     *Maximum value.
     */
    max?: number,
    /**
     *Fix value.
     */
    equal?: number,
    /**
     *Can't be equal with this value.
     */
    notEqual?: number,
    /**
     *The value must be a non - decimal value.
     */
    integer?: boolean,
    /**
     *The value must be larger than zero.
     */
    positive?: boolean,
    /**
     *The value must be less than zero.
     */
    negative?: boolean,
    /**
     *if true and the type is not Number, try to convert with parseFloat.
     */
    convert?: boolean
}

export type ObjectValidator = {
    type?: ValidatorType,
    props: { [property: string]: Validator | Validator[] }
}

export type StringValidator = {
    type?: ValidatorType,
    /**
     *If true, the validator accepts empty string "".
     */
    empty?: boolean,
    /**
     *Minimum length of value.
     */
    min?: number,
    /**
     *Maximum length of value.
     */
    max?: number,
    /**
     *Fix length of value.
     */
    length?: number,
    /**
     *Regex pattern.
     */
    pattern?: RegExp,
    /**
     *The value must contains this text.
     */
    contains?: string,
    /**
     *The value must be an element of the enum array.
     */
    enum?: any[]
}

export type JavascriptValidator = StringValidator & {
    extraLib?: string
}

export type Validator = ArrayValidator | DateValidator | AnyValidator | /*ForbiddenValidator |*/ BooleanValidator | NumberValidator | ObjectValidator | StringValidator | JavascriptValidator;

export const enum RTTIItemState {
    NORMAL = 0,
    SEND_ALWAYS = 1 << 1,
    FULLWIDTH = 1 << 2,
    ALLOW_CREATE = 1 << 3,

    PRIMARY = 1 << 4 | SEND_ALWAYS,
    READONLY = 1 << 5 | SEND_ALWAYS,
    HIDDEN = 1 << 6 | READONLY,
}

export function has(item: RTTIItem, state: RTTIItemState) {
    return (item.state & state) === state;
}

export type AdditionalProperties = {
    description?: string,
    placeholder?: string,
    remote?: string;
    uploadUrl?: string;
    uploadAccept?: string;
    state?: number;
}

export type RTTIItem = Validator & AdditionalProperties & {
    type: ValidatorType,
    optional: boolean,//for validators
    state: number,
    default?: any,
    enumPairs?: any[][],
}

export type DisplayInfoTypes = 'single' | 'table';

export interface DisplayInfo {
    icon?: string;
    display?: DisplayInfoTypes;
    order?: number;
    sortable?: boolean;
}

export interface RTTI<T = ObjectLiteral> {
    displayInfo: DisplayInfo;
    props: { [_ in keyof T]: RTTIItem }
}

export type Action = 'GET' | 'PATCH' | 'PUT' | 'DELETE' | '_';

export interface EntityClass<E = any, Args extends any[] = [Connection]
    > extends Function {
    new(): E;
    asyncProvider?: AsyncProvider<E, Args>;
    insertValidate?: (o: E) => void;
    updateValidate?: (o: E) => void;
    checkAccess?: (action: Action, user: { role: number }) => true;
    rtti?: RTTI<E>;
    hooks?: { [_ in Action]?: string };
    __validatorInfo?: { [property: string]: Validator & AdditionalProperties };
}

export interface RepositoryClass<E = any, Args extends any[] = [Connection]> extends Function {
    provider?: ParametricProvider<Repository<E>, Args>;
}

export const enum RowState {
    NONE = 0,
    EDIT = 1,
    SAVE = 2,
    REMOVE = 3
}