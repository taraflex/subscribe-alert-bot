import './preload';

import { resolve4 } from 'dns';
import { createServer, IncomingMessage, OutgoingHttpHeaders } from 'http';
import { Context, ParameterizedContext } from 'koa';
import mount from 'koa-mount';
import send from 'koa-send';
import { normalize } from 'path';
import { setInterval } from 'timers';
import { Container, Inject, Singleton } from 'typescript-ioc';
import { promisify } from 'util';
import { Server as WSServer } from 'ws';

import { pingServers, ServerRPC } from '@rpc/ServerRPC';
import { ADMIN, API, HOSTNAME, PORT, REAL_HOSTNAME, STATIC } from '@utils/config';
import getMyIp from '@utils/get-my-ip';

//@ts-ignore
import { App } from '../App';
import { AdminRouter } from './AdminRouter';
import { ApiRouter } from './ApiRouter';
import { AuthRegister, sessionSig } from './AuthRegister';
import { dbInit } from './DBConnection';
import { Koa } from './Koa';
import { MainRouter } from './MainRouter';

@Singleton
class Main {
    constructor(
        @Inject private readonly baseApp: App,
        @Inject private readonly authRegister: AuthRegister,
        @Inject private readonly app: Koa,
        @Inject private readonly router: MainRouter,
        @Inject private readonly apiRouter: ApiRouter,
        @Inject private readonly adminRouter: AdminRouter
    ) { }

    async main() {
        const { router, apiRouter, adminRouter, app, baseApp } = this;

        await baseApp.init();

        router.use(API, apiRouter.routes(), apiRouter.allowedMethods())
        router.use(ADMIN, adminRouter.routes(), adminRouter.allowedMethods())

        app
            //routes
            .use(router.routes())
            .use(router.allowedMethods())
            //static assets
            .use(mount(STATIC, (ctx: Context) => send(ctx, ctx.path, {
                root: normalize(__dirname + STATIC),
                maxage: 365 * 24 * 60 * 60 * 1000,
                gzip: !DEBUG,
                brotli: false,
                setHeaders(res: ParameterizedContext['res']) {
                    res.setHeader('Access-Control-Allow-Origin', 'http://' + HOSTNAME);
                    res.setHeader('Cache-Control', 'no-store, no-cache, max-age=0');
                }
            })));

        const server = createServer(app.callback());

        const { passportComposed } = this.authRegister;

        const wss = new WSServer({
            server,
            verifyClient: (
                info: { origin: string; secure: boolean; req: IncomingMessage }
                , callback: (res: boolean, code?: number, message?: string, headers?: OutgoingHttpHeaders) => void
            ) => {
                try {
                    const ctx = app.createContext(info.req, null);
                    passportComposed(ctx, () => {
                        callback(!!ctx.state.user);
                        return null;
                    }).catch(console.error);
                } catch (err) {
                    LOG_ERROR(err);
                }
            }
        });

        wss.on('connection', async (socket, request) => {
            try {
                const ctx = app.createContext(request, null);
                await passportComposed(ctx, () => {
                    let terminalSize = ctx.query.cols && ctx.query.rows ? {
                        cols: +ctx.query.cols || 100,
                        rows: +ctx.query.rows || 40
                    } : null;
                    new ServerRPC(
                        socket,
                        parseInt(ctx.query.tab),
                        sessionSig(ctx as Context),
                        ctx.state.user,
                        terminalSize
                    );
                    return null;
                });
            } catch (err) {
                LOG_ERROR(err);
                socket.close();
            }
        });

        setInterval(pingServers, 10000, 8000).unref();

        const [currentIp, hostIps] = await Promise.all([
            await getMyIp(),
            await promisify(resolve4)(HOSTNAME).catch(LOG_ERROR),
            new Promise<void>(resolve => server.listen(PORT, resolve))
        ]);

        console.log(`Current ip: ${currentIp} 
Host: ${REAL_HOSTNAME} [${hostIps || ''}]
Admin url: http://${HOSTNAME}` + ADMIN);
    }
}

dbInit()
    .then(() => (Container.get(Main) as Main).main())
    .catch(err => {
        LOG_ERROR(err);
        process.exit(1);
    });