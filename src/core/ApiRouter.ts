import etag from 'etag';
import { Context } from 'koa';
import Router from 'koa-router';
import { paramCase } from 'param-case';
import pluralize from 'pluralize';
import { ObjectLiteral, Repository } from 'typeorm';
import { Container, Inject, Singleton } from 'typescript-ioc';

import { has, RTTI, RTTIItemState } from '@crud/types';
import { entities } from '@entities';
import bodyParseMsgpack from '@middlewares/body-parse-msgpack';
import checkEntityAccess from '@middlewares/check-entity-access';
import { setNoStoreHeader } from '@middlewares/no-store';
import { encode } from '@utils/msgpack';

import { AuthRegister } from './AuthRegister';
import { DBConnection } from './DBConnection';

export async function data2Model(ctx: Context, model: any, repository: Repository<ObjectLiteral>): Promise<ObjectLiteral> {
    const { body } = ctx.request;
    const { props } = model.constructor.rtti as RTTI;
    for (let k in body) {
        const p = props[k];
        if (p && !has(p, RTTIItemState.READONLY)) {
            model[k] = body[k];
        }
    }
    if (model.beforeFilter) {
        await model.beforeFilter();
    }
    const o = await repository.save(model);
    if (o.afterFilter) {
        await o.afterFilter();
    }
    return o;
}

export function msgpack(ctx: Context, data: any) {
    ctx.type = 'application/x-msgpack';
    const body = Array.isArray(data) ?
        (data.length > 0 ? encode({
            fields: Object.keys(data[0]),
            values: data.map(v => Object.values(v))
        }) : null) :
        (data != null ? encode(data) : null);

    if (ctx.method === 'GET') {
        const et = body && etag(body);
        if (et) {
            ctx.set('Cache-Control', 'no-cache, max-age=31536000');
            ctx.set('ETag', et);
            if (ctx.headers['if-none-match'] && ctx.headers['if-none-match'].endsWith(et)) {
                ctx.status = 304;
                return;
            }
        } else {
            setNoStoreHeader(ctx);
        }
    }
    ctx.body = body;
}

@Singleton
export class ApiRouter extends Router {

    constructor(@Inject connection: DBConnection, @Inject { session, passport, passportSession }: AuthRegister) {
        super();
        this.use(session, passport, passportSession);
        for (let e of entities) {
            const repository = connection.getRepository(e);

            const access = checkEntityAccess(e);
            const name = paramCase(pluralize(e.name));
            const singltonInstance = e.asyncProvider ? Container.get(e) : null;

            const saveModel = singltonInstance ? async (ctx: Context, model: any) => {
                const o = await data2Model(ctx, model, repository);
                const { props } = e.rtti;
                for (let p in props) {
                    singltonInstance[p] = o[p];
                    if (!has(props[p], RTTIItemState.SEND_ALWAYS)) {
                        delete o[p];
                    }
                }
                ctx.status = 201;
                msgpack(ctx, o);
            } : async (ctx: Context, model: any) => {
                const o = await data2Model(ctx, model, repository);
                const { props } = e.rtti;
                for (let p in props) {
                    if (!has(props[p], RTTIItemState.SEND_ALWAYS)) {
                        delete o[p];
                    }
                }
                ctx.status = 201;
                msgpack(ctx, o);
            }

            this
                .get(name, '/' + name, access, async (ctx: Context) => {
                    msgpack(ctx, await repository.find());
                })
                .get(`/${name}/:id`, access, async (ctx: Context) => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) {
                        throw 404;
                    }
                    msgpack(ctx, model);
                })
                .put('/' + name, access, bodyParseMsgpack, (ctx: Context) => saveModel(ctx, new e()))
                .patch(`/${name}/:id`, access, bodyParseMsgpack, async (ctx: Context) => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) {
                        throw 404;
                    }
                    await saveModel(ctx, model);
                })
                .delete(`/${name}`, access, async (ctx: Context) => {
                    await repository.clear();
                    ctx.body = null;
                })
                .delete(`/${name}/:id`, access, async (ctx: Context) => {
                    await repository.delete(ctx.params.id);
                    ctx.body = null;
                });
        }
    }
}