import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';
import { API_HASH_RE, BOT_TOKEN_RE } from '@utils/tg-utils';

@Access({ GET: 0, PATCH: 0 }, { display: 'single', icon: 'cog', order: 4 }, { PATCH: 'tgclient_save' })
@SingletonEntity()
export class ClientSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ notEqual: 0, description: 'Telegram Api id взять [тут](https://my.telegram.org/apps)' })
    @Column({ type: 'int', default: 0, unsigned: true })
    apiId: number;

    @v({ pattern: API_HASH_RE, description: 'Telegram Api hash взять [там же](https://my.telegram.org/apps)' })
    @Column({ default: '', length: 32 })
    apiHash: string;

    @v({ pattern: BOT_TOKEN_RE, description: 'Токен телеграм бота, получить у [@BotFather](tg://resolve?domain=BotFather)' })
    @Column({ default: '' })
    token: string;

    @v({ notEqual: 0, description: 'Id канала (без -100 в начале)' })
    @Column({ type: 'int', unsigned: true, default: 0 })
    channel: number;

    @v({ min: 1, description: 'Минимальная пауза между оповещениями в секундах (изменения применяются только на следующей итерации)' })
    @Column({ type: 'int', default: 360, unsigned: true })
    alertInterval: number;

    @v({ min: 1, description: 'Минимальная пауза перед первой проверкой пользователя после старта диалога с ботом' })
    @Column({ type: 'int', default: 120, unsigned: true })
    firstPause: number;

    @v({ min: 1, description: 'Ответ на /start', type: 'wysiwyg' })
    @Column({ type: 'text', default: '' })
    helloMessage: string;

    @v({ min: 1, description: 'Сообщение о необходимости подписки', type: 'wysiwyg' })
    @Column({ type: 'text', default: '' })
    badMessage: string;

    @v({ description: 'Сообщение после подписки (пустое - не высылать)', type: 'wysiwyg' })
    @Column({ type: 'text', default: '' })
    goodMessage: string;
}

export abstract class ClientSettingsRepository extends Repository<ClientSettings>{ }