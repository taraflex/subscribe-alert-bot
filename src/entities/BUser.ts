import { Column, Entity, PrimaryGeneratedColumn, Repository } from 'typeorm';

@Entity()
export class BUser {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'int', unsigned: true, default: 0, unique: true })
    tId: number;

    @Column({ type: 'int', unsigned: true, default: 0 })
    subscribed: number;

    @Column({ type: 'int', unsigned: true, default: 0 })
    lastAlert: number;
}

export abstract class BUserRepository extends Repository<BUser>{ }