import mem from 'mem';
import { striptags } from 'striptags';
import { formattedText, Update } from 'tdlib-types';
import { Inject, Singleton } from 'typescript-ioc';

import { BUser, BUserRepository } from '@entities/BUser';
import { ClientSettings } from '@entities/ClientSettings';

import { BaseTelegramClient } from './core/BaseTelegramClient';

function now() {
    return Math.ceil(Date.now() / 1000);
}

function setShift<T>(s: Set<T>): T {
    for (let x of s) {
        s.delete(x);
        return x;
    }
}

const NULL_ITER = { value: null };

@Singleton
export class TgClient extends BaseTelegramClient {
    ////////////////////////////////////////////////
    [Symbol.asyncIterator]() {
        return this;
    }
    protected objects: number[] = [];
    protected readonly resolvers: Set<(_: { value: number }) => void> = new Set();
    get size() {
        return this.objects.length;
    }
    add(value: number) {
        if (this.resolvers.size > 0) {
            setShift(this.resolvers)({ value });
        } else {
            this.objects.push(value);
        }
    }
    has(id: number) {
        return this.objects.some(u => u === id);
    }
    next(): Promise<{ value: number }> {
        return this.objects.length > 0 ?
            Promise.resolve({ value: this.objects.shift() }) :
            new Promise<{ value: number }>(resolve => {
                this.resolvers.add(resolve);
                setTimeout(() => {
                    this.resolvers.delete(resolve);
                    resolve(NULL_ITER);
                }, Math.max(100, this.settings.alertInterval * 1000 / this.members | 0)).unref();
            });
    }
    ////////////////////////////////////////////////
    protected members = 1;

    constructor(
        @Inject protected readonly settings: ClientSettings,
        @Inject protected readonly repository: BUserRepository
    ) {
        super();
    }

    protected readonly sanitize = mem(function (s: string) {
        return this.client.execute({
            _: 'parseTextEntities',
            text: striptags(s, { tagReplacementText: '\n', allowedTags: new Set(['strong', 'b', 'i', 'em', 's', 'strike', 'del', 'code', 'pre', 'u', 'a']) }).replace(/&nbsp;/g, ' ').trim().replace(/[\r\n]+/g, '\n'),
            parse_mode: { _: 'textParseModeHTML' }
        }) as formattedText;
    });

    protected t;

    protected readonly afterUpdate = async (authInfo) => {
        await this.client.invoke({ _: 'getChat', chat_id: +('-100' + authInfo.channel) });
        this.members = (await this.repository.count()) || 1;
        this.t ??= this.thread().catch(LOG_ERROR);
    };

    protected readonly onUpdate = async (update: Update) => {
        if (
            update._ === 'updateNewMessage' &&
            !update.message.is_outgoing &&
            !update.message.is_channel_post &&
            update.message.content._ === 'messageText' &&
            /^\/start(\s|$)/.test(update.message.content.text.text)
        ) {
            const user = { tId: update.message.chat_id };
            this.send(user, this.settings.helloMessage).catch(LOG_ERROR);
            if (
                !this.has(user.tId) &&
                !await this.repository.findOne(user)
            ) {
                setTimeout(() => {
                    if (!this.has(user.tId)) {
                        this.add(user.tId);
                    }
                }, this.settings.firstPause * 1000).unref();
            }
        }
    }

    protected send(to: { tId: number }, text: string) {
        return this.sendMessage(to.tId, {
            _: 'messageText',
            web_page: undefined,
            text: this.sanitize(text),
        });
    }

    protected async thread() {
        let lastUId = -1;
        const db = this.repository.createQueryBuilder().select();
        for await (let tId of this) {
            let user: BUser;
            try {
                user = tId ?
                    (await this.repository.findOne({ tId })) || this.repository.create({ tId }) :
                    await db.where('id > :id', { id: lastUId }).andWhere('lastAlert <= :lastAlert', { lastAlert: now() - this.settings.alertInterval }).getOne();
                if (!user) {
                    lastUId = -1;
                    this.add(null);
                    continue;
                }
                if (!tId) {
                    lastUId = user.id;
                }
                await this.client.invoke({ _: 'createPrivateChat', user_id: user.tId, force: true });
                const status = await this.client.invoke({
                    _: 'getChatMember',
                    user_id: user.tId,
                    chat_id: +('-100' + this.settings.channel)
                });
                if (status.status._ === 'chatMemberStatusLeft') {
                    user.subscribed = 0;
                    user.lastAlert = now();
                    await this.repository.save(user);
                    await this.send(user, this.settings.badMessage);
                } else if (!user.subscribed) {
                    user.subscribed = now();
                    if (this.settings.goodMessage) {
                        user.lastAlert = user.subscribed;
                    }
                    await this.repository.save(user);
                    if (this.settings.goodMessage) {
                        await this.send(user, this.settings.goodMessage);
                    }
                }
            } catch (err) {
                if (err && (
                    err.message === 'User not found' ||
                    err.message === 'Bot was blocked by the user' ||
                    err.message === 'Chat not found' ||
                    err.message === 'Chat info not found' ||
                    err.message === "Can't access the chat"
                )) {
                    await this.repository.remove(user).catch(LOG_ERROR);
                }
                LOG_ERROR(user, err);
            } finally {
                if (user) {
                    await this.repository.count().then(v => this.members = v || 1, LOG_ERROR);
                }
            }
        }
    }
}