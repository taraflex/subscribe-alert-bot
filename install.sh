apt-get update -y 
apt-get upgrade --yes --force-yes 
apt-get remove apache2 -y 
apt-get install git lsof man htop ufw nginx unzip openssl tmux build-essential libssl-dev htop python zlib1g-dev zlib1g ccache automake cmake gperf socat jq curl tinyproxy libx11-xcb1 libxt6 libdbus-glib-1-2 -y

if [ -z $(which npm) ]; then
    curl -sSL https://deb.nodesource.com/setup_14.x | bash -
    apt-get install nodejs
    source ~/.bashrc
    npm install -g pm2@latest
    pm2 startup
fi

if [ -z $(which mo) ]; then
    curl -sSL https://git.io/get-mo -o /usr/local/bin/mo
    chmod +x /usr/local/bin/mo
fi

systemctl enable nginx
systemctl enable tinyproxy

npm install --production

export PROXYPORT=$(jq -r '.regulus.proxyport // 8438' ./package.json)
export PROXYUSER=$(LC_CTYPE=C tr -d -c '[:alnum:]' </dev/urandom | head -c 20)
export PROXYPASSWORD=$(LC_CTYPE=C tr -d -c '[:alnum:]' </dev/urandom | head -c 72)

export PORT=$(jq -r '.regulus.port // 7812' ./package.json)
export DEVPORT=$(jq -r '.regulus.devport // 7813' ./package.json)
export HOSTNAME=$(jq -r '.regulus.hostname // "default"' ./package.json)
export APPNAME="$(jq -r .name ./package.json)-$PORT";
export ADMINPAGE=$(jq -r '.regulus.routes.admin // "/adm"' ./package.json)

ufw default deny incoming
ufw allow proto tcp from any to any port $DEVPORT
ufw allow proto tcp from any to any port $PROXYPORT
ufw allow ssh
ufw allow http
ufw allow https
ufw logging off
ufw --force enable

rm -f /etc/nginx/sites-enabled/default

export NGINX_RELOAD="cd $(pwd) && SSL_CERT=\$(test -s /etc/nginx/ssl/$HOSTNAME/cert.pem && echo /etc/nginx/ssl/$HOSTNAME/cert.pem) SSL_KEY=\$(test -s /etc/nginx/ssl/$HOSTNAME/key.pem && echo /etc/nginx/ssl/$HOSTNAME/key.pem) PORT=$PORT DEVPORT=$DEVPORT STATICDIR=$(realpath ./build/static) HOSTNAME=\$(test $HOSTNAME != 'default' && echo $HOSTNAME) mo ./src/core/templates/nginx.mustache.conf > /etc/nginx/sites-available/$HOSTNAME && ln -sf /etc/nginx/sites-available/$HOSTNAME /etc/nginx/sites-enabled/ && systemctl reload nginx"

eval $NGINX_RELOAD || exit 1

if [ $HOSTNAME != 'default' ]; then
    mkdir -p /etc/nginx/ssl/$HOSTNAME/
    chown -R root:root /etc/nginx/ssl/$HOSTNAME/
    chmod -R 0640 /etc/nginx/ssl/$HOSTNAME/ #read -og / write -o
    if [ ! -f /root/.acme.sh/acme.sh ]; then
        curl -sSL https://get.acme.sh | sh
    fi
    /root/.acme.sh/acme.sh --upgrade
    /root/.acme.sh/acme.sh --issue --nginx -d $HOSTNAME -d dev.$HOSTNAME #--force --test 
    /root/.acme.sh/acme.sh --install-cert --force -d $HOSTNAME -d dev.$HOSTNAME --key-file /etc/nginx/ssl/$HOSTNAME/key.pem --fullchain-file /etc/nginx/ssl/$HOSTNAME/cert.pem --reloadcmd "$NGINX_RELOAD"
fi

mo ./src/core/templates/tinyproxy.mustache.conf > /etc/tinyproxy/tinyproxy.conf
systemctl restart tinyproxy

pm2 delete $APPNAME
pm2 start "$(realpath ./build/index.js)" --name $APPNAME --namespace "$HOSTNAME$ADMINPAGE" --log-date-format 'DD.MM HH:mm:ss'
pm2 save --force